# 'Banner' Application
# ###########################

#Installation

This project provides a `docker-compose.yml` for use with
[docker-compose](https://docs.docker.com/compose/); it
uses the `Dockerfile` files stored in .docker/ directory (separate for every project service). 


For useful docker usage better to add your user to "docker" group by next command from "root" user or "sudo":

```bash
$ useradd -G docker {your-username}
$ newgrp docker 
```

Next requried to pull all Composer dependencies using next commands in project root directory:

```bash
$ curl -s https://getcomposer.org/installer | php --
$ php composer.phar update
```

Finally build and start the project services and network using:

```bash
$ docker-compose up -d --build
```

At this point, you can visit http://banner.local/ to see the site running.

Additionally you could visit next ULRs for access development tools:

* [phpMyAdmin](http://phpmyadmin.banner.local/)

* [MailDev](http://maildev.banner.local/)


Next URLs used for non-UI services:

* [MySQL](http://mysql.banner.local/)

===============================================================================================================================================================
Some information about setting up project on Ubuntu:


*GIT
cd ..
sudo apt-get install git
git config --global user.name "Artem"
git config --global user.email "artiom.rudenco@gmail.com"

*curl
sudo apt-get install curl

*ssh-keygen (ex. for Bitbacket)
sudo ssh-keygen
enter  <--- will create in /root/.ssh/ "id_rsa" and "id_rsa.pub"  files
sudo cat /root/.ssh/id_rsa.pub  <- copy displaying key and create new in bitvucket


*docker
sudo apt-get install docker.io  (was installed Docker version 1.13.1, build 092cba3)
useradd -G docker {Artiom}
newgrp docker (if doesn't want use command with "sudo" and one more time "without sudo")


*docker-compose  (from project directory)
(Docs: https://docs.docker.com/compose/install/#install-compose)
curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
+
sudo chmod +x /usr/local/bin/docker-compose
To change docker permitions (to run not as su):
/var/run/docker.sock
+
$ docker-compose --version


Important:
-DOCKER and DOCKER-COMPOSE should show similar version for superuser and for root (example.: sudo docker --version)
-docker-compose should be launched as root user (without sudo).Sometimes (for example when docker.io wasn't attribuite root privileges -> docker-compose may be launched just as superuser)



