<?php

use Zend\Session\Storage\SessionArrayStorage;
use Zend\Session\Validator\HttpUserAgent;
use Zend\Session\Validator\RemoteAddr;

return [

    // Session configuration.
    'session_config' => [
        'cookie_lifetime'     => 60*60*1, // Session cookie will expire in 1 hour.
        'gc_maxlifetime'      => 60*60*24*30, // How long to store session data on server (for 1 month).
    ],
    // Session manager configuration.
    'session_manager' => [
        // Session validators (used for security).
        'validators' => [
            RemoteAddr::class,
            HttpUserAgent::class,
        ]
    ],

    // Setup SMTP transport
    'smtp' =>[
        'name' => 'localhost',
        'host' => '172.18.0.1',
        'port' => 25,
    ],





    // Session storage configuration.
    'session_storage' => [
        'type' => SessionArrayStorage::class
    ],

    'doctrine' => [
        // настройка миграций
        'migrations_configuration' => [
            'orm_default' => [
                'directory' => 'data/Migrations',
                'name'      => 'Doctrine Database Migrations',
                'namespace' => 'Migrations',
                'table'     => 'migrations',
            ],
        ],
        'custom_template' => 'data/doctrineMigrationTemplate.inc'
    ],

    'doctrine_factories' => [
        'migrations_configuration' =>  Application\Service\Doctrine\MigrationsConfigurationFactory::class
    ]
];