<?php

return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'params'        => [
                    'host'     => getenv('MYSQL_HOSTNAME'),
                    'user'     => getenv('MYSQL_USER'),
                    'password' => getenv('MYSQL_PASSWORD'),
                    'dbname'   => getenv('MYSQL_DATABASE'),
                    'charset'  => 'utf8',
                ],
            ],
        ],
    ],
];
