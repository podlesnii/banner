<?php

use Zend\Console\Console;
use Zend\Stdlib\ArrayUtils;
use Zend\Stdlib\Glob;
use ZF\Console\Application;
use ZF\Console\Dispatcher;

define('VERSION', '0.2');

chdir(dirname('/../' . __DIR__));
include __DIR__ . '/../vendor/autoload.php';

$appConfig = require __DIR__ . '/../config/application.config.php';
if (file_exists(__DIR__ . '/../config/development.config.php')) {
    $appConfig = ArrayUtils::merge($appConfig, require __DIR__ . '/../config/development.config.php');
}

//foreach (Glob::glob('config/autoload/{{*}}{{,*.local}}.php', Glob::GLOB_BRACE) as $file) {
//    $appConfig = ArrayUtils::merge($appConfig, include $file);
//}

$application = Zend\Mvc\Application::init($appConfig);
$services = $application->getServiceManager();
$config = $services->get('Config');

$dispatcher = new Dispatcher($services);

$application = new Application(
    $config['console']['name'],
    $config['console']['version'],
    $config['console']['routes'],
    Console::getInstance(),
    $dispatcher
);

$exit = $application->run();
exit($exit);