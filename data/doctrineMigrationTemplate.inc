<?php

namespace <namespace>;

use Application\Service\Doctrine\AbstractMigration;
use Application\Service\DomainModelManager;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version<version> extends AbstractMigration
{
    public
    /**
     * @param DomainModelManager $domainManager
     */

    function seed( DomainModelManager $domainManager)
    {
    // this seed() migration is auto-generated, please modify it to your needs
    }

}

