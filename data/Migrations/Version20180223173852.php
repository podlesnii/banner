<?php

namespace Migrations;

use Application\Entity\City;
use Application\Model\CityDomain;
use Application\Service\Doctrine\AbstractMigration;
use Application\Service\DomainModelManager;


/**
 * Class Version20180223173852
 * @package Migrations
 */
class Version20180223173852 extends AbstractMigration
{
    /**
     * @param DomainModelManager $domainManager
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function seed(DomainModelManager $domainManager)
    {
        /** @var CityDomain $cityDomain */
        $cityDomain = $domainManager->get(CityDomain::class);
        $city = new City();
        $city->setIndexnamecity('chisinau');
        $city->setNamecity('Chisinau');
        $city->setIndexnameregion('municipiul_chisinau');
        $city->setNameregion('Municipiul Chisinau');
        $cityDomain->persist($city);

        $city2 = new City();
        $city2->setIndexnamecity('balti');
        $city2->setNamecity('Balti');
        $city2->setIndexnameregion('municipiul_balti');
        $city2->setNameregion('Municipiul Balti');
        $cityDomain->persist($city2);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city3 = new City();
        $city3->setIndexnamecity('tiraspol');
        $city3->setNamecity('Tiraspol');
        $city3->setIndexnameregion('uta_din_stinga_nistrului');
        $city3->setNameregion('UTA din Stinga Nistrului');
        $cityDomain->persist($city3);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city4 = new City();
        $city4->setIndexnamecity('tighina');
        $city4->setNamecity('Tighina');
        $city4->setIndexnameregion('uta_din_stinga_nistrului');
        $city4->setNameregion('UTA din Stinga Nistrului');
        $cityDomain->persist($city4);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city5 = new City();
        $city5->setIndexnamecity('ribnita');
        $city5->setNamecity('Ribnita');
        $city5->setIndexnameregion('uta_din_stinga_nistrului');
        $city5->setNameregion('UTA din Stinga Nistrului');
        $cityDomain->persist($city5);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city6 = new City();
        $city6->setIndexnamecity('ungheni');
        $city6->setNamecity('Ungheni');
        $city6->setIndexnameregion('raionul_ungheni');
        $city6->setNameregion('Raionul Ungheni');
        $cityDomain->persist($city6);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city7 = new City();
        $city7->setIndexnamecity('cahul');
        $city7->setNamecity('Cahul');
        $city7->setIndexnameregion('raionul_cahul');
        $city7->setNameregion('Raionul Cahul');
        $cityDomain->persist($city7);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city8 = new City();
        $city8->setIndexnamecity('soroca');
        $city8->setNamecity('Soroca');
        $city8->setIndexnameregion('raionul_soroca');
        $city8->setNameregion('Raionul Soroca');
        $cityDomain->persist($city8);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city9 = new City();
        $city9->setIndexnamecity('orhei');
        $city9->setNamecity('Orhei');
        $city9->setIndexnameregion('raionul_orhei');
        $city9->setNameregion('Raionul Orhei');
        $cityDomain->persist($city9);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city10 = new City();
        $city10->setIndexnamecity('comrat');
        $city10->setNamecity('Comrat');
        $city10->setIndexnameregion('uta_gagauzia');
        $city10->setNameregion('UTA Gagauzia');
        $cityDomain->persist($city10);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city11 = new City();
        $city11->setIndexnamecity('dubasari');
        $city11->setNamecity('Dubasari');
        $city11->setIndexnameregion('uta_din_stinga_nistrului');
        $city11->setNameregion('UTA din Stinga Nistrului');
        $cityDomain->persist($city11);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city12 = new City();
        $city12->setIndexnamecity('straseni');
        $city12->setNamecity('Straseni');
        $city12->setIndexnameregion('straseni');
        $city12->setNameregion('Straseni');
        $cityDomain->persist($city12);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city13 = new City();
        $city13->setIndexnamecity('durlesti');
        $city13->setNamecity('Durlesti');
        $city13->setIndexnameregion('municipiul_chisinau');
        $city13->setNameregion('Municipiul Chisinau');
        $cityDomain->persist($city13);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city14 = new City();
        $city14->setIndexnamecity('ceadar_lunga');
        $city14->setNamecity('Ceadar-Lunga');
        $city14->setIndexnameregion('uta_gagauzia');
        $city14->setNameregion('UTA Gagauzia');
        $cityDomain->persist($city14);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city15 = new City();
        $city15->setIndexnamecity('causeni');
        $city15->setNamecity('Causeni');
        $city15->setIndexnameregion('raionul_causeni');
        $city15->setNameregion('Raionul Causeni');
        $cityDomain->persist($city15);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city16 = new City();
        $city16->setIndexnamecity('codru');
        $city16->setNamecity('Codru');
        $city16->setIndexnameregion('municipiul_chisinau');
        $city16->setNameregion('Municipiul Chisinau');
        $cityDomain->persist($city16);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city17 = new City();
        $city17->setIndexnamecity('edinet');
        $city17->setNamecity('Edinet');
        $city17->setIndexnameregion('raionul_edinet');
        $city17->setNameregion('Raionul Edinet');
        $cityDomain->persist($city17);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city18 = new City();
        $city18->setIndexnamecity('drochia');
        $city18->setNamecity('Drochia');
        $city18->setIndexnameregion('raionul_drochia');
        $city18->setNameregion('Raionul Drochia');
        $cityDomain->persist($city18);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city19 = new City();
        $city19->setIndexnamecity('slobozia');
        $city19->setNamecity('Slobozia');
        $city19->setIndexnameregion('uta_din_stinga_nistrului');
        $city19->setNameregion('UTA din Stinga Nistrului');
        $cityDomain->persist($city19);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city20 = new City();
        $city20->setIndexnamecity('ialoveni');
        $city20->setNamecity('Ialoveni');
        $city20->setIndexnameregion('raionul_ialoveni');
        $city20->setNameregion('Raionul Ialoveni');
        $cityDomain->persist($city20);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city21 = new City();
        $city21->setIndexnamecity('hancesti');
        $city21->setNamecity('Hancesti');
        $city21->setIndexnameregion('raionul_hancesti');
        $city21->setNameregion('Raionul Hancesti');
        $cityDomain->persist($city21);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city22 = new City();
        $city22->setIndexnamecity('sangerei');
        $city22->setNamecity('Sangerei');
        $city22->setIndexnameregion('raionul_sangerei');
        $city22->setNameregion('Raionul Sangerei');
        $cityDomain->persist($city22);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city23 = new City();
        $city23->setIndexnamecity('taraclia');
        $city23->setNamecity('Taraclia');
        $city23->setIndexnameregion('raionul_taraclia');
        $city23->setNameregion('Raionul Taraclia');
        $cityDomain->persist($city23);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city24 = new City();
        $city24->setIndexnamecity('vulcanesti');
        $city24->setNamecity('Vulcanesti');
        $city24->setIndexnameregion('uta_gagauzia');
        $city24->setNameregion('UTA Gagauzia');
        $cityDomain->persist($city24);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city25 = new City();
        $city25->setIndexnamecity('falesti');
        $city25->setNamecity('Falesti');
        $city25->setIndexnameregion('raionul_falesti');
        $city25->setNameregion('Raionul Falesti');
        $cityDomain->persist($city25);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city26 = new City();
        $city26->setIndexnamecity('floresti');
        $city26->setNamecity('Floresti');
        $city26->setIndexnameregion('raionul_floresti');
        $city26->setNameregion('Raionul Floresti');
        $cityDomain->persist($city26);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city27 = new City();
        $city27->setIndexnamecity('cimislia');
        $city27->setNamecity('Cimislia');
        $city27->setIndexnameregion('raionul_cimislia');
        $city27->setNameregion('Raionul Cimislia');
        $cityDomain->persist($city27);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city28 = new City();
        $city28->setIndexnamecity('rezina');
        $city28->setNamecity('Rezina');
        $city28->setIndexnameregion('raionul_rezina');
        $city28->setNameregion('Raionul Rezina');
        $cityDomain->persist($city28);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city29 = new City();
        $city29->setIndexnamecity('calarasi');
        $city29->setNamecity('Calarasi');
        $city29->setIndexnameregion('raionul_calarasi');
        $city29->setNameregion('Raionul Calarasi');
        $cityDomain->persist($city29);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city30 = new City();
        $city30->setIndexnamecity('cricova');
        $city30->setNamecity('Cricova');
        $city30->setIndexnameregion('municipiul_chisinau');
        $city30->setNameregion('Municipiul Chisinau');
        $cityDomain->persist($city30);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city31 = new City();
        $city31->setIndexnamecity('dnestrovsc');
        $city31->setNamecity('Dnestrovsc');
        $city31->setIndexnameregion('uta_din_stinga_nistrului');
        $city31->setNameregion('UTA din Stinga Nistrului');
        $cityDomain->persist($city31);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city32 = new City();
        $city32->setIndexnamecity('nisporeni');
        $city32->setNamecity('Nisporeni');
        $city32->setIndexnameregion('raionul_nisporeni');
        $city32->setNameregion('Raionul Nisporeni');
        $cityDomain->persist($city32);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city33 = new City();
        $city33->setIndexnamecity('grigoriopol');
        $city33->setNamecity('Grigoriopol');
        $city33->setIndexnameregion('uta_din_stinga_nistrului');
        $city33->setNameregion('UTA din Stinga Nistrului');
        $cityDomain->persist($city33);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city34 = new City();
        $city34->setIndexnamecity('rascani');
        $city34->setNamecity('Rascani');
        $city34->setIndexnameregion('raionul_rascani');
        $city34->setNameregion('Raionul Rascani');
        $cityDomain->persist($city34);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city35 = new City();
        $city35->setIndexnamecity('glodeni');
        $city35->setNamecity('Glodeni');
        $city35->setIndexnameregion('raionul_glodeni');
        $city35->setNameregion('Raionul Glodeni');
        $cityDomain->persist($city35);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city36 = new City();
        $city36->setIndexnamecity('camenca');
        $city36->setNamecity('Camenca');
        $city36->setIndexnameregion('uta_din_stinga_nistrului');
        $city36->setNameregion('UTA din Stinga Nistrului');
        $cityDomain->persist($city36);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city37 = new City();
        $city37->setIndexnamecity('basarabeasca');
        $city37->setNamecity('Basarabeasca');
        $city37->setIndexnameregion('raionul_basarabeasca');
        $city37->setNameregion('Raionul Basarabeasca');
        $cityDomain->persist($city37);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city38 = new City();
        $city38->setIndexnamecity('leova');
        $city38->setNamecity('Leova');
        $city38->setIndexnameregion('raionul_leova');
        $city38->setNameregion('Raionul Leova');
        $cityDomain->persist($city38);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city39 = new City();
        $city39->setIndexnamecity('ocnita');
        $city39->setNamecity('Ocnita');
        $city39->setIndexnameregion('raionul_ocnita');
        $city39->setNameregion('Raionul Ocnita');
        $cityDomain->persist($city39);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city40 = new City();
        $city40->setIndexnamecity('donduseni');
        $city40->setNamecity('Donduseni');
        $city40->setIndexnameregion('raionul_donduseni');
        $city40->setNameregion('Raionul Donduseni');
        $cityDomain->persist($city40);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city41 = new City();
        $city41->setIndexnamecity('briceni');
        $city41->setNamecity('Briceni');
        $city41->setIndexnameregion('raionul_briceni');
        $city41->setNameregion('Raionul_Briceni');
        $cityDomain->persist($city41);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city42 = new City();
        $city42->setIndexnamecity('otaci');
        $city42->setNamecity('Otaci');
        $city42->setIndexnameregion('raionul_ocnita');
        $city42->setNameregion('Raionul Ocnita');
        $cityDomain->persist($city42);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city43 = new City();
        $city43->setIndexnamecity('anenii_noi');
        $city43->setNamecity('Anenii Noi');
        $city43->setIndexnameregion('raionul_anenii_noi');
        $city43->setNameregion('Raionul Anenii Noi');
        $cityDomain->persist($city43);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city44 = new City();
        $city44->setIndexnamecity('stefan_voda');
        $city44->setNamecity('Stefan Voda');
        $city44->setIndexnameregion('raionul_stefan_voda');
        $city44->setNameregion('Raionul Stefan Voda');
        $cityDomain->persist($city44);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city45 = new City();
        $city45->setIndexnamecity('cupcini');
        $city45->setNamecity('Cupcini');
        $city45->setIndexnameregion('raionul_edinet');
        $city45->setNameregion('Raionul Edinet');
        $cityDomain->persist($city45);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city46 = new City();
        $city46->setIndexnamecity('sangera');
        $city46->setNamecity('Sangera');
        $city46->setIndexnameregion('municipiul_chisinau');
        $city46->setNameregion('Municipiul Chisinau');
        $cityDomain->persist($city46);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city47 = new City();
        $city47->setIndexnamecity('criuleni');
        $city47->setNamecity('Criuleni');
        $city47->setIndexnameregion('raionul_criuleni');
        $city47->setNameregion('Raionul Criuleni');
        $cityDomain->persist($city47);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city48 = new City();
        $city48->setIndexnamecity('telenesti');
        $city48->setNamecity('Telenesti');
        $city48->setIndexnameregion('raionul_telenesti');
        $city48->setNameregion('Raionul Telenesti');
        $cityDomain->persist($city48);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city49 = new City();
        $city49->setIndexnamecity('soldanesti');
        $city49->setNamecity('Soldanesti');
        $city49->setIndexnameregion('raionul_soldanesti');
        $city49->setNameregion('Raionul Soldanesti');
        $cityDomain->persist($city49);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city50 = new City();
        $city50->setIndexnamecity('tvardita');
        $city50->setNamecity('Tvardita');
        $city50->setIndexnameregion('raionul_taraclia');
        $city50->setNameregion('Raionul Taraclia');
        $cityDomain->persist($city50);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city51 = new City();
        $city51->setIndexnamecity('lipcani');
        $city51->setNamecity('Lipcani');
        $city51->setIndexnameregion('raionul_briceni');
        $city51->setNameregion('Raionul Briceni');
        $cityDomain->persist($city51);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city52 = new City();
        $city52->setIndexnamecity('vadul_lui_voda');
        $city52->setNamecity('Vadul lui Voda');
        $city52->setIndexnameregion('municipiul_chisinau');
        $city52->setNameregion('Municipiul Chisinau');
        $cityDomain->persist($city52);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city53 = new City();
        $city53->setIndexnamecity('iargara');
        $city53->setNamecity('Iargara');
        $city53->setIndexnameregion('raionul_leova');
        $city53->setNameregion('Raionul Leova');
        $cityDomain->persist($city53);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city54 = new City();
        $city54->setIndexnamecity('cainari');
        $city54->setNamecity('Cainari');
        $city54->setIndexnameregion('raionul_causeni');
        $city54->setNameregion('Raionul Causeni');
        $cityDomain->persist($city54);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city55 = new City();
        $city55->setIndexnamecity('cantemir');
        $city55->setNamecity('Cantemir');
        $city55->setIndexnameregion('raionul_cantemir');
        $city55->setNameregion('Raionul Cantemir');
        $cityDomain->persist($city55);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city56 = new City();
        $city56->setIndexnamecity('vatra');
        $city56->setNamecity('Vatra');
        $city56->setIndexnameregion('municipiul_chisinau');
        $city56->setNameregion('Municipiul Chisinau');
        $cityDomain->persist($city56);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city57 = new City();
        $city57->setIndexnamecity('biruinta');
        $city57->setNamecity('Biruinta');
        $city57->setIndexnameregion('raionul_sangerei');
        $city57->setNameregion('Raionul Sangerei');
        $cityDomain->persist($city57);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city58 = new City();
        $city58->setIndexnamecity('crasnoe');
        $city58->setNamecity('Crasnoe');
        $city58->setIndexnameregion('uta_din_stinga_nistrului');
        $city58->setNameregion('UTA din Stinga Nistrului');
        $cityDomain->persist($city58);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city59 = new City();
        $city59->setIndexnamecity('cornesti');
        $city59->setNamecity('Cornesti');
        $city59->setIndexnameregion('raionul_ungheni');
        $city59->setNameregion('Raionul Ungheni');
        $cityDomain->persist($city59);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city60 = new City();
        $city60->setIndexnamecity('costesti');
        $city60->setNamecity('Costesti');
        $city60->setIndexnameregion('raionul_riscani');
        $city60->setNameregion('Raionul Riscani');
        $cityDomain->persist($city60);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city61 = new City();
        $city61->setIndexnamecity('marculesti');
        $city61->setNamecity('Marculesti');
        $city61->setIndexnameregion('raionul_floresti');
        $city61->setNameregion('Raionul Floresti');
        $cityDomain->persist($city61);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city62 = new City();
        $city62->setIndexnamecity('ghindesti');
        $city62->setNamecity('Ghindesti');
        $city62->setIndexnameregion('raionul_floresti');
        $city62->setNameregion('Raionul Floresti');
        $cityDomain->persist($city62);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city63 = new City();
        $city63->setIndexnamecity('tiraspolul_nou');
        $city63->setNamecity('Tiraspolul Nou');
        $city63->setIndexnameregion('uta_din_stinga_nistrului');
        $city63->setNameregion('UTA din Stinga Nistrului');
        $cityDomain->persist($city63);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city64 = new City();
        $city64->setIndexnamecity('frunza');
        $city64->setNamecity('Frunza');
        $city64->setIndexnameregion('uta_din_stinga_nistrului');
        $city64->setNameregion('UTA din Stinga Nistrului');
        $cityDomain->persist($city64);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city65 = new City();
        $city65->setIndexnamecity('bucovat');
        $city65->setNamecity('Bucovat');
        $city65->setIndexnameregion('raionul_straseni');
        $city65->setNameregion('Raionul Straseni');
        $cityDomain->persist($city65);

        $cityDomain = $domainManager->get(CityDomain::class);
        $city66 = new City();
        $city66->setIndexnamecity('maiac');
        $city66->setNamecity('Maiac');
        $city66->setIndexnameregion('uta_din_stinga_nistrului');
        $city66->setNameregion('UTA din Stinga Nistrului');
        $cityDomain->persist($city66);

        $cityDomain->flush();
    }
}