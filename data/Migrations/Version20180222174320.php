<?php

namespace Migrations;

use Application\Service\Doctrine\AbstractMigration;
use Application\Service\DomainModelManager;
use Application\Entity\User;
use Application\Model\UserDomain;
use Acl\Model\RoleDomain;


/**
 * Class Version20180222174320
 * @package Migrations
 */
class Version20180222174320 extends AbstractMigration
{
    /**
     * @param DomainModelManager $domainManager
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function seed( DomainModelManager $domainManager)
    {
        /** @var UserDomain $userDomain */
        $userDomain = $domainManager->get(UserDomain::class);
        $roleDomain = $domainManager->get(RoleDomain::class);

        $someRole = $roleDomain->findOneBy(['name' => 'Administrator']);
        $user = new User();
        $user->setIcon('/img/user_admin.png');
        $user->setName('Administrator');
        $user->setIndexName('administrator');
        $user->setLogin('Adminuser');
        $user->setEmail('admin@email.com');
        $user->setPassword('$2y$10$FYJp7nCgnWxE3sf83FFAwe8hC108ex3YZeBIHC2ZMBwCxvfxSKdhu');
        $user->setRole($someRole);
        $user->setActive('1');
        $user->setLanguage('ro_RO');
        $userDomain->persist($user);

        $someRole2 = $roleDomain->findOneBy(['name' => 'Owner']);
        $user2 = new User();
        $user2->setIcon('/img/user_owner.jpeg');
        $user2->setName('Owner');
        $user2->setIndexName('owner');
        $user2->setLogin('Owneruser');
        $user2->setEmail('owner@email.com');
        $user2->setPassword('$2y$10$GKFv4FQcwsIa/aCML2ZOD.bMM22aaT4pbmmBGKDMlHyaqae07NfPC');
        $user2->setRole($someRole2);
        $user2->setActive('1');
        $user2->setLanguage('ro_RO');
        $userDomain->persist($user2);

        $someRole3 = $roleDomain->findOneBy(['name' => 'Agency']);
        $user3 = new User();
        $user3->setIcon('/img/user_agency.png');
        $user3->setName('Agency');
        $user3->setIndexName('agency');
        $user3->setLogin('Agencyuser');
        $user3->setEmail('agency@email.com');
        $user3->setPassword('$2y$10$eM3HMniTMOmHP3lghIRHx.c6Or/IpLC.y5o1l9zzDLa3Ewj9cbzZy');
        $user3->setRole($someRole3);
        $user3->setActive('1');
        $user3->setLanguage('ro_RO');
        $userDomain->persist($user3);

        $someRole4 = $roleDomain->findOneBy(['name' => 'Customer']);
        $user4 = new User();
        $user4->setIcon('/img/user_admin.png');
        $user4->setName('Customer');
        $user4->setIndexName('customer');
        $user4->setLogin('Customeruser');
        $user4->setEmail('customer@email.com');
        $user4->setPassword('$2y$10$7y.U74aKS6M5GKAO0uRf1.7ePZ51vlLA5zHHxBuEccxRQ1NegRRl6');
        $user4->setRole($someRole4);
        $user4->setActive('1');
        $user4->setLanguage('ro_RO');
        $userDomain->persist($user4);

        $someRole5 = $roleDomain->findOneBy(['name' => 'Guest']);
        $user5 = new User();
        $user5->setIcon('/img/user_default_icon.png');
        $user5->setName('Guest');
        $user5->setIndexName('guest');
        $user5->setLogin('Guestuser');
        $user5->setEmail('stranger@email.com');
        $user5->setPassword('$2y$10$QGPpN263L6jnZy6r34/H/uR.2wypJI9DcIx7qp5KD6Zj8ZZtQbQpW');
        $user5->setRole($someRole5);
        $user5->setActive('1');
        $user5->setLanguage('ro_RO');
        $userDomain->persist($user5);

        $userDomain->flush();
    }
}

