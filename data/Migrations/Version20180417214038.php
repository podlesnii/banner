<?php

namespace Migrations;

use Application\Entity\Image;
use Application\Model\BannerDomain;
use Application\Service\Doctrine\AbstractMigration;
use Application\Service\DomainModelManager;


/**
 * Class Version20180417214038
 * @package Migrations
 */
class Version20180417214038 extends AbstractMigration
{

    /**
     * @param DomainModelManager $domainManager
     * @return mixed|void
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     */
    public function seed(DomainModelManager $domainManager)
    {

        /** @var BannerDomain $bannersDomain */
        $bannersDomain = $domainManager->get(BannerDomain::class);

        $someBanner = $bannersDomain->findOneBy(['description' => 'Щит размера 3м X 6м для размещения наружной рекламы, расположенный вдоль 
        улиц и трасс. Имеет две стороны: А — обращен к нам лицом (в том случае, если рекламный щит справа). Б — обращен 
        к нам лицом ( в том случае, если расположен слева).']);
        $image = new Image();
        $image->setPath('/img/banner-photo/banner_01.jpg');
        $image->setTitle('banner01');
        $image->setBanner($someBanner);
        $bannersDomain->persist($image);


        $someBanner2 = $bannersDomain->findOneBy(['description' => 'Конструкции с форматом рекламного поля от 3м Х 12м до 5м Х 15м.']);
        $image2 = new Image();
        $image2->setPath('/img/banner-photo/banner_02.jpg');
        $image2->setTitle('banner01');
        $image2->setBanner($someBanner2);
        $bannersDomain->persist($image2);


        $someBanner3 = $bannersDomain->findOneBy(['description' => 'К цифровым баннерам относятся рекламные конструкции, трансляция изображений 
        на которых осуществляется цифровым способом и не требует печати плаката. К таким относятся стандартные и большие 
        баннеры, брандмауэры и ситиборды.']);
        $image3 = new Image();
        $image3->setPath('/img/banner-photo/banner_03.jpg');
        $image3->setTitle('banner01');
        $image3->setBanner($someBanner3);
        $bannersDomain->persist($image3);


        $someBanner4 = $bannersDomain->findOneBy(['description' => 'Рекламные поверхности площадью более 18 кв.м. на стенах зданий.']);
        $image4 = new Image();
        $image4->setPath('/img/banner-photo/banner_04.jpg');
        $image4->setTitle('banner01');
        $image4->setBanner($someBanner4);
        $bannersDomain->persist($image4);


        $someBanner5 = $bannersDomain->findOneBy(['description' => 'Двухстороннее полотно с рекламой, расположено обычно над проезжей частью 
        посредством тросовой конструкции.']);
        $image5 = new Image();
        $image5->setPath('/img/banner-photo/banner_05.jpg');
        $image5->setTitle('banner01');
        $image5->setBanner($someBanner5);
        $bannersDomain->persist($image5);


        $someBanner6 = $bannersDomain->findOneBy(['description' => 'Отдельно стоящие конструкции (обычно 3,7м х 2,5м). Могут быть размещены 
        как наружно так и в помещении: наклейки на транспортных средствах, часть конструкции остановок общественного 
        транспорта, непосредственно в транспортных средствах и др.']);
        $image6 = new Image();
        $image6->setPath('/img/banner-photo/banner_06.jpg');
        $image6->setTitle('banner01');
        $image6->setBanner($someBanner6);
        $bannersDomain->persist($image6);


        $someBanner7 = $bannersDomain->findOneBy(['description' => 'Баннеры имеющие иные характеристики (размеры, форма и т.п.)']);
        $image7 = new Image();
        $image7->setPath('/img/banner-photo/banner_07.jpg');
        $image7->setTitle('banner01');
        $image7->setBanner($someBanner7);
        $bannersDomain->persist($image7);


        $bannersDomain->flush();

    }

}

