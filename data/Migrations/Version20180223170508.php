<?php

namespace Migrations;

use Application\Service\Doctrine\AbstractMigration;
use Application\Service\DomainModelManager;
use Application\Entity\BannerCategory;
use Application\Model\BannerCategoryDomain;


/**
 * Class Version20180223170508
 * @package Migrations
 */
class Version20180223170508 extends AbstractMigration
{
    /**
     * @param DomainModelManager $domainManager
     */
    public function seed(DomainModelManager $domainManager)
    {
        $bannerCategoriesDomain = $domainManager->get(BannerCategoryDomain::class);

        $bannerCategories = new BannerCategory();
        $bannerCategories->setIndexname('standard_banner');
        $bannerCategories->setName('Stangard billboard');
        $bannerCategories->setDescription('');
        $bannerCategories->setImage('');
        $bannerCategoriesDomain->persist($bannerCategories);

        $bannerCategories2 = new BannerCategory();
        $bannerCategories2->setIndexname('big_banner');
        $bannerCategories2->setName('Big billboard');
        $bannerCategories2->setDescription('');
        $bannerCategories2->setImage('');
        $bannerCategoriesDomain->persist($bannerCategories2);

        $bannerCategories3 = new BannerCategory();
        $bannerCategories3->setIndexname('digital_banner');
        $bannerCategories3->setName('Digital billboard');
        $bannerCategories3->setDescription('');
        $bannerCategories3->setImage('');
        $bannerCategoriesDomain->persist($bannerCategories3);

        $bannerCategories4 = new BannerCategory();
        $bannerCategories4->setIndexname('brandmauer');
        $bannerCategories4->setName('Brandmauer');
        $bannerCategories4->setDescription('');
        $bannerCategories4->setImage('');
        $bannerCategoriesDomain->persist($bannerCategories4);

        $bannerCategories5 = new BannerCategory();
        $bannerCategories5->setIndexname('bracing');
        $bannerCategories5->setName('Bracing');
        $bannerCategories5->setDescription('');
        $bannerCategories5->setImage('');
        $bannerCategoriesDomain->persist($bannerCategories5);

        $bannerCategories6 = new BannerCategory();
        $bannerCategories6->setIndexname('citybord');
        $bannerCategories6->setName('Citybord');
        $bannerCategories6->setDescription('');
        $bannerCategories6->setImage('');
        $bannerCategoriesDomain->persist($bannerCategories6);

        $bannerCategories7 = new BannerCategory();
        $bannerCategories7->setIndexname('another');
        $bannerCategories7->setName('Another');
        $bannerCategories7->setDescription('');
        $bannerCategories7->setImage('');
        $bannerCategoriesDomain->persist($bannerCategories7);

        $bannerCategoriesDomain->flush();
    }
}
