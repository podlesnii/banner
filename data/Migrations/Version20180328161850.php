<?php

namespace Migrations;



use Application\Service\Doctrine\AbstractMigration;
use Application\Service\DomainModelManager;
use Application\Entity\Banner;
use Application\Model\BannerDomain;
use Application\Model\BannerCategoryDomain;
use Application\Model\CityDomain;
/**
 * Class Version20180328161850
 * @package Migrations
 */
class Version20180328161850 extends AbstractMigration
{
    /**
     * @param DomainModelManager $domainManager
     * @return mixed|void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function seed(DomainModelManager $domainManager)
    {
        /** @var BannerDomain $bannersDomain */
        $bannersDomain = $domainManager->get(BannerDomain::class);
        $bannerCategoriesDomain = $domainManager->get(BannerCategoryDomain::class);
        $bannerCityDomain = $domainManager->get(CityDomain::class);

        $someCategory = $bannerCategoriesDomain->findOneBy(['indexname' => 'standard_banner']);
        $someCity = $bannerCityDomain->findOneBy(['nameCity' => 'Chisinau']);
        $banner = new Banner();
        $banner->setDescription('Щит размера 3м X 6м для размещения наружной рекламы, расположенный вдоль 
        улиц и трасс. Имеет две стороны: А — обращен к нам лицом (в том случае, если рекламный щит справа). Б — обращен 
        к нам лицом ( в том случае, если расположен слева).');
        $banner->setCity($someCity);
        $banner->setArea('Botanica');
        $banner->setAdress('Decebal 99');
        $banner->setCategory($someCategory);
        $banner->setLongitude('28.8322534');
        $banner->setLatitude('47.0244707');
        $banner->setBannerNumber('BannerName1 ');
        $bannersDomain->persist($banner);

        $someCategory2 = $bannerCategoriesDomain->findOneBy(['indexname' => 'big_banner']);
        $someCity2 = $bannerCityDomain->findOneBy(['nameCity' => 'balti']);
        $banner2 = new Banner();
        $banner2->setDescription('Конструкции с форматом рекламного поля от 3м Х 12м до 5м Х 15м.');
        $banner2->setCity($someCity2);
        $banner2->setArea('Riscanovca');
        $banner2->setAdress('bd. Moscova 11/1');
        $banner2->setCategory($someCategory2);
        $banner2->setLongitude('28.8322734');
        $banner2->setLatitude('47.0244107');
        $banner2->setBannerNumber('BannerName 2');
        $bannersDomain->persist($banner2);

        $someCategory3 = $bannerCategoriesDomain->findOneBy(['indexname' => 'digital_banner']);
        $someCity3 = $bannerCityDomain->findOneBy(['nameCity' => 'cahul']);
        $banner3 = new Banner();
        $banner3->setDescription('К цифровым баннерам относятся рекламные конструкции, трансляция изображений 
        на которых осуществляется цифровым способом и не требует печати плаката. К таким относятся стандартные и большие 
        баннеры, брандмауэры и ситиборды.');
        $banner3->setCity($someCity3);
        $banner3->setArea('Centru');
        $banner3->setAdress('Stefan Cel Mare');
        $banner3->setCategory($someCategory3);
        $banner3->setLongitude('28.8342534');
        $banner3->setLatitude('47.0264707');
        $banner3->setBannerNumber('BannerName 3');
        $bannersDomain->persist($banner3);

        $someCategory4 = $bannerCategoriesDomain->findOneBy(['indexname' => 'brandmauer']);
        $someCity4 = $bannerCityDomain->findOneBy(['nameCity' => 'orhei']);
        $banner4 = new Banner();
        $banner4->setDescription('Рекламные поверхности площадью более 18 кв.м. на стенах зданий.');
        $banner4->setCity($someCity4);
        $banner4->setArea('Centru');
        $banner4->setAdress('Stefan Cel Mare');
        $banner4->setCategory($someCategory4);
        $banner4->setLongitude('28.872534');
        $banner4->setLatitude('47.0744707');
        $banner4->setBannerNumber('BannerName 4');
        $bannersDomain->persist($banner4);

        $someCategory5 = $bannerCategoriesDomain->findOneBy(['indexname' => 'bracing']);
        $someCity5 = $bannerCityDomain->findOneBy(['nameCity' => 'comrat']);
        $banner5 = new Banner();
        $banner5->setDescription('Двухстороннее полотно с рекламой, расположено обычно над проезжей частью 
        посредством тросовой конструкции.');
        $banner5->setCity($someCity5);
        $banner5->setArea('Centru');
        $banner5->setAdress('Stefan Cel Mare');
        $banner5->setCategory($someCategory5);
        $banner5->setLongitude('28.8392534');
        $banner5->setLatitude('47.0249707');
        $banner5->setBannerNumber('BannerName 5');
        $bannersDomain->persist($banner5);

        $someCategory6 = $bannerCategoriesDomain->findOneBy(['indexname' => 'citybord']);
        $someCity6 = $bannerCityDomain->findOneBy(['nameCity' => 'vulcanesti']);
        $banner6 = new Banner();
        $banner6->setDescription('Отдельно стоящие конструкции (обычно 3,7м х 2,5м). Могут быть размещены 
        как наружно так и в помещении: наклейки на транспортных средствах, часть конструкции остановок общественного 
        транспорта, непосредственно в транспортных средствах и др.');
        $banner6->setCity($someCity6);
        $banner6->setArea('Centru');
        $banner6->setAdress('Stefan Cel Mare');
        $banner6->setCategory($someCategory6);
        $banner6->setLongitude('28.8382534');
        $banner6->setLatitude('47.0214707');
        $banner6->setBannerNumber('BannerName 6');
        $bannersDomain->persist($banner6);

        $someCategory7 = $bannerCategoriesDomain->findOneBy(['indexname' => 'another']);
        $someCity7 = $bannerCityDomain->findOneBy(['nameCity' => 'ialoveni']);
        $banner7 = new Banner();
        $banner7->setDescription('Баннеры имеющие иные характеристики (размеры, форма и т.п.)');
        $banner7->setCity($someCity7);
        $banner7->setArea('Buiucani');
        $banner7->setAdress('Alba Iuia 7');
        $banner7->setCategory($someCategory7);
        $banner7->setLongitude('28.8322334');
        $banner7->setLatitude('47.02440707');
        $banner7->setBannerNumber('BannerName 7');
        $bannersDomain->persist($banner7);


        $bannersDomain->flush();

    }
}