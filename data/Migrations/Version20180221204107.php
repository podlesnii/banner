<?php

namespace Migrations;

use Application\Service\Doctrine\AbstractMigration;
use Application\Service\DomainModelManager;
use Acl\Entity\Role;
use Acl\Model\RoleDomain;


/**
 * Class Version20180221204107
 * @package Migrations
 */
class Version20180221204107 extends AbstractMigration
{
    /**
     * @param DomainModelManager $domainManager
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function seed( DomainModelManager $domainManager)
    {
        /** @var RoleDomain $roleDomain */
        $roleDomain = $domainManager->get(RoleDomain::class);

        $role = new Role();
        $role->setName('Administrator');
        $role->setDescription('The Administrator can manage everything on the site except removing banners groups or existing user roles. That group have access to crud options via Admin panel.');
        $roleDomain->persist($role);

        $role2 = new Role();
        $role2->setName('Owner');
        $role2->setDescription('Owner can manage their banners (create/delete/update), share their privileges for agencies.');

        $roleDomain->persist($role2);

        $role3 = new Role();
        $role3->setName('Agency');
        $role3->setDescription('Agency can manage their banner or banners which was shared for them.');
        $roleDomain->persist($role3);

        $role4 = new Role();
        $role4->setName('Customer');
        $role4->setDescription('Customer is the regular user which was registered on the site. The customer present the most group of users. They could rent(book) banners');
        $roleDomain->persist($role4);

        $role5 = new Role();
        $role5->setName('Guest');
        $role5->setDescription('Guest is the regular user which was registered (or not) on the site. The guest present the most group of users. All of that group is potential clients of provided services.');
        $roleDomain->persist($role5);

        $roleDomain->flush();
    }
}

