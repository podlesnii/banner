<?php

namespace Migrations;

use Acl\Entity\Privilege;
use Acl\Entity\Resource;
use Acl\Entity\Role;
use Acl\Model\PrivilegeDomain;
use Acl\Model\ResourceDomain;
use Acl\Model\RoleDomain;
use Application\Service\Doctrine\AbstractMigration;
use Application\Service\DomainModelManager;
use Zend\Permissions\Acl\Acl;


/**
 * Class Version20180521132643
 * @package Migrations
 */
class Version20180521132643 extends AbstractMigration
{


    /**
     * @param DomainModelManager $domainManager
     * @return mixed|void
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function seed(DomainModelManager $domainManager)
    {
        $aclRules = [
            'Administrator' => [
                'Admin\Controller\BannersController:upload' => Acl::TYPE_ALLOW,
            ],

            'Agency' => [
                'Admin\Controller\BannersController:upload' => Acl::TYPE_ALLOW,
            ],

            'Owner' => [
                'Admin\Controller\BannersController:upload' => Acl::TYPE_ALLOW,
            ],
        ];


        /** @var RoleDomain $roleDomain */
        $roleDomain = $domainManager->get(RoleDomain::class);

        /** @var ResourceDomain $resourceDomain */
        $resourceDomain = $domainManager->get(ResourceDomain::class);

        /** @var PrivilegeDomain $privilegeDomain */
        $privilegeDomain = $domainManager->get(PrivilegeDomain::class);

        $allResources = $resourceDomain->findAll();

        foreach ($aclRules as $roleName => $rules) {
            /** @var Role $roleEntity */
            $roleEntity = $roleDomain->findOneBy(['name' => $roleName]);
            foreach ($rules as $resourceName => $privilege) {
                $resourceEntity = null;
                /** @var \Acl\Entity\Resource $entity */
                foreach ($allResources as $entity) {
                    if ($entity->getName() == $resourceName) {
                        $resourceEntity = $entity;
                        break;
                    }
                }
                if (is_null($resourceEntity)) {
                    $resourceEntity = new Resource();
                    $resourceEntity->setName($resourceName);
                    $allResources[] = $resourceEntity;
                    $resourceDomain->persist($resourceEntity);
                }
                $privilegeEntity = new Privilege();

                $privilegeEntity->setResource($resourceEntity);
                $privilegeEntity->setRole($roleEntity);
                $privilegeEntity->setPermissions($privilege);

                $privilegeDomain->persist($privilegeEntity);
            }
        }

        $privilegeDomain->flush();
    }

}

