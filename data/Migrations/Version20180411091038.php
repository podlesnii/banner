<?php

namespace Migrations;

use Acl\Entity\Privilege;
use Acl\Entity\Resource;
use Acl\Entity\Role;
use Acl\Model\PrivilegeDomain;
use Acl\Model\ResourceDomain;
use Acl\Model\RoleDomain;
use Application\Service\Doctrine\AbstractMigration;
use Application\Service\DomainModelManager;
use Zend\Permissions\Acl\Acl;


/**
 * Class Version20180411091038
 * @package Migrations
 */
class Version20180411091038 extends AbstractMigration
{

        /**
     * @param DomainModelManager $domainManager
     * @return mixed|void
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function seed(DomainModelManager $domainManager)
    {
        $aclRules = [
            'Guest' => [
                'Application\Controller\IndexController:index'       => Acl::TYPE_ALLOW,
                'Application\Controller\SearchController:index'      => Acl::TYPE_ALLOW,

                //FE or/and Admin -> logout
                'Application\Controller\AuthController:logout'       => Acl::TYPE_ALLOW,
                'Application\Controller\AuthController:login'        => Acl::TYPE_ALLOW,
                'Application\Controller\AuthController:register'     => Acl::TYPE_ALLOW,
                'Application\Controller\AuthController:success'      => Acl::TYPE_ALLOW,
                'Application\Controller\AuthController:confirmation' => Acl::TYPE_ALLOW,
            ],
            'Agency' => [
                'Application\Controller\IndexController:index'       => Acl::TYPE_ALLOW,
                'Application\Controller\SearchController:index'      => Acl::TYPE_ALLOW,

                //FE or/and Admin -> logout
                'Application\Controller\AuthController:logout'       => Acl::TYPE_ALLOW,
                'Application\Controller\AuthController:login'        => Acl::TYPE_ALLOW,

                'Application\Controller\ProfileController:profile'   => Acl::TYPE_ALLOW,
                'Application\Controller\ProfileController:email'     => Acl::TYPE_ALLOW,
                'Application\Controller\ProfileController:name'     => Acl::TYPE_ALLOW,
                'Application\Controller\ProfileController:password'  => Acl::TYPE_ALLOW,
                'Application\Controller\ProfileController:icon'      => Acl::TYPE_ALLOW,

            ],
            'Owner' => [
                'Application\Controller\IndexController:index'       => Acl::TYPE_ALLOW,
                'Application\Controller\SearchController:index'      => Acl::TYPE_ALLOW,

                //FE or/and Admin -> logout
                'Application\Controller\AuthController:logout'       => Acl::TYPE_ALLOW,
                'Application\Controller\AuthController:login'        => Acl::TYPE_ALLOW,

                'Application\Controller\ProfileController:profile'   => Acl::TYPE_ALLOW,
                'Application\Controller\ProfileController:email'     => Acl::TYPE_ALLOW,
                'Application\Controller\ProfileController:name'     => Acl::TYPE_ALLOW,
                'Application\Controller\ProfileController:password'  => Acl::TYPE_ALLOW,
                'Application\Controller\ProfileController:icon'      => Acl::TYPE_ALLOW,

                //Admin -> Banners CRUD
                'Admin\Controller\BannersController:index'            => Acl::TYPE_ALLOW,
                'Admin\Controller\BannersController:list'             => Acl::TYPE_ALLOW,
                'Admin\Controller\BannersController:add'              => Acl::TYPE_ALLOW,
                'Admin\Controller\BannersController:update'           => Acl::TYPE_ALLOW,
                'Admin\Controller\BannersController:delete'           => Acl::TYPE_ALLOW,
            ],
            'Administrator' => [
                'Application\Controller\IndexController:index'       => Acl::TYPE_ALLOW,
                'Application\Controller\SearchController:index'      => Acl::TYPE_ALLOW,

                //FE or/and Admin -> logout
                'Application\Controller\AuthController:logout'       => Acl::TYPE_ALLOW,
                'Application\Controller\AuthController:login'        => Acl::TYPE_ALLOW,

                //FE or/and Admin -> Profile
                'Application\Controller\ProfileController:profile'   => Acl::TYPE_ALLOW,
                'Application\Controller\ProfileController:email'     => Acl::TYPE_ALLOW,
                'Application\Controller\ProfileController:name'     => Acl::TYPE_ALLOW,
                'Application\Controller\ProfileController:password'  => Acl::TYPE_ALLOW,
                'Application\Controller\ProfileController:icon'      => Acl::TYPE_ALLOW,

                //Admin -> BannerCategories CRUD
                'Admin\Controller\BannerCategoryController:index'    => Acl::TYPE_ALLOW,
                'Admin\Controller\BannerCategoryController:list'     => Acl::TYPE_ALLOW,
                'Admin\Controller\BannerCategoryController:add'      => Acl::TYPE_ALLOW,
                'Admin\Controller\BannerCategoryController:update'   => Acl::TYPE_ALLOW,
                'Admin\Controller\BannerCategoryController:delete'   => Acl::TYPE_ALLOW,

                //Admin -> Banners CRUD
                'Admin\Controller\BannersController:index'            => Acl::TYPE_ALLOW,
                'Admin\Controller\BannersController:list'             => Acl::TYPE_ALLOW,
                'Admin\Controller\BannersController:add'              => Acl::TYPE_ALLOW,
                'Admin\Controller\BannersController:update'           => Acl::TYPE_ALLOW,
                'Admin\Controller\BannersController:delete'           => Acl::TYPE_ALLOW,

                //Admin -> Cities CRUD
                'Admin\Controller\CityController:index'              => Acl::TYPE_ALLOW,
                'Admin\Controller\CityController:list'               => Acl::TYPE_ALLOW,
                'Admin\Controller\CityController:add'                => Acl::TYPE_ALLOW,
                'Admin\Controller\CityController:update'             => Acl::TYPE_ALLOW,
                'Admin\Controller\CityController:delete'             => Acl::TYPE_ALLOW,

                //Admin -> General Page
                'Admin\Controller\IndexController:index'             => Acl::TYPE_ALLOW,

                //Admin -> Roles CRUD
                'Admin\Controller\RolesController:index'              => Acl::TYPE_ALLOW,
                'Admin\Controller\RolesController:list'               => Acl::TYPE_ALLOW,
                'Admin\Controller\RolesController:update'             => Acl::TYPE_ALLOW,

                //Admin -> Users CRUD
                'Admin\Controller\UsersController:index'             => Acl::TYPE_ALLOW,
                'Admin\Controller\UsersController:list'              => Acl::TYPE_ALLOW,
                'Admin\Controller\UsersController:add'               => Acl::TYPE_ALLOW,
                'Admin\Controller\UsersController:update'            => Acl::TYPE_ALLOW,
                'Admin\Controller\UsersController:delete'            => Acl::TYPE_ALLOW,
            ],
        ];

        /** @var RoleDomain $roleDomain */
        $roleDomain = $domainManager->get(RoleDomain::class);

        /** @var ResourceDomain $resourceDomain */
        $resourceDomain = $domainManager->get(ResourceDomain::class);

        /** @var PrivilegeDomain $privilegeDomain */
        $privilegeDomain = $domainManager->get(PrivilegeDomain::class);

        $allResources = $resourceDomain->findAll();
        
        foreach ($aclRules as $roleName => $rules) {
            /** @var Role $roleEntity */
            $roleEntity = $roleDomain->findOneBy(['name' => $roleName]);
            foreach ($rules as $resourceName => $privilege) {
                $resourceEntity = null;
                /** @var \Acl\Entity\Resource $entity */
                foreach ($allResources as $entity) {
                    if ($entity->getName() == $resourceName) {
                        $resourceEntity = $entity;
                        break;
                    }
                }
                if (is_null($resourceEntity)) {
                    $resourceEntity = new Resource();
                    $resourceEntity->setName($resourceName);
                    $allResources[] = $resourceEntity;
                    $resourceDomain->persist($resourceEntity);
                }
                $privilegeEntity = new Privilege();

                $privilegeEntity->setResource($resourceEntity);
                $privilegeEntity->setRole($roleEntity);
                $privilegeEntity->setPermissions($privilege);

                $privilegeDomain->persist($privilegeEntity);
            }
        }

        $privilegeDomain->flush();
    }

}

