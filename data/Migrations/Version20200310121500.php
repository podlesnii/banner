<?php

namespace Migrations;

use Application\Entity\Banner;
use Application\Model\BannerCategoryDomain;
use Application\Model\BannerDomain;
use Application\Model\CityDomain;
use Application\Model\UserDomain;
use Application\Service\Doctrine\AbstractMigration;
use Application\Service\DomainModelManager;


/**
 * Class Version20200310121500
 * @package Migrations
 */
class Version20200310121500 extends AbstractMigration
{

    /**
     * @param DomainModelManager $domainManager
     * @return mixed|void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function seed(DomainModelManager $domainManager)
    {
        /** @var BannerDomain $bannersDomain */
        $bannersDomain = $domainManager->get(BannerDomain::class);
        $bannerCategoriesDomain = $domainManager->get(BannerCategoryDomain::class);
        $bannerCityDomain = $domainManager->get(CityDomain::class);
        $bannerUserIds = $domainManager->get(UserDomain::class);

        $someCategory = $bannerCategoriesDomain->findOneBy(['indexname' => 'standard_banner']);
        $someCity = $bannerCityDomain->findOneBy(['nameCity' => 'Chisinau']);
        $bannerUser = $bannerUserIds->findOneBy(['name' => 'Administrator']);
        $banner = new Banner();
        $banner->setDescription('Kaufland');
        $banner->setCity($someCity);
        $banner->setArea('Botanica');
        $banner->setAdress('Decebal 99/5');
        $banner->setCategory($someCategory);
        $banner->setLatitude('46.991086');
        $banner->setLongitude('28.858987');
        $banner->setUserId($bannerUser);
        $banner->setBannerNumber('BannerName 15');
        $bannersDomain->persist($banner);

        $someCategory2 = $bannerCategoriesDomain->findOneBy(['indexname' => 'big_banner']);
        $someCity2 = $bannerCityDomain->findOneBy(['nameCity' => 'balti']);
        $bannerUser2 = $bannerUserIds->findOneBy(['name' => 'Administrator']);
        $banner2 = new Banner();
        $banner2->setDescription('Конструкции с форматом рекламного поля от 3м Х 12м до 5м Х 15м.');
        $banner2->setCity($someCity2);
        $banner2->setArea('Riscanovca');
        $banner2->setAdress('bd. Moscova 12/2');
        $banner2->setCategory($someCategory2);
        $banner2->setLatitude('47.0517754');
        $banner2->setLongitude('28.863631');
        $banner2->setBannerNumber('BannerName 16');
        $banner2->setUserId($bannerUser2);
        $bannersDomain->persist($banner2);

        $someCategory3 = $bannerCategoriesDomain->findOneBy(['indexname' => 'digital_banner']);
        $someCity3 = $bannerCityDomain->findOneBy(['nameCity' => 'cahul']);
        $bannerUser3 = $bannerUserIds->findOneBy(['name' => 'Owner']);
        $banner3 = new Banner();
        $banner3->setDescription('К цифровым баннерам относятся рекламные конструкции, трансляция изображений 
        на которых осуществляется цифровым способом и не требует печати плаката. К таким относятся стандартные и большие 
        баннеры, брандмауэры и ситиборды.');
        $banner3->setCity($someCity3);
        $banner3->setArea('Centru');
        $banner3->setAdress('Stefan Cel Mare 5');
        $banner3->setCategory($someCategory3);
        $banner3->setLatitude('45.907164');
        $banner3->setLongitude('28.189473');
        $banner3->setBannerNumber('BannerName 17');
        $banner3->setUserId($bannerUser3);
        $bannersDomain->persist($banner3);

        $someCategory4 = $bannerCategoriesDomain->findOneBy(['indexname' => 'brandmauer']);
        $someCity4 = $bannerCityDomain->findOneBy(['nameCity' => 'orhei']);
        $bannerUser4 = $bannerUserIds->findOneBy(['name' => 'Owner']);
        $banner4 = new Banner();
        $banner4->setDescription('Рекламные поверхности площадью более 18 кв.м. на стенах зданий.');
        $banner4->setCity($someCity4);
        $banner4->setArea('Centru');
        $banner4->setAdress('str. Vasile Lupu 8');
        $banner4->setCategory($someCategory4);
        $banner4->setLatitude('47.374985');
        $banner4->setLongitude('28.822449');
        $banner4->setBannerNumber('BannerName 18');
        $banner4->setUserId($bannerUser4);
        $bannersDomain->persist($banner4);

        $someCategory5 = $bannerCategoriesDomain->findOneBy(['indexname' => 'bracing']);
        $someCity5 = $bannerCityDomain->findOneBy(['nameCity' => 'comrat']);
        $bannerUser5 = $bannerUserIds->findOneBy(['name' => 'Owner']);
        $banner5 = new Banner();
        $banner5->setDescription('Двухстороннее полотно с рекламой, расположено обычно над проезжей частью 
        посредством тросовой конструкции.');
        $banner5->setCity($someCity5);
        $banner5->setArea('Centru');
        $banner5->setAdress('Lenina 1');
        $banner5->setCategory($someCategory5);
        $banner5->setLatitude('46.295901');
        $banner5->setLongitude('28.65794');
        $banner5->setBannerNumber('BannerName 19');
        $banner5->setUserId($bannerUser5);
        $bannersDomain->persist($banner5);

        $someCategory6 = $bannerCategoriesDomain->findOneBy(['indexname' => 'citybord']);
        $someCity6 = $bannerCityDomain->findOneBy(['nameCity' => 'vulcanesti']);
        $bannerUser6 = $bannerUserIds->findOneBy(['name' => 'Owner']);
        $banner6 = new Banner();
        $banner6->setDescription('Отдельно стоящие конструкции (обычно 3,7м х 2,5м). Могут быть размещены 
        как наружно так и в помещении: наклейки на транспортных средствах, часть конструкции остановок общественного 
        транспорта, непосредственно в транспортных средствах и др.');
        $banner6->setCity($someCity6);
        $banner6->setArea('Centru');
        $banner6->setAdress('str. Lenin 15');
        $banner6->setCategory($someCategory6);
        $banner6->setLatitude('45.682851,');
        $banner6->setLongitude('28.404149');
        $banner6->setBannerNumber('BannerName 20');
        $banner6->setUserId($bannerUser6);
        $bannersDomain->persist($banner6);

        $someCategory7 = $bannerCategoriesDomain->findOneBy(['indexname' => 'another']);
        $someCity7 = $bannerCityDomain->findOneBy(['nameCity' => 'ialoveni']);
        $bannerUser7 = $bannerUserIds->findOneBy(['name' => 'Agency']);
        $banner7 = new Banner();
        $banner7->setDescription('Баннеры имеющие иные характеристики (размеры, форма и т.п.)');
        $banner7->setCity($someCity7);
        $banner7->setArea('Buiucani');
        $banner7->setAdress('str. Basarabiei 8');
        $banner7->setCategory($someCategory7);
        $banner7->setLatitude('46.942289');
        $banner7->setLongitude('28.790689');
        $banner7->setBannerNumber('BannerName 21');
        $banner7->setUserId($bannerUser7);
        $bannersDomain->persist($banner7);

        $someCategory8 = $bannerCategoriesDomain->findOneBy(['indexname' => 'bracing']);
        $someCity8 = $bannerCityDomain->findOneBy(['nameCity' => 'comrat']);
        $bannerUser8 = $bannerUserIds->findOneBy(['name' => 'Agency']);
        $banner8 = new Banner();
        $banner8->setDescription('Двухстороннее полотно с рекламой, расположено обычно над проезжей частью 
        посредством тросовой конструкции.');
        $banner8->setCity($someCity8);
        $banner8->setArea('Centru');
        $banner8->setAdress('Lenina 17');
        $banner8->setCategory($someCategory8);
        $banner8->setLatitude('46.298365');
        $banner8->setLongitude('28.655926');
        $banner8->setBannerNumber('BannerName 22');
        $banner8->setUserId($bannerUser8);
        $bannersDomain->persist($banner8);

        $someCategory9 = $bannerCategoriesDomain->findOneBy(['indexname' => 'bracing']);
        $someCity9 = $bannerCityDomain->findOneBy(['nameCity' => 'comrat']);
        $bannerUser9 = $bannerUserIds->findOneBy(['name' => 'Agency']);
        $banner9 = new Banner();
        $banner9->setDescription('Двухстороннее полотно с рекламой, расположено обычно над проезжей частью 
        посредством тросовой конструкции.');
        $banner9->setCity($someCity9);
        $banner9->setArea('Centru');
        $banner9->setAdress('strada Lenin, 5B');
        $banner9->setCategory($someCategory9);
        $banner9->setLatitude('46.294951');
        $banner9->setLongitude('28.65534');
        $banner9->setBannerNumber('BannerName 23');
        $banner9->setUserId($bannerUser9);
        $bannersDomain->persist($banner9);

        $someCategory10 = $bannerCategoriesDomain->findOneBy(['indexname' => 'standard_banner']);
        $someCity10 = $bannerCityDomain->findOneBy(['nameCity' => 'Chisinau']);
        $bannerUser10 = $bannerUserIds->findOneBy(['name' => 'Agency']);
        $banner10 = new Banner();
        $banner10->setDescription('Cityboard.');
        $banner10->setCity($someCity10);
        $banner10->setArea('Botanica');
        $banner10->setAdress('bd. Decebal 74');
        $banner10->setCategory($someCategory10);
        $banner10->setLatitude('46.987483');
        $banner10->setLongitude('28.857365');
        $banner10->setBannerNumber('BannerName 24');
        $banner10->setUserId($bannerUser10);
        $bannersDomain->persist($banner10);

        $someCategory11 = $bannerCategoriesDomain->findOneBy(['indexname' => 'standard_banner']);
        $someCity11 = $bannerCityDomain->findOneBy(['nameCity' => 'Chisinau']);
        $bannerUser11 = $bannerUserIds->findOneBy(['name' => 'Agency']);
        $banner11 = new Banner();
        $banner11->setDescription('Cityboard.');
        $banner11->setCity($someCity11);
        $banner11->setArea('Botanica');
        $banner11->setAdress('bd. Dacia 21/1');
        $banner11->setCategory($someCategory11);
        $banner11->setLatitude('46.987223');
        $banner11->setLongitude('28.856230');
        $banner11->setBannerNumber('BannerName 25');
        $banner11->setUserId($bannerUser11);
        $bannersDomain->persist($banner11);

        $bannersDomain->flush();
    }

}

