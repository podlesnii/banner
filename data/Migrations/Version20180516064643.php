<?php

namespace Migrations;


use Application\Entity\News;
use Application\Model\NewsDomain;
use Application\Service\Doctrine\AbstractMigration;
use Application\Service\DomainModelManager;


/**
 * Class Version20180516064643
 * @package Migrations
 */
class Version20180516064643 extends AbstractMigration
{
    /**
     * @param DomainModelManager $domainManager
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function seed(DomainModelManager $domainManager)
    {

        /** @var NewsDomain $newsDomain */
        $newsDomain = $domainManager->get(NewsDomain::class);
        $news = new News();
        $news->setTitle('BANNER.MD');
        $news->setArticle('A banner can be a flag or other piece of cloth bearing a symbol, logo, slogan or 
        other message. A flag whose design is the same as the shield in a coat of arms (but usually in a square or 
        rectangular shape) is called a banner of arms. Also, a bar shape piece of non-cloth advertising material 
        sporting a name, slogan, or other marketing message.');
        $news->setImage('img/banner_02.jpg');
        $news->setActive('1');
        $newsDomain->persist($news);

        $news2 = new News();
        $news2->setTitle('Design your next banner in Canva');
        $news2->setArticle('Maintain your online presence by regularly updating your social media banners. Canva’s 
        banner maker gives you access to stunning banner layouts for social media, Etsy, eBay, emails and more. All you 
        need to do is choose images, fonts and colors you like and a professional banner could be yours in a matter of minutes.
        Forget tricky image sizing – all of our banners are automatically formatted to the correct dimensions for each 
        platform. With the best selection of banner layouts on the web to choose from, Canva makes it easy to keep your 
        online profiles looking fresh.');
        $news2->setImage('img/banner_03.jpg');
        $news2->setActive('1');
        $newsDomain->persist($news2);

        $news3 = new News();
        $news3->setTitle('PVC Banners');
        $news3->setArticle('PVC banners (also known as vinyl banners) are lightweight, durable and weather 
        resistant making them perfect for both indoor and outdoor advertising. As one of the UK\'s leading PVC banner 
        printers, all our banners are to printed to a high quality, and we offer a two year guarantee and express 24 
        hour printing service. We offer a comprehensive end-to-end service which includes a site visit, graphic design 
        services, digital printing and precision cutting, and banner installation. You can also choose any combination 
        of these services to suit your exact requirements. We offer installation services (which are perfect for large 
        or complicated installations) across the whole of the UK. Call us today for a free quotation or to ask any 
        questions that you may have about our end-to-end service.');
        $news3->setImage('img/banner_04.jpg');
        $news3->setActive('1');
        $newsDomain->persist($news3);

        $news4 = new News();
        $news4->setTitle('Stand-out stands');
        $news4->setArticle('You want it? We’ve got it! From flags to outdoor banners, cheap pop up banners or 
        massive exhibition stands. The ultimate range means you’re bound to find something you like.');
        $news4->setImage('img/banner_06.jpg');
        $news4->setActive('1');
        $newsDomain->persist($news4);

        $news5 = new News();
        $news5->setTitle('PVC Banners');
        $news5->setArticle('PVC banners (also known as vinyl banners) are lightweight, durable and weather 
        resistant making them perfect for both indoor and outdoor advertising. As one of the UK\'s leading PVC banner 
        printers, all our banners are to printed to a high quality, and we offer a two year guarantee and express 24 
        hour printing service. We offer a comprehensive end-to-end service which includes a site visit, graphic design 
        services, digital printing and precision cutting, and banner installation. You can also choose any combination 
        of these services to suit your exact requirements. We offer installation services (which are perfect for large 
        or complicated installations) across the whole of the UK. Call us today for a free quotation or to ask any 
        questions that you may have about our end-to-end service.');
        $news5->setImage('img/banner_05.jpg');
        $news5->setActive('1');
        $newsDomain->persist($news5);

        $news6 = new News();
        $news6->setTitle('Stand-out stands');
        $news6->setArticle('You want it? We’ve got it! From flags to outdoor banners, cheap pop up banners or 
        massive exhibition stands. The ultimate range means you’re bound to find something you like.');
        $news6->setImage('img/banner_07.jpg');
        $news6->setActive('1');
        $newsDomain->persist($news6);

        $newsDomain->flush();
    }
}