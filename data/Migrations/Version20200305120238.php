<?php

namespace Migrations;

use Application\Entity\BannerRatings;
use Application\Model\BannerDomain;
use Application\Model\BannerRatingsDomain;
use Application\Service\Doctrine\AbstractMigration;
use Application\Service\DomainModelManager;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200305120238 extends AbstractMigration
{


    /**
     * @param DomainModelManager $domainManager
     * @return mixed|void
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function seed(DomainModelManager $domainManager)
    {
        /** @var BannerDomain $bannersDomain */
        $bannersDomain = $domainManager->get(BannerDomain::class);
        $ratingDomain = $domainManager->get(BannerRatingsDomain::class);

        $someBanner = $bannersDomain->findOneBy(['bannerNumber' => 'BannerName 11']);
        $rating = new BannerRatings();
        $rating->setBanner($someBanner);
        $rating->setRating(4);
        $ratingDomain->persist($rating);

        $someBanner2 = $bannersDomain->findOneBy(['bannerNumber' => 'BannerName 11']);
        $rating2 = new BannerRatings();
        $rating2->setBanner($someBanner2);
        $rating2->setRating(5);
        $ratingDomain->persist($rating2);

        $someBanner3 = $bannersDomain->findOneBy(['bannerNumber' => 'BannerName 11']);
        $rating3 = new BannerRatings();
        $rating3->setBanner($someBanner3);
        $rating3->setRating(3);
        $ratingDomain->persist($rating3);

        $someBanner4 = $bannersDomain->findOneBy(['bannerNumber' => 'BannerName 11']);
        $rating4 = new BannerRatings();
        $rating4->setBanner($someBanner4);
        $rating4->setRating(3);
        $ratingDomain->persist($rating4);

        $someBanner5 = $bannersDomain->findOneBy(['bannerNumber' => 'BannerName 10']);
        $rating5 = new BannerRatings();
        $rating5->setBanner($someBanner5);
        $rating5->setRating(2);
        $ratingDomain->persist($rating5);

        $someBanner6 = $bannersDomain->findOneBy(['bannerNumber' => 'BannerName 9']);
        $rating6 = new BannerRatings();
        $rating6->setBanner($someBanner6);
        $rating6->setRating(2);
        $ratingDomain->persist($rating6);

        $someBanner7 = $bannersDomain->findOneBy(['bannerNumber' => 'BannerName 8']);
        $rating7 = new BannerRatings();
        $rating7->setBanner($someBanner7);
        $rating7->setRating(2);
        $ratingDomain->persist($rating7);

        $someBanner8 = $bannersDomain->findOneBy(['bannerNumber' => 'BannerName 8']);
        $rating8 = new BannerRatings();
        $rating8->setBanner($someBanner8);
        $rating8->setRating(2);
        $ratingDomain->persist($rating8);

        $ratingDomain->flush();
    }

}

