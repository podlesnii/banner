<?php

namespace Migrations;

use Application\Model\UserDomain;
use Application\Service\Doctrine\AbstractMigration;
use Application\Service\DomainModelManager;
use Application\Entity\Banner;
use Application\Model\BannerDomain;


/**
 * Class Version20180517110905
 * @package Migrations
 */
class Version20180517110905 extends AbstractMigration {
    /**
     * @param DomainModelManager $domainManager
     *
     * @return mixed|void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function seed(DomainModelManager $domainManager) {
        /** @var BannerDomain $bannersDomain */
        $bannersDomain = $domainManager->get(BannerDomain::class);
        $bannerUserDomain = $domainManager->get(UserDomain::class);

        $someUser = $bannerUserDomain->findOneBy(['indexName' => 'administrator']);
        $banner = $bannersDomain->findOneBy(['id' => '1']);
        $banner->setUserId($someUser);
        $bannersDomain->persist($banner);

        $someUser2 = $bannerUserDomain->findOneBy(['indexName' => 'administrator']);
        $banner2 = $bannersDomain->findOneBy(['id' => '2']);
        $banner2->setUserId($someUser2);
        $bannersDomain->persist($banner2);

        $someUser3 = $bannerUserDomain->findOneBy(['indexName' => 'owner']);
        $banner3 = $bannersDomain->findOneBy(['id' => '3']);
        $banner3->setUserId($someUser3);
        $bannersDomain->persist($banner3);

        $someUser4 = $bannerUserDomain->findOneBy(['indexName' => 'owner']);
        $banner4 = $bannersDomain->findOneBy(['id' => '4']);
        $banner4->setUserId($someUser4);
        $bannersDomain->persist($banner4);

        $someUser5 = $bannerUserDomain->findOneBy(['indexName' => 'owner']);
        $banner5 = $bannersDomain->findOneBy(['id' => '5']);
        $banner5->setUserId($someUser5);
        $bannersDomain->persist($banner5);

        $someUser6 = $bannerUserDomain->findOneBy(['indexName' => 'agency']);
        $banner6 = $bannersDomain->findOneBy(['id' => '6']);
        $banner6->setUserId($someUser6);
        $bannersDomain->persist($banner6);

        $someUser7 = $bannerUserDomain->findOneBy(['id' => 'agency']);
        $banner7 = $bannersDomain->findOneBy(['id' => '7']);
        $banner7->setUserId($someUser7);
        $bannersDomain->persist($banner7);

        $bannersDomain->flush();
    }
}