<?php

namespace Migrations;


use Acl\Model\PrivilegeDomain;
use Acl\Model\ResourceDomain;
use Acl\Model\RoleDomain;
use Application\Service\Doctrine\AbstractMigration;
use Application\Service\DomainModelManager;


/**
 * Class Version20180521113307
 *
 * @package Migrations
 */
class Version20180521113307 extends AbstractMigration {
    /**
     * @param DomainModelManager $domainManager
     *
     * @return mixed|void
     */
    public function seed(DomainModelManager $domainManager)
    {

        /** @var RoleDomain $roleDomain */
        $roleDomain = $domainManager->get(RoleDomain::class);

        /** @var ResourceDomain $resourceDomain */
        $resourceDomain = $domainManager->get(ResourceDomain::class);

        /** @var PrivilegeDomain $privilegeDomain */
        $privilegeDomain = $domainManager->get(PrivilegeDomain::class);


        $roleMapper = $roleDomain->getMapper('name', 'id');
        $resourceMapper = $resourceDomain->getMapper('name', 'id');

        $qb = $privilegeDomain->getRepository()->createQueryBuilder('p');

        $qb->delete('Acl\Entity\Privilege', 'p')
                ->add('where', $qb->expr()->andX(
                        $qb->expr()->in('p.role_id', [
                                $roleMapper['Owner'],
                                $roleMapper['Agency'],
                                $roleMapper['Customer'],
                                $roleMapper['Guest'],
                        ]),
                        $qb->expr()->in('p.resource_id', [
                                $resourceMapper['Application\Controller\OrderController:ordered'],
                                $resourceMapper['Application\Controller\OrderController:index'],
                                $resourceMapper['Application\Controller\OrderController:download'],
                        ])
                ));

        $query = $qb->getQuery();

        $query->execute();

    }

}

