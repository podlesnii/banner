<?php

namespace Migrations;

use Application\Service\Doctrine\AbstractMigration;
use Application\Service\DomainModelManager;
use Acl\Entity\Privilege;
use Acl\Entity\Resource;
use Acl\Entity\Role;
use Acl\Model\PrivilegeDomain;
use Acl\Model\ResourceDomain;
use Acl\Model\RoleDomain;
use Zend\Permissions\Acl\Acl;


/**
 * Class Version20180425111016
 * @package Migrations
 */
class Version20180425111016 extends AbstractMigration
{


    /**
     * @param DomainModelManager $domainManager
     * @return mixed|void
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function seed(DomainModelManager $domainManager)
    {

        $aclRules = [
            'Owner' => [
                //Admin -> Owner invoice
                'Admin\Controller\InvoiceController:index'  => Acl::TYPE_ALLOW,
                'Admin\Controller\InvoiceController:list'   => Acl::TYPE_ALLOW,
                'Admin\Controller\InvoiceController:add'    => Acl::TYPE_ALLOW,
                'Admin\Controller\InvoiceController:update' => Acl::TYPE_ALLOW,
            ],


        ];

        /** @var RoleDomain $roleDomain */
        $roleDomain = $domainManager->get(RoleDomain::class);

        /** @var ResourceDomain $resourceDomain */
        $resourceDomain = $domainManager->get(ResourceDomain::class);

        /** @var PrivilegeDomain $privilegeDomain */
        $privilegeDomain = $domainManager->get(PrivilegeDomain::class);

        $allResources = $resourceDomain->findAll();

        foreach ($aclRules as $roleName => $rules) {
            /** @var Role $roleEntity */
            $roleEntity = $roleDomain->findOneBy(['name' => $roleName]);
            foreach ($rules as $resourceName => $privilege) {
                $resourceEntity = null;
                /** @var \Acl\Entity\Resource $entity */
                foreach ($allResources as $entity) {
                    if ($entity->getName() == $resourceName) {
                        $resourceEntity = $entity;
                        break;
                    }
                }
                if (is_null($resourceEntity)) {
                    $resourceEntity = new Resource();
                    $resourceEntity->setName($resourceName);
                    $allResources[] = $resourceEntity;
                    $resourceDomain->persist($resourceEntity);
                }
                $privilegeEntity = new Privilege();

                $privilegeEntity->setResource($resourceEntity);
                $privilegeEntity->setRole($roleEntity);
                $privilegeEntity->setPermissions($privilege);

                $privilegeDomain->persist($privilegeEntity);
            }
        }

        $privilegeDomain->flush();
    }

}




