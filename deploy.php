<?php
require_once "recipe/common.php";
require_once "bin/deployer/recipe/telegram.php";

use function Deployer\after;
use function Deployer\before;
use function Deployer\cd;
use function Deployer\fail;
use function Deployer\get;
use function Deployer\inventory;
use function Deployer\run;
use function Deployer\set;
use function Deployer\task;



inventory('hosts.yml');

set('branch', 'develop');
set('ssh_type', 'ext-ssh2');
set('shared_dirs', [
    'data/cache',
    'data/DoctrineORMModule',
]);
set('writable_dirs', get('shared_dirs'));
set('http_user', 'www-data');

set('telegram_token', '582157386:AAHf0vqTfkpbuU057geDbI6Tv-65Q1chAsU');
set('telegram_chat_id', '-208167877');
set('telegram_text', 'Pipelines deploying `{{branch}}` to *{{target}}*');

/*
 * Make sure we are on the right branch
 */
task('deploy:checkout', function () {
    $deployPath = get('deploy_path');
    cd($deployPath);
    run('git checkout ' . get('branch', '') . ' 2> /dev/null');
})->desc('checking out git branch');

/*
 * Pull the changes onto the server
 */
task('deploy:pull_changes', function () {
    $deployPath = get('deploy_path');
    cd($deployPath);
    run('git pull origin ' . get('branch', '') . ' ');
})->desc('pull changes on server');

/*
 * Update the DB schema
 */
task('deploy:orm_update', function () {
    $deployPath = get('deploy_path');
    cd($deployPath);
    run('php vendor/bin/doctrine-module orm:schema-tool:update --force');
})->desc('updating schema');

/*
 * Run the migrations on the server
 */
task('deploy:run_migrations', function () {
    $deployPath = get('deploy_path');
    cd($deployPath);
    run('php vendor/bin/doctrine-module migrations:migrate');
})->desc('running migrations');

/*
 * Run composer update on the server
 */
task('deploy:composer_update', function () {
    $deployPath = get('deploy_path');
    cd($deployPath);
    run('php composer.phar update');
})->desc('running composer update');


task('deploy:dev', [
    'deploy:info',
    'deploy:checkout',
    'deploy:pull_changes',
    'deploy:composer_update',
    'deploy:orm_update',
    'deploy:run_migrations',
])->desc('Deploy application to staging.');

after('deploy:dev', 'success');

before('deploy:dev', 'telegram:notify');
fail('deploy:dev', 'telegram:notify:fail');
after('success', 'telegram:notify:success');
