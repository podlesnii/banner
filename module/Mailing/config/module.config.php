<?php

namespace Mailing;

use Mailing\Service\Factory\MailingServiceFactory;
use Mailing\Service\Mailing;
use Zend\Router\Http\Literal;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router'          => [
        'routes' => [
            'mailing' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/mailing',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers'     => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            Mailing::class => MailingServiceFactory::class,
        ],
    ],
    'view_manager'    => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'email_config' => [
            'email_addresses' => [
                    'no_reply' => [
                            'email' => 'no_reply@banner.md',
                            'name'  => 'Banner.MD'
                    ],
                    'contact'  => [
                            'email' => 'banner_contact@banner.md',
                            'name'  => 'contact',
                    ],
            ],
    ],
];
