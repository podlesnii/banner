<?php

namespace Mailing\Service;

use Application\Controller\ContactController;
use Application\Controller\OrderController;
use Application\Service\Auth;
use Zend\Mail\Message;
use Zend\Mail\Transport\TransportInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;


/**
 * Class Mailing
 */
class Mailing
{

    /**
     * Authentication service.
     * @var TransportInterface
     */
    protected $transport;

    /** @var PhpRenderer */
    protected $viewRenderer;

    protected $variables;

    /** @var [] */
    protected $config;
    /**
     * Mailing constructor.
     *
     * @param $transport
     */
    public function __construct($transport)
    {
        $this->transport = $transport;

    }

    /**
     * @param Auth $email
     * @param Auth $variables
     */
    public function setRegistrationConfirmation($email, $variables)
    {

        $content = $this->renderTemplate('mailing/registration-confirmation', $variables);

        $html = new MimePart($content);
        $html->type = "text/html";

        $body = new MimeMessage();
        $body->addPart($html);

        $message = new Message();
        $message->addFrom($this->config['email_addresses']['no_reply']['email'],
                          $this->config['email_addresses']['no_reply']['name']);
        $message->addTo($email);
        $message->setSubject("Banner - Email confirmation");
        $message->setBody($body);

        $this->transport->send($message);
    }

    /**
     * @param Auth $email
     * @param Auth $variables
     */
    public function sendResetPassword($email, $variables)
    {

        $content = $this->renderTemplate('mailing/send-reset-password', $variables);

        $html = new MimePart($content);
        $html->type = "text/html";

        $body = new MimeMessage();
        $body->addPart($html);

        $message = new Message();
        $message->addFrom($this->config['email_addresses']['no_reply']['email'],
                          $this->config['email_addresses']['no_reply']['name']);
        $message->addTo($email);
        $message->setSubject("Banner - Reset Password");
        $message->setBody($body);

        $this->transport->send($message);
    }

    /**
     * @param OrderController $email
     * @param OrderController $variables
     */
    public function sendBookingConfirmation($email, $variables)
    {

        $content = $this->renderTemplate('pdf/invoice.phtml', $variables);

        $html = new MimePart($content);
        $html->type = "text/html";

        $body = new MimeMessage();
        $body->addPart($html);

        $message = new Message();
        $message->addFrom($this->config['email_addresses']['no_reply']['email'],
                          $this->config['email_addresses']['no_reply']['name']);
        $message->addTo($email);
        $message->setSubject("Banner - Booking confirmation");
        $message->setBody($body);

        $this->transport->send($message);
    }


    /**
     * @param $email
     * @param $variables
     */
    public function sendContactMessage($email, $variables)
    {

        $content = $this->renderTemplate('mailing/send-contact-message', $variables);

        $html = new MimePart($content);
        $html->type = "text/html";

        $body = new MimeMessage();
        $body->addPart($html);

        $message = new Message();
        $message->addFrom($email, $variables['name']);
        $message->addTo($this->config['email_addresses']['contact']['email'],
                        $this->config['email_addresses']['contact']['name'] );
        $message->setSubject("Banner - Contact Message");
        $message->setBody($body);

        $this->transport->send($message);

    }

    /**
     * @param $templateName
     * @param $variables
     *
     * @return string
     */
    public function renderTemplate($templateName, $variables)
    {
        $viewModel = new ViewModel();
        $viewModel->setTemplate($templateName)
                  ->setVariables($variables);

        $content = $this->viewRenderer->render($viewModel);

        return $content;
    }


    /**
     * @param PhpRenderer $viewRenderer
     */
    public function setViewRenderer(PhpRenderer $viewRenderer)
    {
        $this->viewRenderer = $viewRenderer;
    }

    /**
     * @param mixed $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }


}