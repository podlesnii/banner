<?php

namespace Mailing\Service\Factory;

use Interop\Container\ContainerInterface;
use Mailing\Service\Mailing;
use Zend\Mail\Transport\Sendmail;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;


/**
 * Class MailingServiceFactory
 * @package Mailing\Service\Factory
 */
class MailingServiceFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return Mailing|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        // Setup Sendmail transport
        $transport = new Sendmail();

        /** @var PhpRenderer $renderer */
        $renderer = $container->get('Zend\View\Renderer\RendererInterface');

        $config = $container->get('Config');

        $mailing = new Mailing($transport);
        $mailing->setViewRenderer($renderer);
        $mailing->setConfig($config['email_config']);

        return $mailing;
    }

}