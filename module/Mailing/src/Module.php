<?php

namespace Mailing;

/**
 * Class Module
 * @package Mailing
 */
class Module
{
    const VERSION = '3.0.3-dev';

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

}
