<?php

namespace Mailing\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Class IndexController
 * @package Mailing\Controller
 */
class IndexController extends AbstractActionController
{
    /**
     * Default action if none provided
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        die ("Mailing index action");
    }
}
