<?php

namespace Pdf\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Class IndexController
 * @package Pdf\Controller
 */
class IndexController extends AbstractActionController
{


    /**
     * @return void|ViewModel
     */
    public function indexAction()
    {
        die ("Pdf index action");
    }
}
