<?php

namespace Pdf;

/**
 * Class Module
 * @package Pdf
 */
class Module
{
    const VERSION = '3.0.3-dev';

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

}
