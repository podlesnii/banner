<?php

namespace Pdf;

use Zend\Router\Http\Literal;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router'          => [
        'routes' => [
            'mailing' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/pdf',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers'     => [
            'factories' => [
                    Controller\IndexController::class => InvokableFactory::class,
            ],
    ],

    'view_manager'    => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
