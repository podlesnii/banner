<?php

namespace Acl;

use Acl\Model\PrivilegeDomain;
use Acl\Model\ResourceDomain;
use Acl\Model\RoleDomain;
use Application\Model\DomainModelAbstractFactory;
use Application\Service\Auth;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Zend\ServiceManager\ServiceManager;

return [
    'domain_model_manager' => [
        'factories' => [
            PrivilegeDomain::class => DomainModelAbstractFactory::class,
            ResourceDomain::class  => DomainModelAbstractFactory::class,
            RoleDomain::class      => DomainModelAbstractFactory::class,
        ],
    ],
    'view_helpers'         => [
        'factories' => [
            'aclHelper' => function (ServiceManager $sm) {
                $authService = $sm->get(Auth::class);
                $aclService = $sm->get(Permissions\Acl::class);
                $helper = new View\Helper\Acl($aclService, $authService);

                return $helper;
            },
        ],
    ],
    'doctrine'             => [
        'driver' => [
            'acl_entities' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity',
                ],
            ],
            'orm_default'  => [
                'drivers' => [
                    'Acl\Entity' => 'acl_entities',
                ],
            ],
        ],
    ],
];