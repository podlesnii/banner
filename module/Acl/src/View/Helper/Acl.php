<?php

namespace Acl\View\Helper;

use Application\Service\Auth;
use Zend\View\Helper\AbstractHelper;

/**
 * Class Acl
 * @package Application\View\Helper
 */
class Acl extends AbstractHelper
{

    /** @var \Acl\Permissions\Acl */
    protected $aclService;

    /** @var Auth */
    protected $authService;

    /**
     * Acl constructor.
     *
     * @param $aclService
     * @param $authService
     */
    public function __construct($aclService, $authService)
    {
        $this->aclService = $aclService;
        $this->authService = $authService;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        return $this;
    }


    /**
     * @param $controller
     * @param $action
     *
     * @return bool
     */
    public function isAllowed($controller, $action)
    {
        $resourceName = implode(':', [
            $controller,
            $action,
        ]);
        $currentRole = $this->authService->getCurrentRole();

        return $this->aclService->isAllowed($currentRole, $resourceName);
    }

}