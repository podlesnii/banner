<?php

namespace Acl\Model;

use Acl\Entity\Role;
use Doctrine\ORM\Query;
use Application\Model\DomainModelAbstract;

/**
 * Class RoleDomain
 * @package Acl\Model
 */
class RoleDomain extends DomainModelAbstract
{
    protected $entityName = Role::class;

    /**
     * @return Query
     */
    public function getRoles()
    {

        $qb = $this->getRepository()->createQueryBuilder('acl_role');

        $query = $qb->getQuery();
        $query->setHydrationMode(Query::HYDRATE_ARRAY);

        return $query;
    }

    /**
     * @param Role $role ;
     * @param $data
     *
     * @return Role
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateRole($role, $data)
    {
        $role->fromArray($data);

        $this->persist($role);
        $this->flush();

        return $role;
    }


}