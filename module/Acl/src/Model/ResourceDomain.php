<?php

namespace Acl\Model;


use Acl\Entity\Resource;
use Application\Model\DomainModelAbstract;

/**
 * Class ResourceDomain
 * @package Acl\Model
 */
class ResourceDomain extends DomainModelAbstract
{

    protected $entityName = Resource::class;

}