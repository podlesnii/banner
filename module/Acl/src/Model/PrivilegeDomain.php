<?php

namespace Acl\Model;


use Acl\Entity\Privilege;
use Application\Model\DomainModelAbstract;

/**
 * Class PrivilegeDomain
 * @package Acl\Model
 */
class PrivilegeDomain extends DomainModelAbstract
{

    protected $entityName = Privilege::class;

}