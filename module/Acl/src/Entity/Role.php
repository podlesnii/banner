<?php

namespace Acl\Entity;

use Application\Entity\EntityAbstract;
use Application\Entity\User;
use Doctrine\ORM\Mapping as ORM;


/**
 * Role
 *
 * @ORM\Table(name="acl_role")
 * @ORM\Entity
 */
class Role extends EntityAbstract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=2560, nullable=false)
     */
    protected $description;

    /**
     * @ORM\OneToMany(targetEntity="\Application\Entity\User", mappedBy="role")
     */
    protected $user;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId ()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName ()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName ( $name )
    {
        $this->name = $name;
    }


    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $description
     */
    public function setDescription( $description)
    {
        $this->description = $description;
    }


    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

}




