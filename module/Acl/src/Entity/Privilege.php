<?php

namespace Acl\Entity;

use Application\Entity\EntityAbstract;
use Doctrine\ORM\Mapping as ORM;

/**
 * Privilege
 *
 * @ORM\Table(name="acl_privilege")
 * @ORM\Entity
 */
class Privilege extends EntityAbstract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="permissions", type="text", nullable=false)
     */
    private $permissions;


    /**
     * @var \Acl\Entity\Resource
     *
     * @ORM\ManyToOne(targetEntity="Acl\Entity\Resource")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="resource_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     * })
     */
    private $resource;

    /**
     * @var \Acl\Entity\Role
     *
     * @ORM\ManyToOne(targetEntity="Acl\Entity\Role")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     * })
     */
    private $role;

    /**
     * @var integer
     *
     * @ORM\Column(name="resource_id", type="integer", nullable=false)
     */
    private $resource_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="role_id", type="integer", nullable=false)
     */
    private $role_id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set permissions
     *
     * @param string $permissions
     *
     * @return Privilege
     */
    public function setPermissions($permissions)
    {
        if (is_array($permissions))
            $this->permissions = json_encode($permissions);
        else
            $this->permissions = $permissions;

        return $this;
    }

    /**
     * Get permissions
     *
     * @return string
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * Set resource
     *
     * @param Resource $resource
     *
     * @return Privilege
     */
    public function setResource(Resource $resource = null)
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * Get resource
     *
     * @return \Acl\Entity\Resource
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * Set role
     *
     * @param \Acl\Entity\Role $role
     *
     * @return Privilege
     */
    public function setRole(Role $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return \Acl\Entity\Role
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return int
     */
    public function getResourceId() {
        return $this->resource_id;
    }

    /**
     * @return int
     */
    public function getRoleId() {
        return $this->role_id;
    }

}
