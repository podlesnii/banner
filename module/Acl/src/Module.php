<?php

namespace Acl;

use Acl\Model\PrivilegeDomain;
use Acl\Model\ResourceDomain;
use Acl\Model\RoleDomain;
use Application\Service\Auth;
use Application\Service\DomainModelManager;
use Interop\Container\ContainerInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\AbstractController;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceManager;


/**
 * Class Module
 * @package Acl
 */
class Module
{
    /**
     * @param MvcEvent $e
     */
    public function onBootstrap( MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $sharedEventManager = $eventManager->getSharedManager();

        $sharedEventManager->attach(AbstractActionController::class,
            MvcEvent::EVENT_DISPATCH, [$this, 'onDispatch'], 100);
    }

    /**
     * @param MvcEvent $event
     *
     * @return mixed
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function onDispatch(MvcEvent $event)
    {
        /** @var ServiceManager $container */
        $container = $event->getApplication()->getServiceManager();

        /** @var AbstractController $controller */
        $controller = $event->getTarget();

        $controllerName = get_class($controller);

        $actionName = $event->getRouteMatch()->getParam('action', null);
        $actionName = str_replace('-', '', lcfirst(ucwords($actionName, '-')));

        $resourceName = implode(':', [
           $controllerName,
           $actionName,
        ]);

        /** @var Permissions\Acl $permisions */
        $permisions = $container->get(Permissions\Acl::class);
        /** @var Auth $authService */
        $authService = $container->get(Auth::class);

        $currentRole = $authService->getCurrentRole();

        if (!$permisions->isAllowed($currentRole, $resourceName)) {
            return $controller->redirect()->toRoute('home');
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * @return array
     */
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                Permissions\Acl::class => function(ContainerInterface $container) {
                    $domainModelManager = $container->get(DomainModelManager::class);
                    /** @var RoleDomain $rolesDomain */
                    $rolesDomain = $domainModelManager->get(RoleDomain::class);
                    /** @var ResourceDomain $resourcesDomain */
                    $resourcesDomain = $domainModelManager->get(ResourceDomain::class);
                    /** @var PrivilegeDomain $privilegesDomain */
                    $privilegesDomain = $domainModelManager->get(PrivilegeDomain::class);

                    /* Get ACL from database */
					$roles      = $rolesDomain->findAll();
					$resources  = $resourcesDomain->findAll();
					$privileges = $privilegesDomain->findAll();

                    return new Permissions\Acl($roles, $resources, $privileges);
                }
            )
        );
    }
}


