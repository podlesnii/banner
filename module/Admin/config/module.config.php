<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin;

use Admin\Controller\Factory\BannerCategoryControllerFactory;
use Admin\Controller\Factory\BannersControllerFactory;
use Admin\Controller\Factory\BannerStatusesControllerFactory;
use Admin\Controller\Factory\BannerStatusesHistoryControllerFactory;
use Admin\Controller\Factory\CityControllerFactory;
use Admin\Controller\Factory\InvoiceControllerFactory;
use Admin\Controller\Factory\NewsAdminControllerFactory;
use Admin\Controller\Factory\UsersControllerFactory;
use Admin\Controller\Factory\RolesControllerFactory;
use Admin\Form\BannerForm;
use Admin\Form\Factory\BannerFormFactory;
use Admin\Form\UserForm;
use Admin\Form\Factory\UserFormFactory;
use Admin\Service\AddFromFile;
use Admin\Service\Factory\AddFromFileFactory;
use Admin\Service\ImageManager;
use Admin\Service\YMapAddresses;
use Application\Form\Factory\SearchFormFactory;
use Application\Form\SearchForm;
use Application\Service\Notifications;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\ServiceManager\ServiceManager;

return [
    'router'               => [
        'routes' => [
            'admin' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/admin[/:controller[/:action]]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers'          => [
        'factories' => [
            Controller\IndexController::class                 => InvokableFactory::class,
            Controller\UsersController::class                 => UsersControllerFactory::class,
            Controller\RolesController::class                 => RolesControllerFactory::class,
            Controller\BannersController::class               => BannersControllerFactory::class,
            Controller\BannerCategoryController::class        => BannerCategoryControllerFactory::class,
            Controller\CityController::class                  => CityControllerFactory::class,
            Controller\InvoiceController::class               => InvoiceControllerFactory::class,
            Controller\BannerStatusesController::class        => BannerStatusesControllerFactory::class,
            Controller\BannerStatusesHistoryController::class => BannerStatusesHistoryControllerFactory::class,
            Controller\NewsAdminController::class             => NewsAdminControllerFactory::class,

            ],
            'aliases'   => [
                'index'                  => Controller\IndexController::class,
                'users'                  => Controller\UsersController::class,
                'roles'                  => Controller\RolesController::class,
                'banners'                => Controller\BannersController::class,
                'bannercategory'         => Controller\BannerCategoryController::class,
                'city'                   => Controller\CityController::class,
                'invoice'                => Controller\InvoiceController::class,
                'bannerstatuses'         => Controller\BannerStatusesController::class,
                'bannerstatuseshistory'  => Controller\BannerStatusesHistoryController::class,
                'newsadmin'              => Controller\NewsAdminController::class,
            ],
        ],
        'service_manager'      => [
            'factories' => [
                BannerForm::class   => BannerFormFactory::class,
                UserForm::class     => UserFormFactory::class,
                SearchForm::class   => SearchFormFactory::class,
                ImageManager::class => InvokableFactory::class,
                AddFromFile::class  => AddFromFileFactory::class,
                YMapAddresses::class  => InvokableFactory::class,
            ],
        ],
        'domain_model_manager' => [
            'factories' => [
            ],
        ],
        'doctrine'             => [
            'driver' => [
                'admin_entities' => [
                    'class' => AnnotationDriver::class,
                    'cache' => 'array',
                    'paths' => [
                        __DIR__ . '/../src/Entity',
                    ],
                ],
                'orm_default'    => [
                    'drivers' => [
                        'Admin\Entity' => 'admin_entities',
                    ],
                ],
            ],
        ],
    'view_helpers'         => array(
        'factories' => array(
            'notificationsHelper' => function (ServiceManager $sm) {
                $notificationsService = $sm->get(Notifications::class);

                $helper = new \Application\View\Helper\Notifications($notificationsService);

                return $helper;
            },
        ),
    ),
        'view_manager'         => [
            'template_map'        => [
                'admin/index/index'           => __DIR__ . '/../view/admin/index/index.phtml',
                'admin/index'                 => __DIR__ . '/../view/admin/index/index.phtml',
                'admin/users'                 => __DIR__ . '/../view/admin_layout/layout.phtml',
                'admin/banners'               => __DIR__ . '/../view/admin_layout/layout.phtml',
                'admin/banners/list'          => __DIR__ . '/../view/admin/banners/partials/list.phtml',
                'admin/bannercategory'        => __DIR__ . '/../view/admin_layout/layout.phtml',
                'admin/city'                  => __DIR__ . '/../view/admin_layout/layout.phtml',
                'admin/invoice'               => __DIR__ . '/../view/admin_layout/layout.phtml',
                'admin/bannerstatuses '       => __DIR__ . '/../view/admin_layout/layout.phtml',
                'admin/bannerstatuseshistory' => __DIR__ . '/../view/admin_layout/layout.phtml',
                'admin/newsadmin'             => __DIR__ . '/../view/admin_layout/layout.phtml',
            ],
            'template_path_stack' => [
                __DIR__ . '/../view',
            ],
            'module_layouts'      => [
                'Admin' => 'admin_layout/layout.phtml',
            ],
            'strategies'          => array (
                'ViewJsonStrategy',
            ),
        ],
];

