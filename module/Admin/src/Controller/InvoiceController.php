<?php

namespace Admin\Controller;

use Admin\Form\InvoiceForm;
use Application\Model\InvoiceDomain;
use Application\Service\Auth;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;


/**
 * Class InvoiceController
 *
 * @package Admin\Controller
 */
class InvoiceController extends AbstractActionController {

    /** @var InvoiceDomain */
    protected $invoiceDomain;

    /** @var InvoiceForm */
    protected $invoiceForm;

    /** @var Auth $auth */
    protected $auth;


    /**
     * @return ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function indexAction() {
        $user = $this->auth->getCurrentUser();
        $userId = $user->getId();

        $filter = null;
        $invoice = $this->invoiceDomain->getInvoice($filter);


        $viewModel = new ViewModel([
                'invoice' => $invoice,
                'userId'  => $userId,
        ]);
        return $viewModel;
    }


    /**
     * @return ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function listAction()
    {
        $user = $this->auth->getCurrentUser();
        $userId = $user->getId();

        $invoice = $this->invoiceDomain->getInvoice();

        $viewModel = new ViewModel([
                'invoice' => $invoice,
                'userId'  => $userId,
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }


    /**
     * @return JsonModel|ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addAction()
    {
        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost(); // Fill in the form with POST data
            $this->invoiceForm->setData($data);

            if ($this->invoiceForm->isValid()) {  // Validate form

                $user = $this->auth->getCurrentUser();
                $userId = $user->getId();

                $data = $this->invoiceForm->getData(); // Get filtered and validated data
                $this->invoiceDomain->addInvoice($data, $userId);  // Create new invoice
                $notyParameters = ['type' => 'success', 'message' => 'Successfully added!'];

                /**
                 * In case if form is valid we do not need to return form again. We return JSON with 'callbacks' method,
                 * which is validated in admin.js and if it's values are set correctly, callbacks are executed.
                 */
                return new JsonModel([
                    'callbacks' => [
                        [
                            'callback' => 'loadDataToContainer',
                            'arguments' => [
                                'records-container',
                                $this->url()->fromRoute('admin', ['controller' => 'invoice', 'action' => 'list']),
                            ],
                        ], [
                            'callback' => 'hideModal',
                            'arguments' => [],
                        ],
                    ],
                    'notifications' => [[
                        'type' => $notyParameters['type'],
                        'message' => $notyParameters['message'],
                    ]],
                ]);
            }
        }

        $viewModel = new ViewModel([
                'form' => $this->invoiceForm,
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }


    /**
     * @return bool|JsonModel|ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateAction() {
        $id = $this->params()->fromQuery('id');

        if (!is_numeric($id)) {
            return false;
        }

        $invoice = $this->invoiceDomain->find($id);

        if (!$invoice) {
            return false;
        }

        $this->invoiceForm->populateValues($invoice->getArrayCopy()); //Populate form with array values from Invoice entity

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost(); // Fill in the form with POST data
            $this->invoiceForm->setData($data);

            if ($this->invoiceForm->isValid()) {  // Validate form
                $data = $this->invoiceForm->getData(); // Get filtered and validated data

                $this->invoiceDomain->updateInvoice($invoice, $data);  // Update existing invoice
                $notyParameters = ['type' => 'success', 'message' => 'Successfully updated!'];

                /**
                 * In case if form is valid we do not need to return form again. We return JSON with 'callbacks' method,
                 * which is validated in admin.js and if it's values are set correctly, callbacks are executed.
                 */
                return new JsonModel([
                    'callbacks' => [
                        [
                            'callback' => 'loadDataToContainer',
                            'arguments' => [
                                'records-container',
                                $this->url()->fromRoute('admin', ['controller' => 'invoice', 'action' => 'list']),
                            ],
                        ], [
                            'callback' => 'hideModal',
                            'arguments' => [],
                        ],
                    ],
                    'notifications' => [[
                        'type' => $notyParameters['type'],
                        'message' => $notyParameters['message'],
                    ]],
                ]);
            }
        }

        $viewModel = new ViewModel([
                'form'      => $this->invoiceForm,
                'invoiceId' => $invoice->getId(),
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }


    /**
     * @param InvoiceDomain $invoiceDomain
     */
    public function setInvoiceDomain(InvoiceDomain $invoiceDomain) {
        $this->invoiceDomain = $invoiceDomain;
    }

    /**
     * @param InvoiceForm $invoiceForm
     */
    public function setInvoiceForm(InvoiceForm $invoiceForm) {
        $this->invoiceForm = $invoiceForm;
    }

    /**
     * @param Auth $auth
     */
    public function setAuth(Auth $auth) {
        $this->auth = $auth;
    }

}
