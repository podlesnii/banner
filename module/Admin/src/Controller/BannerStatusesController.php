<?php

namespace Admin\Controller;

use Admin\Form\BannerStatusForm;
use Application\Model\BannerStatusDomain;
use Application\Model\BannerStatusHistoryDomain;
use Application\Service\Auth;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Zend\Paginator\Paginator;

/**
 * Class BannerStatusesController
 * @package Admin\Controller
 */
class BannerStatusesController extends AbstractActionController
{

    /** @var BannerStatusDomain */
    protected $bannerStatusDomain;

    /** @var BannerStatusHistoryDomain */
    protected $bannerStatusHistoryDomain;

    /** @var BannerStatusForm */
    protected $bannerStatusForm;

    /** @var Auth */
    protected $auth;


    /**
     * @return ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function indexAction()
    {
        $paginator = $this->getListPaginator();

        $viewModel = new ViewModel([
            'paginator' => $paginator,

        ]);

        return $viewModel;
    }


    /**
     * @return ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function listAction()
    {
        $paginator = $this->getListPaginator();


        $viewModel = new ViewModel([
            'paginator' => $paginator,
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }


    /**
     * @return bool|JsonModel|ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateAction()
    {
        $statusId = $this->params()->fromQuery('id');

        if (!is_numeric($statusId)) {
            return false;
        }

        $bannerStatus = $this->bannerStatusDomain->find($statusId);
        $bannerId = $bannerStatus->getBanner()->getId();
        if (!$bannerStatus) {
            return false;
        }

        $this->bannerStatusForm->populateValues($bannerStatus->getArrayCopy()); //Populate form with array values from Banner entity

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost(); // Fill in the form with POST data
            $this->bannerStatusForm->setData($data);

            if ($this->bannerStatusForm->isValid()) {  // Validate form
                $data = $this->bannerStatusForm->getData(); // Get filtered and validated data
                $this->bannerStatusDomain->updateBannerStatus($bannerStatus, $data);  // Update existing banner
                $this->bannerStatusHistoryDomain->addBannerStatusHistory($bannerId, $data); //Create banner status history

                $notyParameters = ['type' => 'success', 'message' => 'Successfully updated!'];

                /**
                 * In case if form is valid we do not need to return form again. We return JSON with 'callbacks' method,
                 * which is validated in admin.js and if it's values are set correctly, callbacks are executed.
                 */
                return new JsonModel([
                    'callbacks' => [
                        [
                            'callback'  => 'loadDataToContainer',
                            'arguments' => [
                                'records-container',
                                $this->url()->fromRoute('admin', ['controller' => 'bannerstatuses', 'action' => 'list']),
                            ],
                        ], [
                            'callback'  => 'hideModal',
                            'arguments' => [],
                        ],
                    ],
                    'notifications' => [[
                        'type' => $notyParameters['type'],
                        'message' => $notyParameters['message'],
                    ]],
                ]);
            }
        }


        $viewModel = new ViewModel([
            'form'     => $this->bannerStatusForm,
            'bannerStatusId' => $bannerStatus->getId(),
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }


    /**
     * @return bool|JsonModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteAction()
    {
        $id = $this->params()->fromQuery('id');

        if (!is_numeric($id)) {
            return false;
        }

        $bannerStatus = $this->bannerStatusDomain->find($id);

        if (!$bannerStatus) {
            return false;
        }

        $this->bannerStatusDomain->deleteBannerStatus($bannerStatus);
        $notyParameters = ['type' => 'info', 'message' => 'Successfully deleted!!'];

        /**
         * We do not transmit 'hideModal' function call here, because confirmation modal will be hidden anyway
         */
        return new JsonModel([
            'callbacks' => [
                [
                    'callback'  => 'loadDataToContainer',
                    'arguments' => [
                        'records-container',
                        $this->url()->fromRoute('admin', ['controller' => 'bannerstatuses', 'action' => 'list']),
                    ],
                ],
            ],
            'notifications' => [[
                'type' => $notyParameters['type'],
                'message' => $notyParameters['message'],
            ]],
        ]);
    }


    /**
     * @return Paginator
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    protected function getListPaginator()
    {
        $currentUser = $this->auth->getCurrentUser();

        $query = $this->bannerStatusDomain->getBannerStatuses($currentUser);

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new Paginator($adapter);

        $page = $this->params()->fromQuery('page');

        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);

        if ($page > $paginator->count()) {
            $paginator->setCurrentPageNumber(1);
        }

        return $paginator;
    }

    /**
     * @param BannerStatusDomain $bannerStatusDomain
     */
    public function setBannerStatusDomain(BannerStatusDomain $bannerStatusDomain)
    {
        $this->bannerStatusDomain = $bannerStatusDomain;
    }

    /**
     * @param BannerStatusHistoryDomain $bannerStatusHistoryDomain
     */
    public function setBannerStatusHistoryDomain(BannerStatusHistoryDomain $bannerStatusHistoryDomain)
    {
        $this->bannerStatusHistoryDomain = $bannerStatusHistoryDomain;
    }

    /**
     * @param BannerStatusForm $bannerStatusForm
     */
    public function setBannerStatusForm(BannerStatusForm $bannerStatusForm)
    {
        $this->bannerStatusForm = $bannerStatusForm;
    }

    /**
     * @param Auth $auth
     */
    public function setAuthService(Auth $auth)
    {
        $this->auth = $auth;
    }



}
