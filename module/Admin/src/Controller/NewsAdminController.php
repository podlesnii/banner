<?php

namespace Admin\Controller;

use Admin\Form\NewsForm;
use Application\Model\NewsDomain;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Zend\Paginator\Paginator;


/**
 * Class NewsAdminController
 * @package Admin\Controller
 */
class NewsAdminController extends AbstractActionController
{

    /** @var NewsDomain */
    protected $newsDomain;

    /** @var NewsForm */
    protected $newsForm;


    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        $paginator = $this->getListPaginator();

        $viewModel = new ViewModel([
            'paginator' => $paginator,
        ]);

        return $viewModel;
    }

    /**
     * List Action does same functionality as indexAction, but has different view and disabled layout, because
     * it's used for ajax calls
     *
     * @return ViewModel
     */
    public function listAction()
    {
        $paginator = $this->getListPaginator();

        $viewModel = new ViewModel([
            'paginator' => $paginator,
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }


    /**
     * @return JsonModel|ViewModel
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addAction()
    {
        $form = new NewsForm();

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost(); // Fill in the form with POST data
            $form->setData($data);

            if ($form->isValid()) {  // Validate form
                $data = $form->getData(); // Get filtered and validated data
                $this->newsDomain->addNews($data);  // Create News
                $notyParameters = ['type' => 'success', 'message' => 'Successfully added!'];

                /**
                 * In case if form is valid we do not need to return form again. We return JSON with 'callbacks' method,
                 * which is validated in admin.js and if it's values are set correctly, callbacks are executed.
                 */
                return new JsonModel([
                    'callbacks' => [
                        [
                            'callback' => 'loadDataToContainer',
                            'arguments' => [
                                'records-container',
                                $this->url()->fromRoute('admin', ['controller' => 'newsadmin', 'action' => 'list']),
                            ],
                        ], [
                            'callback' => 'hideModal',
                            'arguments' => [],
                        ],
                    ],
                    'notifications' => [[
                        'type' => $notyParameters['type'],
                        'message' => $notyParameters['message'],
                    ]],
                ]);
            }
        }

        $viewModel = new ViewModel([
            'form' => $form,
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }

    /**
     * @return bool|JsonModel|ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateAction()
    {
        $id = $this->params()->fromQuery('id');

        if (!is_numeric($id)) {
            return false;
        }

        $news = $this->newsDomain->find($id);

        if (!$news) {
            return false;
        }

        $this->newsForm->populateValues($news->getArrayCopy()); //Populate form with array values from News entity

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost(); // Fill in the form with POST data
            $this->newsForm->setData($data);

            if ($this->newsForm->isValid()) {  // Validate form
                $data = $this->newsForm->getData(); // Get filtered and validated data
                $this->newsDomain->updateNews($news, $data);  // Update existing news
                $notyParameters = ['type' => 'success', 'message' => 'Successfully updated!'];

                /**
                 * In case if form is valid we do not need to return form again. We return JSON with 'callbacks' method,
                 * which is validated in admin.js and if it's values are set correctly, callbacks are executed.
                 */
                return new JsonModel([
                    'callbacks' => [
                        [
                            'callback' => 'loadDataToContainer',
                            'arguments' => [
                                'records-container',
                                $this->url()->fromRoute('admin', ['controller' => 'newsadmin', 'action' => 'list']),
                            ],
                        ], [
                            'callback' => 'hideModal',
                            'arguments' => [],
                        ],
                    ],
                    'notifications' => [[
                        'type' => $notyParameters['type'],
                        'message' => $notyParameters['message'],
                    ]],
                ]);
            }
        }

        $viewModel = new ViewModel([
            'form' => $this->newsForm,
            'newsId' => $news->getId(),
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }


    /**
     * @return bool|JsonModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteAction()
    {
        $id = $this->params()->fromQuery('id');

        if (!is_numeric($id)) {
            return false;
        }

        $news = $this->newsDomain->find($id);

        if (!$news) {
            return false;
        }

        $this->newsDomain->deleteNews($news);
        $notyParameters = ['type' => 'info', 'message' => 'Successfully deleted!!'];

        /**
         * We do not transmit 'hideModal' function call here, because confirmation modal will be hidden anyway
         */
        return new JsonModel([
            'callbacks' => [
                [
                    'callback' => 'loadDataToContainer',
                    'arguments' => [
                        'records-container',
                        $this->url()->fromRoute('admin', ['controller' => 'newsadmin', 'action' => 'list']),
                    ],
                ],
            ],
            'notifications' => [[
                'type' => $notyParameters['type'],
                'message' => $notyParameters['message'],
            ]],
        ]);
    }


    /**
     * @return Paginator
     */
    protected function getListPaginator()
    {
        $query = $this->newsDomain->getNews();

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new Paginator($adapter);

        $page = $this->params()->fromQuery('page');

        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);

        if ($page > $paginator->count()) {
            $paginator->setCurrentPageNumber(1);
        }

        return $paginator;
    }

    /**
     * @param NewsDomain $newsDomain
     */
    public function setNewsDomain(NewsDomain $newsDomain)
    {
        $this->newsDomain = $newsDomain;
    }

    /**
     * @param NewsForm $newsForm
     */
    public function setNewsForm(NewsForm $newsForm)
    {
        $this->newsForm = $newsForm;
    }

}
