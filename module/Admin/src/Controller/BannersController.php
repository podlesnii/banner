<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Admin\Form\BannerForm;
use Admin\Form\ImageForm;
use Admin\Service\AddFromFile;
use Admin\Service\YMapAddresses;
use Application\Entity\Banner;
use Application\Model\BannerDomain;
use Application\Model\ImageDomain;
use Application\Service\Auth;
use Application\Service\Notifications;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Zend\Paginator\Paginator;
use SimpleXLSX;


/**
 * Class BannersController
 * @package Admin\Controller
 */
class BannersController extends AbstractActionController
{

    /** @var BannerDomain */
    protected $bannerDomain;

    /** @var ImageDomain */
    protected $imageDomain;

    /** @var BannerForm */
    protected $bannerForm;

    /** @var Auth $auth */
    protected $auth;

    /** @var Notifications */
    protected $notificationsService;

    /** @var AddFromFile $addFromFileService */
    protected $addFromFileService;

    /** @var YMapAddresses $YMapAddressesService */
    protected $YMapAddresses;



    public function indexAction()
    {
        $paginator = $this->getListPaginator();

        $viewModel = new ViewModel([
            'paginator' => $paginator,
        ]);

        return $viewModel;
    }

    /**
     * List Action does same functionality as indexAction, but has different view and disabled layout, because
     * it's used for ajax calls
     *
     * @return ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function listAction()
    {
        $paginator = $this->getListPaginator();

        $viewModel = new ViewModel([
            'paginator' => $paginator,
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }


    /**
     * @return bool|ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Yandex\Geo\Exception
     * @throws \Yandex\Geo\Exception\CurlError
     * @throws \Yandex\Geo\Exception\ServerError
     */
    public function addAction()
    {
        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost(); // Fill in the form with POST data
            $this->bannerForm->setData($data);

            if ($this->bannerForm->isValid()) {  // Validate form
                $data = $this->bannerForm->getData(); // Get filtered and validated data
                $currentUserId = $this->auth->getCurrentUser()->getId();

                $adress = $data['adress'];
                $city = $data['city'];

                //Balti in Yandex map is Beltsy
                if($city === "Balti"){
                    $city = "Beltsy";
                }

                $coordinates = $this->YMapAddresses->getCoordinates($city, $adress);
                $data['latitude'] = $coordinates['latitude'];
                $data['longitude'] = $coordinates['longitude'];

                $this->bannerDomain->addBanner($data, $currentUserId);  // Create new banner

                /**
                 * In case if form is valid we redirect customer to banner list page
                 */
                $this->redirect()->toRoute('admin', ['controller' => 'banners', 'action' => 'index']);

                return true;
            }
        }

        $viewModel = new ViewModel([
            'form' => $this->bannerForm,
         ]);

        return $viewModel;
    }


    /**
     * @return ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Yandex\Geo\Exception
     * @throws \Yandex\Geo\Exception\CurlError
     * @throws \Yandex\Geo\Exception\ServerError
     */
    public function addManyAction()
    {
        
        $addBanners = null;
        $wrongBannerIds = null;
        $xlsx = null;
        if ($this->getRequest()->isPost()) {
            $dataFromFile = $_FILES['file'];
            if ($dataFromFile['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                if ($dataFromFile['name'] !== '' and $dataFromFile['type'] !== '') {
                    $xlsx = SimpleXLSX::parse($dataFromFile['tmp_name']);
                    $currentUserId = $this->auth->getCurrentUser()->getId();
                    $addBanners = $this->addFromFileService->addBannersFromFile($xlsx, $currentUserId);

                    if ($addBanners['wrongBannerIds'] != null) {
                        foreach ($addBanners['wrongBannerIds'] as $wrongBanner) {
                            if ($wrongBanner['type'] != null and $wrongBanner['reason'] != null) {
                                $wrongBannerIds[] = $wrongBanner;
                                $parameters = ['type' => 'warning', 'message' => $wrongBanner['reason']];
                                $this->notificationsService->addNotificationMessage($parameters);
                            }
                        }
                    }

                    if ($addBanners['addedBanners'] != null) {
                        $parameters = ['type' => 'success', 'message' => 'Successfully added!'];
                        $this->notificationsService->addNotificationMessage($parameters);
                    }
                }
            } else {
                //Wrong type of file
                $parameters = ['type' => 'warning', 'message' => 'Please select Excell file under 10mb!'];
                $this->notificationsService->addNotificationMessage($parameters);
            }
            if ($dataFromFile == null) {
                //Wrong data in file
                $parameters = ['type' => 'warning', 'message' => 'File exceeds size limit!'];
                $this->notificationsService->addNotificationMessage($parameters);
            }


        }
        $viewModel = new ViewModel([
            'xlsx' => $xlsx,
            'wrongBannerIds' => $wrongBannerIds,
            'addedBanners' => $addBanners['addedBanners']
        ]);

        return $viewModel;
    }


    /**
     * @return bool|JsonModel|ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateAction()
    {
        $id = $this->params()->fromQuery('id');

        if (!is_numeric($id)) {
            return false;
        }

        $banner = $this->bannerDomain->find($id);

        //Get Banner Images
        $bannerOldImages = $banner->getImages();
        $oldImages = [];
        foreach ($bannerOldImages as $i){
            $path = $i->getPath();
            $oldImages[] = $path;
        }

        if (!$banner) {
            return false;
        }

        $this->bannerForm->populateValues($banner->getArrayCopy()); //Populate form with array values from Banner entity

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost(); // Fill in the form with POST data
            $this->bannerForm->setData($data);

            if ($this->bannerForm->isValid()) {  // Validate form
                $data = $this->bannerForm->getData(); // Get filtered and validated data

                if($data['image1'] != null && $data['image1'] != ''){
                    if ($oldImages[0]) {
                        $images = $this->imageDomain->findBy(['path' => $oldImages[0]]);
                        foreach ($images as $img) {
                            $this->imageDomain->updateImage($img, $data['image1']);
                        }
                    } else {
                        $this->imageDomain->addImage($banner, $data['image1']);
                    }
                }
                if ($data['image2'] != null && $data['image2'] != '') {
                    if ($oldImages[1]) {
                        $images = $this->imageDomain->findBy(['path' => $oldImages[1]]);
                        foreach ($images as $img) {
                            $this->imageDomain->updateImage($img, $data['image2']);
                        }
                    } else {
                        $this->imageDomain->addImage($banner, $data['image2']);
                    }
                }
                if($data['image3'] != null && $data['image3'] != ''){
                    if ($oldImages[2]) {
                        $images = $this->imageDomain->findBy(['path' => $oldImages[2]]);
                        foreach ($images as $img) {
                            $this->imageDomain->updateImage($img, $data['image3']);
                        }
                    }else{
                        $this->imageDomain->addImage($banner, $data['image3']);
                    }
                }
                if($data['image4'] != null && $data['image4'] != ''){
                    if ($oldImages[3]) {
                        $images = $this->imageDomain->findBy(['path' => $oldImages[3]]);
                        foreach ($images as $img) {
                            $this->imageDomain->updateImage($img, $data['image4']);
                        }
                    }else{
                        $this->imageDomain->addImage($banner, $data['image4']);
                    }
                }
                if($data['image5'] != null && $data['image5'] != ''){
                    if ($oldImages[4]) {
                        $images = $this->imageDomain->findBy(['path' => $oldImages[4]]);
                        foreach ($images as $img) {
                            $this->imageDomain->updateImage($img, $data['image5']);
                        }
                    }else{
                        $this->imageDomain->addImage($banner, $data['image5']);
                    }
                }

                $this->bannerDomain->updateBanner($banner, $data);  // Update existing banner

                /**
                 * In case if form is valid we redirect customer to banner list page
                 */
                $this->redirect()->toRoute('admin', ['controller' => 'banners', 'action' => 'index']);

                return true;
            }
        }

        $viewModel = new ViewModel([
            'form'     => $this->bannerForm,
            'bannerId' => $banner->getId(),
        ]);

        return $viewModel;
    }

    /**
     * @return bool|JsonModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteAction()
    {
        $id = $this->params()->fromQuery('id');

        if (!is_numeric($id)) {
            return false;
        }

        $banner = $this->bannerDomain->find($id);

        if (!$banner) {
            return false;
        }
        $some = $banner->getImages();

        $bannerImages = $this->imageDomain->findBy(['banner' => $banner->getId()]);
        $this->imageDomain->deleteImages($bannerImages);
        $this->bannerDomain->deleteBanner($banner);
        $notyParameters = ['type' => 'info', 'message' => 'Successfully deleted!!'];

        /**
         * We do not transmit 'hideModal' function call here, because confirmation modal will be hidden anyway
         */
        return new JsonModel([
            'callbacks' => [
                [
                    'callback'  => 'loadDataToContainer',
                    'arguments' => [
                        'records-container',
                        $this->url()->fromRoute('admin', ['controller' => 'banners', 'action' => 'list']),
                    ],
                ],
            ],
            'notifications' => [[
                'type' => $notyParameters['type'],
                'message' => $notyParameters['message'],
            ]],
        ]);
    }


    /**
     * @return bool|JsonModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function uploadAction()
    {
        $id = $this->params()->fromQuery('id');

        if (!is_numeric($id)) {
            return false;
        }

        /** @var Banner $banner */
        $banner = $this->bannerDomain->find($id);

        if (!$banner) {
            return false;
        }

        $return = [];
        $form = new ImageForm($banner->getId());

        if ($this->getRequest()->isPost()) {
            $request = $this->getRequest();
            $data = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );

            $form->setData($data);

            if ($form->isValid()) {
                $data = $form->getData();
                $files = [];
                foreach ($data['files'] as $image) {
                    $path = str_replace($form->getPublicPath(), '', $image['tmp_name']);
                    $this->imageDomain->addBannerImage($banner, [
                        'title' => $image['name'],
                        'path'  => $path,
                    ]);
                    $files[] = [
                        'url'          => $path,
                        'thumbnailUrl' => $path,
                        'deleteUrl'    => str_replace($form->getPublicPath(), '', $image['tmp_name']),
                        'deleteType'   => 'DELETE',
                        'type'         => $image['type'],
                        'size'         => $image['size'],
                        'name'         => $image['name'],
                    ];
                }
                $return = [
                    'files' => $files,
                ];
            }
        }

        return new JsonModel($return);
    }


    /**
     * @return Paginator
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    protected function getListPaginator()
    {
        $query = $this->bannerDomain->getBanners([
            'currentUserRole' => $this->auth->getCurrentUser()->getRole()->getName(),
            'currentUserId'   => $this->auth->getCurrentUser()->getId(),
        ]);

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new Paginator($adapter);

        $page = $this->params()->fromQuery('page');

        $paginator->setItemCountPerPage(250);
        $paginator->setCurrentPageNumber($page);

        if ($page > $paginator->count()) {
            $paginator->setCurrentPageNumber(1);
        }

        return $paginator;
    }

    /**
     * @param BannerDomain $bannerDomain
     */
    public function setBannerDomain(BannerDomain $bannerDomain)
    {
        $this->bannerDomain = $bannerDomain;
    }

    /**
     * @param ImageDomain $imageDomain
     */
    public function setImageDomain(ImageDomain $imageDomain)
    {
        $this->imageDomain = $imageDomain;
    }

    /**
     * @param BannerForm $bannerForm
     */
    public function setBannerForm(BannerForm $bannerForm)
    {
        $this->bannerForm = $bannerForm;
    }

    /**
     * @param Auth $auth
     */
    public function setAuthService(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param Notifications $notificationsService
     */
    public function setNotificationsService($notificationsService)
    {
        $this->notificationsService = $notificationsService;
    }

    /**
     * @param AddFromFile $addFromFileService
     */
    public function setAddFromFileService($addFromFileService)
    {
        $this->addFromFileService = $addFromFileService;
    }

    /**
     * @param YMapAddresses $YMapAddresses
     */
    public function setYMapAddresses($YMapAddresses)
    {
        $this->YMapAddresses = $YMapAddresses;
    }


}
