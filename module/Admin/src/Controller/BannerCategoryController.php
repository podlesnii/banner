<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Admin\Form\BannerCategoryForm;
use Application\Model\BannerCategoryDomain;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Zend\Paginator\Paginator;

/**
 * Class BannerCategoryController
 * @package Admin\Controller
 */
class BannerCategoryController extends AbstractActionController
{

    /** @var BannerCategoryDomain */
    protected $bannerCategoryDomain;


    /**
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        $paginator = $this->getListPaginator();

        $viewModel = new ViewModel([
            'paginator' => $paginator,
        ]);

        return $viewModel;
    }

    /**
     * List Action does same functionality as indexAction, but has different view and disabled layout, because
     * it's used for ajax calls
     *
     * @return ViewModel
     */
    public function listAction()
    {
        $paginator = $this->getListPaginator();

        $viewModel = new ViewModel([
            'paginator' => $paginator,
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }


    /**
     * @return JsonModel|ViewModel
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addAction()
    {
        $form = new BannerCategoryForm();

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost(); // Fill in the form with POST data
            $form->setData($data);

            if ($form->isValid()) {  // Validate form
                $data = $form->getData(); // Get filtered and validated data
                $this->bannerCategoryDomain->addBannerCategory($data);  // Create Banner Category
                $notyParameters = ['type' => 'success', 'message' => 'Successfully added!!'];

                /**
                 * In case if form is valid we do not need to return form again. We return JSON with 'callbacks' method,
                 * which is validated in admin.js and if it's values are set correctly, callbacks are executed.
                 */
                return new JsonModel([
                    'callbacks' => [
                        [
                            'callback' => 'loadDataToContainer',
                            'arguments' => [
                                'records-container',
                                $this->url()->fromRoute('admin', ['controller' => 'bannercategory', 'action' => 'list']),
                            ],
                        ], [
                            'callback' => 'hideModal',
                            'arguments' => [],
                        ],
                    ],
                    'notifications' => [[
                        'type' => $notyParameters['type'],
                        'message' => $notyParameters['message'],
                    ]],
                ]);
            }
        }

        $viewModel = new ViewModel([
            'form' => $form,
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }


    /**
     * @return bool|JsonModel|ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateAction()
    {
        $form = new BannerCategoryForm();
        $id = $this->params()->fromQuery('id');

        if (!is_numeric($id)) {
            return false;
        }

        $bannerCategory = $this->bannerCategoryDomain->find($id);

        if (!$bannerCategory) {
            return false;
        }

        $form->populateValues($bannerCategory->getArrayCopy()); //Populate form with array values from BannerCategory entity

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost(); // Fill in the form with POST data
            $form->setData($data);

            if ($form->isValid()) {  // Validate form
                $data = $form->getData(); // Get filtered and validated data
                $this->bannerCategoryDomain->updateBannerCategory($bannerCategory, $data);  // Update existing Category
                $notyParameters = ['type' => 'success', 'message' => 'Successfully updated!'];

                /**
                 * In case if form is valid we do not need to return form again. We return JSON with 'callbacks' method,
                 * which is validated in admin.js and if it's values are set correctly, callbacks are executed.
                 */
                return new JsonModel([
                    'callbacks' => [
                        [
                            'callback' => 'loadDataToContainer',
                            'arguments' => [
                                'records-container',
                                $this->url()->fromRoute('admin', ['controller' => 'bannercategory', 'action' => 'list']),
                            ],
                        ], [
                            'callback' => 'hideModal',
                            'arguments' => [],
                        ],
                    ],
                    'notifications' => [[
                        'type' => $notyParameters['type'],
                        'message' => $notyParameters['message'],
                    ]],
                ]);
            }
        }

        $viewModel = new ViewModel([
            'form' => $form,
            'bannerCategoryId' => $bannerCategory->getId(),
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }


    /**
     * @return bool|JsonModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteAction()
    {
        $id = $this->params()->fromQuery('id');

        if (!is_numeric($id)) {
            return false;
        }

        $bannerCategory = $this->bannerCategoryDomain->find($id);

        if (!$bannerCategory) {
            return false;
        }

        $this->bannerCategoryDomain->deleteBannerCategory($bannerCategory);
        $notyParameters = ['type' => 'info', 'message' => 'Successfully deleted!!'];

        /**
         * We do not transmit 'hideModal' function call here, because confirmation modal will be hidden anyway
         */
        return new JsonModel([
            'callbacks' => [
                [
                    'callback' => 'loadDataToContainer',
                    'arguments' => [
                        'records-container',
                        $this->url()->fromRoute('admin', ['controller' => 'bannercategory', 'action' => 'list']),
                    ],
                ],
            ],
            'notifications' => [[
                'type' => $notyParameters['type'],
                'message' => $notyParameters['message'],
            ]],
        ]);
    }

    /**
     * @return Paginator
     */
    protected function getListPaginator()
    {
        $query = $this->bannerCategoryDomain->getBannerCategory();

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new Paginator($adapter);

        $page = $this->params()->fromQuery('page');

        $paginator->setItemCountPerPage(5);
        $paginator->setCurrentPageNumber($page);

        if ($page > $paginator->count()) {
            $paginator->setCurrentPageNumber(1);
        }

        return $paginator;
    }

    /**
     * @param BannerCategoryDomain $bannerCategoryDomain
     */
    public function setBannerCategoryDomain(BannerCategoryDomain $bannerCategoryDomain)
    {
        $this->bannerCategoryDomain = $bannerCategoryDomain;
    }


}
