<?php

namespace Admin\Controller\Factory;

use Admin\Controller\BannerStatusesController;
use Admin\Form\BannerStatusForm;
use Application\Model\BannerStatusDomain;
use Application\Model\BannerStatusHistoryDomain;
use Application\Service\Auth;
use Application\Service\DomainModelManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Admin\Controller\BannersController;


/**
 * Class BannerStatusesControllerFactory
 * @package Admin\Controller\Factory
 */
class BannerStatusesControllerFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     *
     * @return BannersController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke (ContainerInterface $container, $requestedName, array $options = null )
    {
        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        $bannerStatusDomain = $domainManager->get(BannerStatusDomain::class);
        $bannerStatusHistoryDomain= $domainManager->get(BannerStatusHistoryDomain::class);

        $bannerStatusForm = new BannerStatusForm();

        $auth = $container->get(Auth::class);

        $bannerStatusesController = new BannerStatusesController();
        $bannerStatusesController->setBannerStatusDomain($bannerStatusDomain);
        $bannerStatusesController->setBannerStatusHistoryDomain($bannerStatusHistoryDomain);
        $bannerStatusesController->setBannerStatusForm($bannerStatusForm);
        $bannerStatusesController->setAuthService($auth);


        // Instantiate the controller and inject dependencies
        return $bannerStatusesController;
    }
}