<?php

namespace Admin\Controller\Factory;

use Admin\Controller\BannerCategoryController;
use Application\Model\BannerCategoryDomain;
use Application\Service\DomainModelManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class BannerCategoryControllerFactory
 * @package Admin\Controller\Factory
 */
class BannerCategoryControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @return BannerCategoryController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        $bannerCategoryDomain = $domainManager->get(BannerCategoryDomain::class);

        $bannerCategoryController = new BannerCategoryController();
        $bannerCategoryController->setBannerCategoryDomain($bannerCategoryDomain);

        // Instantiate the controller and inject dependencies
        return $bannerCategoryController;
    }
}




