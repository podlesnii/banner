<?php

namespace Admin\Controller\Factory;

use Admin\Form\CityForm;
use Application\Model\CityDomain;
use Application\Service\DomainModelManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Admin\Controller\CityController;


/**
 * Class CityControllerFactory
 * @package Admin\Controller\Factory
 */
class CityControllerFactory implements FactoryInterface
{


    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return CityController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke (ContainerInterface $container, $requestedName, array $options = null )
    {
        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        $cityDomain = $domainManager->get(CityDomain::class);

        $cityForm = new CityForm();


        $cityController = new CityController();
        $cityController->setCityDomain($cityDomain);
        $cityController->setCityForm($cityForm);

        // Instantiate the controller and inject dependencies
        return $cityController;
    }
}