<?php

namespace Admin\Controller\Factory;

use Admin\Form\BannerForm;
use Admin\Service\AddFromFile;
use Admin\Service\YMapAddresses;
use Application\Model\BannerDomain;
use Application\Model\ImageDomain;
use Application\Service\Auth;
use Application\Service\DomainModelManager;
use Application\Service\Notifications;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Admin\Controller\BannersController;


/**
 * Class BannersControllerFactory
 * @package Admin\Controller\Factory
 */
class BannersControllerFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     *
     * @return BannersController
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke (ContainerInterface $container, $requestedName, array $options = null )
    {
        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        /** @var BannerDomain $bannerDomain */
        $bannerDomain = $domainManager->get(BannerDomain::class);
        /** @var ImageDomain $imageDomain */
        $imageDomain = $domainManager->get(ImageDomain::class);

        $bannerForm = $container->get(BannerForm::class);

        $auth = $container->get(Auth::class);

        $notificationsService = $container->get(Notifications::class);

        $addFromFileService = $container->get(AddFromFile::class);

        $YMapAddressesService = $container->get(YMapAddresses::class);

        $bannersController = new BannersController();
        $bannersController->setBannerDomain($bannerDomain);
        $bannersController->setBannerForm($bannerForm);
        $bannersController->setAuthService($auth);
        $bannersController->setImageDomain($imageDomain);
        $bannersController->setNotificationsService($notificationsService);
        $bannersController->setAddFromFileService($addFromFileService);
        $bannersController->setYMapAddresses($YMapAddressesService);

        // Instantiate the controller and inject dependencies
        return $bannersController;
    }
}