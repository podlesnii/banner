<?php

namespace Admin\Controller\Factory;

use Admin\Form\UserForm;
use Application\Model\UserDomain;
use Application\Service\DomainModelManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Admin\Controller\UsersController;


/**
 * Class UserControllerFactory
 * @package Admin\Controller\Factory
 */
class UsersControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @return UsersController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke ( ContainerInterface $container, $requestedName, array $options = null )
    {
        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        $userDomain = $domainManager->get(UserDomain::class);

        $userForm = $container->get(UserForm::class);

        $userController = new UsersController();
        $userController->setUserDomain($userDomain);
        $userController->setUserForm($userForm);

        // Instantiate the controller and inject dependencies
        return $userController;




    }
}