<?php

namespace Admin\Controller\Factory;

use Admin\Controller\InvoiceController;
use Admin\Form\InvoiceForm;
use Application\Model\InvoiceDomain;
use Application\Service\Auth;
use Application\Service\DomainModelManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class InvoiceControllerFactory
 * @package Admin\Controller\Factory
 */
class InvoiceControllerFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     *
     * @return InvoiceController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke (ContainerInterface $container, $requestedName, array $options = null )
    {
        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        $invoiceDomain = $domainManager->get(InvoiceDomain::class);

        $auth = $container->get(Auth::class);

        $invoiceForm = new InvoiceForm();

        $invoiceController = new InvoiceController();
        $invoiceController->setInvoiceDomain($invoiceDomain);
        $invoiceController->setInvoiceForm($invoiceForm);
        $invoiceController->setAuth($auth);


        // Instantiate the controller and inject dependencies
        return $invoiceController;
   }
}