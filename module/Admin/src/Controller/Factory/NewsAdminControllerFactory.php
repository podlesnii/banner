<?php

namespace Admin\Controller\Factory;

use Admin\Controller\NewsAdminController;
use Admin\Form\NewsForm;
use Application\Model\NewsDomain;
use Application\Service\DomainModelManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class NewsAdminControllerFactory
 * @package Admin\Controller\Factory
 */
class NewsAdminControllerFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return NewsAdminController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        $newsDomain = $domainManager->get(NewsDomain::class);

        $newsForm = new NewsForm();

        $newsAdminController = new NewsAdminController();
        $newsAdminController->setNewsDomain($newsDomain);
        $newsAdminController->setNewsForm($newsForm);

        // Instantiate the controller and inject dependencies
        return $newsAdminController;
    }
}




