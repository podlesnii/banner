<?php

namespace Admin\Controller\Factory;

use Admin\Controller\BannerStatusesHistoryController;
use Application\Model\BannerStatusHistoryDomain;
use Application\Service\Auth;
use Application\Service\DomainModelManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Admin\Controller\BannersController;


/**
 * Class BannerStatusesHistoryControllerFactory
 * @package Admin\Controller\Factory
 */
class BannerStatusesHistoryControllerFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     *
     * @return BannersController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke (ContainerInterface $container, $requestedName, array $options = null )
    {
        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        $bannerStatusHistoryDomain= $domainManager->get(BannerStatusHistoryDomain::class);

        $auth = $container->get(Auth::class);

        $bannerStatusesController = new BannerStatusesHistoryController();
        $bannerStatusesController->setBannerStatusHistoryDomain($bannerStatusHistoryDomain);
        $bannerStatusesController->setAuthService($auth);

        // Instantiate the controller and inject dependencies
        return $bannerStatusesController;
    }
}