<?php

namespace Admin\Controller\Factory;

use Acl\Model\RoleDomain;
use Application\Service\DomainModelManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Admin\Controller\RolesController;


/**
 * Class RolesControllerFactory
 * @package Admin\Controller\Factory
 */
class RolesControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @return RolesController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke ( ContainerInterface $container, $requestedName, array $options = null )
    {
        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        $roleDomain = $domainManager->get(RoleDomain::class);

        $roleController = new RolesController();
        $roleController->setRoleDomain($roleDomain);

        // Instantiate the controller and inject dependencies
        return $roleController;
    }
}