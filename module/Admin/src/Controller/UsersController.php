<?php

namespace Admin\Controller;

use Admin\Form\UserForm;
use Application\Model\UserDomain;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Zend\Paginator\Paginator;


/**
 * Class UsersController
 * @package Admin\Controller
 */
class UsersController extends AbstractActionController
{

    /** @var UserDomain */
    protected $userDomain;


    /** @var UserForm */
    protected $userForm;


    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        $paginator = $this->getListPaginator();

        $viewModel = new ViewModel([
            'paginator' => $paginator,
        ]);

        return $viewModel;
    }

    /**
     * List Action does same functionality as indexAction, but has different view and disabled layout, because
     * it's used for ajax calls
     *
     * @return ViewModel
     */
    public function listAction()
    {
        $paginator = $this->getListPaginator();

        $viewModel = new ViewModel([
            'paginator' => $paginator,
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }


    /**
     * @return JsonModel|ViewModel
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addAction()
    {
        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost(); // Fill in the form with POST data
            $this->userForm->setData($data);

            if ($this->userForm->isValid()) {  // Validate form
                $data = $this->userForm->getData(); // Get filtered and validated data
                $this->userDomain->addUser($data);  // Create new user
                $notyParameters = ['type' => 'success', 'message' => 'Successfully added!'];

                /**
                 * In case if form is valid we do not need to return form again. We return JSON with 'callbacks' method,
                 * which is validated in admin.js and if it's values are set correctly, callbacks are executed.
                 */
                return new JsonModel([
                    'callbacks' => [
                        [
                            'callback' => 'loadDataToContainer',
                            'arguments' => [
                                'records-container',
                                $this->url()->fromRoute('admin', ['controller' => 'users', 'action' => 'list']),
                            ],
                        ], [
                            'callback' => 'hideModal',
                            'arguments' => [],
                        ],
                    ],
                    'notifications' => [[
                        'type' => $notyParameters['type'],
                        'message' => $notyParameters['message'],
                    ]],
                ]);
            }
        }

        $viewModel = new ViewModel([
            'form' => $this->userForm,
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }

    /**
     * @return bool|JsonModel|ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateAction()
    {
        $id = $this->params()->fromQuery('id');

        if (!is_numeric($id)) {
            return false;
        }

        $user = $this->userDomain->find($id);

        if (!$user) {
            return false;
        }

        $this->userForm->populateValues($user->getArrayCopy()); //Populate form with array values from User entity

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost(); // Fill in the form with POST data
            $this->userForm->setData($data);

            if ($this->userForm->isValid()) {  // Validate form
                $data = $this->userForm->getData(); // Get filtered and validated data
                $this->userDomain->updateUser($user, $data);  // Update existing user
                $notyParameters = ['type' => 'success', 'message' => 'Successfully updated!'];

                /**
                 * In case if form is valid we do not need to return form again. We return JSON with 'callbacks' method,
                 * which is validated in admin.js and if it's values are set correctly, callbacks are executed.
                 */
                return new JsonModel([
                    'callbacks' => [
                        [
                            'callback' => 'loadDataToContainer',
                            'arguments' => [
                                'records-container',
                                $this->url()->fromRoute('admin', ['controller' => 'users', 'action' => 'list']),
                            ],
                        ],
                        [
                            'callback' => 'hideModal',
                            'arguments' => [],
                        ],
                    ],
                    'notifications' => [[
                        'type' => $notyParameters['type'],
                        'message' => $notyParameters['message'],
                    ]],
                ]);
            }
        }

        $viewModel = new ViewModel([
            'form' => $this->userForm,
            'userId' => $user->getId(),
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }


    /**
     * @return bool|JsonModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteAction()
    {
        $id = $this->params()->fromQuery('id');

        if (!is_numeric($id)) {
            return false;
        }

        $user = $this->userDomain->find($id);

        if (!$user) {
            return false;
        }

        $this->userDomain->deleteUser($user);
        $notyParameters = ['type' => 'info', 'message' => 'Successfully deleted!!'];


        /**
         * We do not transmit 'hideModal' function call here, because confirmation modal will be hidden anyway
         */
        return new JsonModel([
            'callbacks' => [
                [
                    'callback' => 'loadDataToContainer',
                    'arguments' => [
                        'records-container',
                        $this->url()->fromRoute('admin', ['controller' => 'users', 'action' => 'list']),
                    ],
                ],
            ],
            'notifications' => [[
                'type' => $notyParameters['type'],
                'message' => $notyParameters['message'],
            ]],
        ]);
    }


    /**
     * @return Paginator
     */
    protected function getListPaginator()
    {
        $filter = null;
        $query = $this->userDomain->getUsers($filter);

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new Paginator($adapter);

        $page = $this->params()->fromQuery('page');

        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);

        if ($page > $paginator->count()) {
            $paginator->setCurrentPageNumber(1);
        }

        return $paginator;
    }

    /**
     * @param UserDomain $userDomain
     */
    public function setUserDomain(UserDomain $userDomain)
    {
        $this->userDomain = $userDomain;
    }

    /**
     * @param UserForm $userForm
     */
    public function setUserForm(UserForm $userForm)
    {
        $this->userForm = $userForm;
    }

}
