<?php

namespace Admin\Controller;

use Acl\Model\RoleDomain;
use Admin\Form\RoleForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Zend\Paginator\Paginator;


/**
 * Class RolesController
 * @package Admin\Controller
 */
class RolesController extends AbstractActionController
{

    /** @var RoleDomain */
    protected $roleDomain;


    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        $paginator = $this->getListPaginator();

        $viewModel = new ViewModel([
            'paginator' => $paginator,
        ]);

        return $viewModel;
    }

    /**
     * List Action does same functionality as indexAction, but has different view and disabled layout, because
     * it's used for ajax calls
     *
     * @return ViewModel
     */
    public function listAction()
    {
        $paginator = $this->getListPaginator();

        $viewModel = new ViewModel([
            'paginator' => $paginator,
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }


    /**
     * @return bool|JsonModel|ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateAction()
    {
        $form = new RoleForm();
        $id = $this->params()->fromQuery('id');

        if (!is_numeric($id)) {
            return false;
        }

        $role = $this->roleDomain->find($id);

        if (!$role) {
            return false;
        }

        $form->populateValues($role->getArrayCopy()); //Populate form with array values from Role entity

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost(); // Fill in the form with POST data
            $form->setData($data);

            if ($form->isValid()) {  // Validate form
                $data = $form->getData(); // Get filtered and validated data
                $this->roleDomain->updateRole($role, $data);  // Update existing role
                $notyParameters = ['type' => 'success', 'message' => 'Successfully updated!'];

                /**
                 * In case if form is valid we do not need to return form again. We return JSON with 'callbacks' method,
                 * which is validated in admin.js and if it's values are set correctly, callbacks are executed.
                 */
                return new JsonModel([
                    'callbacks' => [
                        [
                            'callback' => 'loadDataToContainer',
                            'arguments' => [
                                'records-container',
                                $this->url()->fromRoute('admin', ['controller' => 'roles', 'action' => 'list']),
                            ],
                        ], [
                            'callback' => 'hideModal',
                            'arguments' => [],
                        ],
                    ],
                    'notifications' => [[
                        'type' => $notyParameters['type'],
                        'message' => $notyParameters['message'],
                    ]],
                ]);
            }
        }

        $viewModel = new ViewModel([
            'form' => $form,
            'roleId' => $role->getId(),
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }


    /**
     * @return Paginator
     */
    protected function getListPaginator()
    {
        $query = $this->roleDomain->getRoles();

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new Paginator($adapter);

        $page = $this->params()->fromQuery('page');

        $paginator->setItemCountPerPage(5);
        $paginator->setCurrentPageNumber($page);

        if ($page > $paginator->count()) {
            $paginator->setCurrentPageNumber(1);
        }

        return $paginator;
    }

    /**
     * @param RoleDomain $roleDomain
     */
    public function setRoleDomain(RoleDomain $roleDomain)
    {
        $this->roleDomain = $roleDomain;
    }


}
