<?php

namespace Admin\Controller;

use Application\Model\BannerStatusHistoryDomain;
use Application\Service\Auth;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Zend\Paginator\Paginator;

/**
 * Class BannerStatusesHistoryController
 * @package Admin\Controller
 */
class BannerStatusesHistoryController extends AbstractActionController
{

    /** @var BannerStatusHistoryDomain */
    protected $bannerStatusHistoryDomain;

    /** @var Auth */
    protected $auth;


    /**
     * @return ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function indexAction()
    {
        $paginator = $this->getListPaginator();

        $viewModel = new ViewModel([
            'paginator' => $paginator,

        ]);

        return $viewModel;
    }


    /**
     * @return ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function listAction()
    {
        $paginator = $this->getListPaginator();


        $viewModel = new ViewModel([
            'paginator' => $paginator,
        ]);
        $viewModel->setTerminal(true);

        return $viewModel;
    }


    /**
     * @return bool|JsonModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteAction()
    {
        $id = $this->params()->fromQuery('id');

        if (!is_numeric($id)) {
            return false;
        }

        $bannerStatus = $this->bannerStatusHistoryDomain->find($id);

        if (!$bannerStatus) {
            return false;
        }

        $this->bannerStatusHistoryDomain->deleteBannerStatusHistory($bannerStatus);
        $notyParameters = ['type' => 'info', 'message' => 'Successfully deleted!!'];

        /**
         * We do not transmit 'hideModal' function call here, because confirmation modal will be hidden anyway
         */
        return new JsonModel([
            'callbacks' => [
                [
                    'callback'  => 'loadDataToContainer',
                    'arguments' => [
                        'records-container',
                        $this->url()->fromRoute('admin', ['controller' => 'bannerstatuseshistory', 'action' => 'list']),
                    ],
                ],
            ],
            'notifications' => [[
                'type' => $notyParameters['type'],
                'message' => $notyParameters['message'],
            ]],
        ]);
    }


    /**
     * @return Paginator
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    protected function getListPaginator()
    {
        $currentUser = $this->auth->getCurrentUser();

        $query = $this->bannerStatusHistoryDomain->getBannerStatusesHistory($currentUser);

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new Paginator($adapter);

        $page = $this->params()->fromQuery('page');

        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);

        if ($page > $paginator->count()) {
            $paginator->setCurrentPageNumber(1);
        }

        return $paginator;
    }

    /**
     * @param BannerStatusHistoryDomain $bannerStatusHistoryDomain
     */
    public function setBannerStatusHistoryDomain(BannerStatusHistoryDomain $bannerStatusHistoryDomain)
    {
        $this->bannerStatusHistoryDomain = $bannerStatusHistoryDomain;
    }


    /**
     * @param Auth $auth
     */
    public function setAuthService(Auth $auth)
    {
        $this->auth = $auth;
    }

}
