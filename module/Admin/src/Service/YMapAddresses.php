<?php


namespace Admin\Service;

use Yandex\Geo\Api;


/**
 * Class YMapAddresses
 * @package Admin\Service
 */
class YMapAddresses
{

    /**
     * @param $adress
     * @return array
     * @throws \Yandex\Geo\Exception
     * @throws \Yandex\Geo\Exception\CurlError
     * @throws \Yandex\Geo\Exception\ServerError
     */
    public function getCoordinates($city, $adress){

        $api = new Api();
        $api->setQuery($adress);
        $api->setLang(Api::LANG_US) // локаль ответа
            ->load();
        $response = $api->getResponse();

        // Список найденных точек
        $collection = $response->getList();
        foreach ($collection as $item) {
            if($item->getCountryCode() === "MD") {
                if($item->getLocalityName() === 'Chisinau'){
                    $lat = $item->getLatitude(); // широта
                    $long = $item->getLongitude(); // долгота
                }
            }
        }

        return [
            'latitude' => $lat,
            'longitude' => $long
        ];

    }




}


