<?php


namespace Admin\Service;


use Application\Entity\BannerCategory;
use Application\Model\BannerCategoryDomain;
use Application\Model\BannerDomain;
use Application\Model\CityDomain;
use Admin\Service\YMapAddresses;


/**
 * Class AddFromFile
 * @package Admin\Service
 */
class AddFromFile
{

    /** @var BannerDomain $bannerDomain */
    protected $bannerDomain;

    /** @var CityDomain $cityDomain */
    protected $cityDomain;

    /** @var BannerCategoryDomain $categoryDomain */
    protected $categoryDomain;

    /** @var YMapAddresses $YMapAddresses */
    protected $YMapAddresses;

    protected $xlsxMapper = [
        1 => 'banner_number',
        2 => 'name_city',
        3 => 'category',
        4 => 'description',
        5 => 'area',
        6 => 'adress',
        7 => 'price'
    ];


    /**
     * @param $xlsx
     * @param $currentUserId
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Yandex\Geo\Exception
     * @throws \Yandex\Geo\Exception\CurlError
     * @throws \Yandex\Geo\Exception\ServerError
     */
    public function addBannersFromFile($xlsx, $currentUserId){

        $currentUserError = null;
        $wrongBannerIds = null;
        $banners = null;

        // Checking if user exist
        if (is_null($currentUserId) && $currentUserId = '') {
            $currentUserError = true;
            return ([
                'currentUserError' => $currentUserError
            ]);
        } else {

            if ($xlsx != null) {
                $banners = [];
                foreach ($xlsx->rows() as $r => $k) {
                    // skip first row (column description)
                    if ($r == 0 || $r == 1) continue;
                    $banner = [];
                    foreach ($this->xlsxMapper as $column => $name) {
                        $banner[$name] = $k[$column];
                    }

                    $banners[$r] = $banner;

                    //Get Coordinates by Address
                    //Balti in Yandex map is Beltsy
                    if ($k[1] === "Balti") {
                        $k[1] = "Beltsy";
                    }
                    $coordinates = $this->YMapAddresses->getCoordinates($k[1], $k[5]);
                    $banners[$r]['latitude'] = $coordinates['latitude'];
                    $banners[$r]['longitude'] = $coordinates['longitude'];

                }

                $banners = $this->fillBannersCities($banners);
                $banners = $this->fillBannersCategories($banners);

                $wrongBannerIds = $this->analyseBanners($banners, $currentUserId);

                //Unset banners with errors (exists or with empty rows)
                if (!empty($wrongBannerIds)) {
                    $banners = array_diff_key($banners, array_flip(array_column($wrongBannerIds, 'id')));
                    $uniqueBanners  = array_unique($banners, 4);
                    if (count($uniqueBanners)) {
                        $this->bannerDomain->addManyBanners($uniqueBanners, $currentUserId);
                    }
                }
                if (empty($wrongBannerIds)) {
                        $this->bannerDomain->addManyBanners($banners, $currentUserId);
                }

            }


            return ([
                'userIdError' => $currentUserError,
                'wrongBannerIds' => $wrongBannerIds,
                'addedBanners' => $banners,
            ]);
        }
    }


    /**
     * @param $banners
     * @return mixed
     */
    protected function fillBannersCities($banners){
        $cityNames = array_column($banners, 'name_city');

        $cities = $this->cityDomain->getCities([
            'name_city' => $cityNames
        ]);

        $cityMapper = [];
        foreach ($cities as $city) {
            $cityMapper[$city['nameCity']] = $city['id'];
        }

        foreach ($banners as &$banner) {
            if (!empty($banner['name_city'] && !empty($cityMapper[$banner['name_city']]))) {
                $banner['city_id'] = $cityMapper[$banner['name_city']];
            }
        }
        return $banners;
    }

    /**
     * @param $banners
     * @return mixed
     */
    protected function fillBannersCategories($banners)
    {
        $categoryNames = array_column($banners, 'category');

        $categories = $this->categoryDomain->getBannerCategories([
            'name' => $categoryNames
        ]);

        $categoryMapper = [];
        foreach ($categories as $category) {
            $categoryMapper[$category['name']] = $category['id'];
        }

        foreach ($banners as &$banner) {
            if (!empty($categoryMapper[$banner['category']])) {
                $banner['category_id'] = $categoryMapper[$banner['category']];
            }
        }
        return $banners;
    }

    /**
     * @param $banners
     * @param $currentUserId
     * @return mixed
     */
    protected function analyseBanners($banners, $currentUserId)
    {
        $wrongBannerIds = [];

        // Getting all banners of current user
        $filter = ['currentUserId' => $currentUserId];
        $getBanners = $this->bannerDomain->getBanners($filter);
        $userBanners = $getBanners->execute();

        // Checking if banner of that user is already exist
        foreach ($userBanners as $userBanner) {
            foreach ($banners as $id => $banner) {
                if ($banner['banner_number'] == $userBanner['banner_number'] &&
                    $banner['name_city'] == $userBanner['city']['nameCity'] &&
                    $banner['category'] == $userBanner['category']['name'] &&
                    $banner['adress'] == $userBanner['adress'] &&
                    $banner['description'] == $userBanner['description'] &&
                    $banner['area'] == $userBanner['area'] &&
                    empty($wrongBannerIds[$id]['type'] == 'isExistErrors')) {
                    $wrongBannerIds[] = [
                        'id' => $id,
                        'type' => 'isExistErrors',
                        'reason' => 'Banner already exist !',
                        'userBanner' => $userBanner
                    ];
                }
            }
        }

        //Checking for unfilled rows in table
        foreach ($banners as $id => $banner) {
            if (empty($banner['name_city']) or (empty($banner['category']))
                or empty($banner['description']) or empty($banner['adress'])) {
                $wrongBannerIds[] = [
                    'id' => $id,
                    'type' => 'emptyRowErrors',
                    'reason' => 'There is some empty rows !',
                    'city' => $banner['name_city'],
                    'category' => $banner['category'],
                    'description' => $banner['description'],
                    'area' => $banner['area'],
                    'address' => $banner['adress'],
                    'image1' => $banner['image1'],
                    'image2' => $banner['image2'],
                    'image3' => $banner['image3'],
                ];
            }
            //Skip from checking the empty rows
            if ($banner['name_city'] == '' and $banner['category'] == ''
                and $banner['description'] == '' and $banner['adress'] == '') {
                continue;
            }

            //Checking if city exist in DB
            if (is_null($banner['city_id']) and $wrongBannerIds[$id]['type'] !== 'emptyRowErrors'
                and $wrongBannerIds[$id]['type'] !== 'cityErrors') {
                $wrongBannerIds[] = [
                    'id' => $id,
                    'type' => 'cityErrors',
                    'reason' => 'City does not exist !',
                    'city' => $banner['name_city'],
                    'category' => $banner['category'],
                    'description' => $banner['description'],
                    'area' => $banner['area'],
                    'address' => $banner['adress'],
                    'image1' => $banner['image1'],
                    'image2' => $banner['image2'],
                    'image3' => $banner['image3'],
                ];
            }
            //Checking if category exist in DB
            if (empty($banner['category_id']) && $wrongBannerIds[$id]['type'] = 'emptyRowErrors'
                && ($wrongBannerIds[$id]['type'] !== 'categoryErrors')) {

                $wrongBannerIds[] = [
                    'id' => $id,
                    'type' => 'categoryErrors',
                    'reason' => 'Category does not exist !',
                    'city' => $banner['name_city'],
                    'category' => $banner['category'],
                    'description' => $banner['description'],
                    'area' => $banner['area'],
                    'address' => $banner['adress'],
                    'image1' => $banner['image1'],
                    'image2' => $banner['image2'],
                    'image3' => $banner['image3'],
                ];
                continue;
            }
        }

        return $wrongBannerIds;
    }


    /**
     * @param BannerDomain $bannerDomain
     */
    public function setBannerDomain($bannerDomain)
    {
        $this->bannerDomain = $bannerDomain;
    }

    /**
     * @param CityDomain $cityDomain
     */
    public function setCityDomain($cityDomain)
    {
        $this->cityDomain = $cityDomain;
    }

    /**
     * @param BannerCategoryDomain $categoryDomain
     */
    public function setCategoryDomain($categoryDomain)
    {
        $this->categoryDomain = $categoryDomain;
    }

    /**
     * @param YMapAddresses $YMapAddresses
     */
    public function setYMapAddresses($YMapAddresses)
    {
        $this->YMapAddresses = $YMapAddresses;
    }

}


