<?php
namespace Admin\Service\Factory;


use Admin\Service\AddFromFile;
use Admin\Service\YMapAddresses;
use Application\Model\BannerCategoryDomain;
use Application\Model\BannerDomain;
use Application\Model\CityDomain;
use Application\Service\DomainModelManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class AddFromFileFactory
 * @package Admin\Service\Factory
 */
class AddFromFileFactory implements FactoryInterface
{


    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return AddFromFile|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        /** @var BannerDomain $bannerDomain */
        $bannerDomain = $domainManager->get(BannerDomain::class);


        /** @var CityDomain $cityDomain */
        $cityDomain = $domainManager->get(CityDomain::class);

        /** @var BannerCategoryDomain $categoryDomain */
        $categoryDomain = $domainManager->get(BannerCategoryDomain::class);

        /** @var YMapAddresses $YMapAddresses */
        $YMapAddresses = $domainManager->get(YMapAddresses::class);

        $addFromFileService = new AddFromFile();
        $addFromFileService->setBannerDomain($bannerDomain);
        $addFromFileService->setCityDomain($cityDomain);
        $addFromFileService->setCategoryDomain($categoryDomain);
        $addFromFileService->setYMapAddresses($YMapAddresses);

        return $addFromFileService;
    }
}

