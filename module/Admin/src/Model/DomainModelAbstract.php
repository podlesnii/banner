<?php

namespace Admin\Model;

use Admin\Entity\EntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Class DomainModelAbstract
 * @package Application\Model
 */
abstract class DomainModelAbstract
{
    protected $entityName = EntityAbstract::class;

    /**
     * Instance of EntityManager
     * @var  EntityManager
     */
    protected $entityManager;

    /**
     * Instance of EntityRepository
     * @var EntityRepository $repository
     */
    protected $repository;


    /**
     * DomainModelAbstractORM constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->entityManager = $em;
        $this->repository = $this->initRepository();
    }

    /**
     * @param $id
     *
     * @return bool|\Doctrine\Common\Proxy\Proxy|null|object
     * @throws \Doctrine\ORM\ORMException
     */
    public function getReference($id)
    {
        return $this->entityManager->getReference($this->entityName, $id);
    }

    /**
     * @param array $filter
     *
     * @return EntityAbstract[]
     */
    public function getEntities($filter)
    {
        $qb = $this->getRepository()->createQueryBuilder('e');

        foreach ($filter as $property => $value) {
            $qb->andWhere('e.' . $property . ' IN (:' . $property . ')')
                ->setParameter($property, $value);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $from
     * @param $to
     * @param array $filter
     *
     * @return array
     */
    public function getMapper($from, $to, $filter = [])
    {
        $qb = $this->getRepository()->createQueryBuilder('e');
        $qb->select('e.'.$from, 'e.'.$to);
        foreach ($filter as $property => $value) {
            $qb->andWhere('e.' . $property . ' IN (:' . $property . ')')
                ->setParameter($property, $value);
        }

        $result = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
        $mapper = [];

        foreach ($result as $value) {
            $mapper[$value[$from]] = $value[$to];
        }

        return $mapper;
    }

    /**
     * @param $data
     * @param $identifier
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function updateOrInsert($data, $identifier)
    {
        $ids = array_keys($data);

        $entities = $this->getEntities([
            $identifier => $ids,
        ]);

        $getterMethodName = 'get' . ucfirst($identifier);

        foreach ($entities as $entity) {
            if (!method_exists($entity, $getterMethodName)) {
                throw new \Exception('Incorrect identifier name or getter is not defined');
            }
            $id = $entity->$getterMethodName();
            if (isset($data[$id])) {
                $entity->fromArray($data[$id]);
                $this->persist($entity);
                unset($data[$id]);
            }
        }

        foreach ($data as $record) {
            /** @var EntityAbstract $entity */
            $entity = new $this->entityName();
            $entity->fromArray($record);
            $this->persist($entity);
        }

        $this->flush();
    }

    /**
     * Find by id
     *
     * @param $id
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function find($id)
    {

        $qb = $this->repository->createQueryBuilder('a');
        $qb->where('a.id = :id')
            ->setParameter('id', $id);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param $filter
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneBy($filter)
    {
        $qb = $this->repository->createQueryBuilder('a');

        foreach ($filter as $property => $value) {
            $qb->where('a.' . $property . ' = :' . $property)
                ->setParameter($property, $value);
        }


        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        $qb = $this->repository->createQueryBuilder('a');

        return $qb->getQuery()->getResult();
    }

    /**
     * Count entities
     *
     * @param array $filter
     *
     * @return integer
     */
    public function countEntities($filter = array())
    {
        $qb = $this->repository->createQueryBuilder('a');
        $qb->select('COUNT(a.id) as entityCount');

        $result = $qb->getQuery()->getResult();

        return $result[0]['entityCount'];
    }

    /**
     * Get repository
     * @return EntityRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Persist data
     *
     * @param $entity
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function persist($entity)
    {
        $this->entityManager->persist($entity);
    }

    /**
     * Flush data
     *
     * @param null $entity
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function flush($entity = null)
    {
        $this->entityManager->flush($entity);
    }

    /**
     * Initialise repository
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function initRepository()
    {
        return $this->entityManager->getRepository($this->entityName);
    }
}