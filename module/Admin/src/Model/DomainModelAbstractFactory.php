<?php

namespace Admin\Model;

use Admin\Service\DomainModelManager;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class DomainModelAbstractFactory
 * @package Admin\Model
 */
class DomainModelAbstractFactory implements FactoryInterface
{
    /**
     *
     * @param  ContainerInterface $container
     * @param $name
     * @param  null|array $options
     *
     * @return DomainModelManager
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $name, array $options = null)
    {
        /** @var EntityManager::class $container */
        $entityManager = $container->get(EntityManager::class);
        $model = new $name($entityManager);
        return $model;
    }
}
