<?php

namespace Admin\Form;

use Application\Service\Provider\BannerCategoryProvider;
use Application\Service\Provider\CityProvider;
use Application\Service\Provider\ImageProvider;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class BannerForm
 * @package Admin\Form
 */
class BannerForm extends Form
{

    /** @var BannerCategoryProvider */
    protected $categoryProvider;

    /** @var CityProvider */
    protected $cityProvider;

    /** @var ImageProvider  */
    protected $imageProvider;

    /**
     * Constructor
     *
     * @param BannerCategoryProvider $categoryProvider
     * @param CityProvider $cityProvider
     * @param ImageProvider $imageProvider
     */
    public function __construct(BannerCategoryProvider $categoryProvider, CityProvider $cityProvider,
    ImageProvider $imageProvider)
    {
        $this->categoryProvider = $categoryProvider;
        $this->cityProvider = $cityProvider;
        $this->imageProvider = $imageProvider;

        // Define form name
        parent::__construct('banner-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        // Add "bannerName" field
        $this->add([
            'type' => 'text',
            'name' => 'bannerName',
            'options' => [
                'label' => 'Banner Name',
            ],
        ]);

        // Add "category" field
        $categoryField = [
            'type' => 'select',
            'name' => 'category',
            'options' => [
                'label' => 'Category',
            ],
        ];
        $categoryField['options']['value_options'] = $this->categoryProvider->getFormValues();
        $categoryField['attributes']['value'] = $this->categoryProvider->getDefaultValue();

        $this->add($categoryField);

        // Add "city" field
        $cityField = [
            'type' => 'select',
            'name' => 'city',
            'options' => [
                'label' => 'City',
            ],
        ];
        $cityField['options']['value_options'] = $this->cityProvider->getFormValues();
        $cityField['attributes']['value'] = $this->cityProvider->getDefaultValue();
        $this->add($cityField);

        // Add "area" field
        $this->add([
            'type' => 'text',
            'name' => 'area',
            'options' => [
                'label' => 'Area',
            ],
        ]);

        // Add "address" field
        $this->add([
            'type' => 'text',
            'name' => 'adress',
            'options' => [
                'label' => 'Address',
            ],
        ]);

        // Add "description" field
        $this->add([
            'type' => 'textarea',
            'name' => 'description',
            'options' => [
                'label' => 'Description',
            ],
            'attributes' => [
                'rows' => '5'
            ]
        ]);

        // Add the submit button
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
            ],
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);


        $inputFilter->add([
            'name' => 'city',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);

        $inputFilter->add([
            'name' => 'category',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);

        $inputFilter->add([
            'name' => 'area',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);


        $inputFilter->add([
            'name' => 'adress',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);

        $inputFilter->add([
            'name' => 'bannerName',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);


        $inputFilter->add([
            'name' => 'description',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);
    }

}