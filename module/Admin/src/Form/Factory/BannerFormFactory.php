<?php

namespace Admin\Form\Factory;

use Admin\Form\BannerForm;
use Application\Service\Provider\BannerCategoryProvider;
use Application\Service\Provider\CityProvider;
use Application\Service\Provider\ImageProvider;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class BannerFormFactory
 * @package Application\Service\Factory
 */
class BannerFormFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return BannerForm
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $bannerCategoryProvider = $container->get(BannerCategoryProvider::class);
        $cityProvider = $container->get(CityProvider::class);
        $imageProvider = $container->get(ImageProvider::class);

        $form = new BannerForm($bannerCategoryProvider, $cityProvider, $imageProvider);

        return $form;
    }
}