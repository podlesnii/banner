<?php

    namespace Admin\Form\Factory;

    use Admin\Form\UserForm;
    use Application\Service\Provider\RoleProvider;
    use Interop\Container\ContainerInterface;
    use Zend\ServiceManager\Factory\FactoryInterface;


    /**
     * Class UserFormFactory
     * @package Application\Service\Factory
     */
    class UserFormFactory implements FactoryInterface
    {

        /**
         * @param ContainerInterface $container
         * @param $requestedName
         * @param array|null $options
         * @return UserForm
         * @throws \Psr\Container\ContainerExceptionInterface
         * @throws \Psr\Container\NotFoundExceptionInterface
         */
        public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
        {
            $roleProvider = $container->get(RoleProvider::class);

            $form = new UserForm($roleProvider);

            return $form;
        }
    }