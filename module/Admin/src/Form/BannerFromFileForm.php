<?php

namespace Admin\Form;

use Zend\Form\Form;
use Zend\InputFilter\FileInput;
use Zend\InputFilter\InputFilter;

/**
 * Class BannerFromFileForm
 * @package Admin\Form
 */
class BannerFromFileForm extends Form
{


    /**
     * BannerFromFileForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('bannerFromFile-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {

        // Add "file" field
        $this->add([
            'type' => 'file',
            'name' => 'file',
            'options' => [
                'label' => ' ',
            ],
        ]);

        // Add the submit button
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Upload Banners',
            ],
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        $inputFilter = new FileInput();


/*        $inputFilter->add([
            'type'     => 'Zend\InputFilter\FileInput',
            'name'     => 'file',
            'required' => true,
            'filters'  => [
            ],
            'validators' => [
            ]
        ]);*/


    }

}