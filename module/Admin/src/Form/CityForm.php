<?php

namespace Admin\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class CityForm
 * @package Admin\Form
 */
class CityForm extends Form
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('city-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {

        // Add "name_city" field
        $this->add([
            'type' => 'text',
            'name' => 'nameCity',
            'options' => [
                'label' => 'Name city',
            ],
        ]);

        // Add "name_region" field
        $this->add([
            'type' => 'text',
            'name' => 'nameRegion',
            'options' => [
                'label' => 'Name region',
            ],
        ]);

        // Add "indexname_city" field
        $this->add([
            'type' => 'text',
            'name' => 'indexnameCity',
            'options' => [
                'label' => 'Indexname city',
            ],
        ]);

        // Add "indexname_region" field
        $this->add([
            'type' => 'text',
            'name' => 'indexnameRegion',
            'options' => [
                'label' => 'Indexname region',
            ],
        ]);

        // Add the submit button
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
            ],
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'nameCity',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);

        $inputFilter->add([
            'name' => 'nameRegion',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);

        $inputFilter->add([
            'name' => 'indexnameCity',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);

        $inputFilter->add([
            'name' => 'indexnameRegion',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);

    }

}