<?php

    namespace Admin\Form;

    use Application\Service\Provider\RoleProvider;
    use Zend\Form\Form;
    use Zend\InputFilter\InputFilter;
    use Zend\Validator\Hostname;

    /**
     * Class UserForm
     * @package Admin\Form
     */
    class UserForm extends Form
    {

        /** @var RoleProvider */
        protected $roleProvider;

        /**
         * Constructor
         *
         * @param RoleProvider $roleProvider
         */
        public function __construct(RoleProvider $roleProvider)
        {
            $this->roleProvider = $roleProvider;

            // Define form name
            parent::__construct('user-form');

            // Set POST method for this form
            $this->setAttribute('method', 'post');

            $this->addElements();
            $this->addInputFilter();
        }

        /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        // Add "role" field
        $roleField = [
            'type' => 'select',
            'name' => 'role',
            'options' => [
                'label' => 'Role',
            ],
        ];
        $roleField['options']['value_options'] = $this->roleProvider->getFormValues();
        $roleField['attributes']['value'] = $this->roleProvider->getDefaultValue();

        $this->add($roleField);


        // Add "name" field
        $this->add([
            'type' => 'text',
            'name' => 'name',
            'options' => [
                'label' => 'Name',
            ],
        ]);

        // Add "login" field
        $this->add([
            'type' => 'text',
            'name' => 'login',
            'options' => [
                'label' => 'Login',
            ],
        ]);

        // Add "email" field
        $this->add([
            'type' => 'text',
            'name' => 'email',
            'options' => [
                'label' => 'Email',
            ],
        ]);

        // Add "password" field
        $this->add([
            'type' => 'text',
            'name' => 'password',
            'options' => [
                'label' => 'Password',
            ],
        ]);

        // Add the submit button
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
            ],
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'email',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'EmailAddress',
                    'options' => [
                        'allow' => Hostname::ALLOW_DNS,
                        'useMxCheck' => false,
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'role',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);

        $inputFilter->add([
            'name' => 'name',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);

        $inputFilter->add([
            'name' => 'login',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);

        $inputFilter->add([
            'name' => 'password',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);

    }

}