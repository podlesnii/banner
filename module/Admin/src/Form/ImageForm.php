<?php

namespace Admin\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\FileInput;

/**
 * The ImageForm form model is used for uploading an image file.
 */
class ImageForm extends Form
{
    protected $uploadPath;
    protected $imagePath;
    protected $publicPath;

    /**
     * Constructor.
     *
     * @param $uploadPathPart
     */
    public function __construct($uploadPathPart)
    {
        // Define form name
        parent::__construct('image-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');

        // Set binary content encoding
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->imagePath = implode(DIRECTORY_SEPARATOR, [
            'uploads',
            'images',
            $uploadPathPart,
        ]);
        $this->publicPath = implode(DIRECTORY_SEPARATOR, [
            getcwd(),
            'public',
        ]);
        $this->uploadPath = implode(DIRECTORY_SEPARATOR, [
            $this->publicPath,
            $this->imagePath,
        ]);

        if (!file_exists($this->uploadPath)) {
            mkdir($this->uploadPath, 0777, true);
        }

        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * @return string
     */
    public function getImagePath(): string
    {
        return $this->imagePath;
    }

    /**
     * @return string
     */
    public function getPublicPath(): string
    {
        return $this->publicPath;
    }


    protected function addElements()
    {
        $this->add([
            'type' => 'file',
            'name' => 'files',
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'type'       => FileInput::class,
            'name'       => 'files',
            'required'   => true,
            'validators' => [
                [
                    'name' => 'FileUploadFile',
                ],
                [
                    'name'    => 'FileMimeType',
                    'options' => [
                        'mimeType' => [
                            'image/jpeg', 'image/png',
                        ],
                    ],
                ],
                [
                    'name' => 'FileIsImage',
                ],
                [
                    'name'    => 'FileImageSize',
                    'options' => [
                        'minWidth'  => 128,
                        'minHeight' => 128,
                        'maxWidth'  => 4096,
                        'maxHeight' => 4096,
                    ],
                ],
            ],
            'filters'    => [
                [
                    'name'    => 'FileRenameUpload',
                    'options' => [
                        'target'             => $this->uploadPath,
                        'useUploadName'      => true,
                        'useUploadExtension' => true,
                        'overwrite'          => true,
                        'randomize'          => false,
                    ],
                ],
            ],
        ]);
    }
}