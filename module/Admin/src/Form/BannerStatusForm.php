<?php

namespace Admin\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class BannerStatusForm
 * @package Admin\Form
 */
class BannerStatusForm extends Form
{

    /**
     * BannerStatusForm constructor.
     */
    public function __construct()
    {

        // Define form name
        parent::__construct('bannerStatus-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {

        // Add "status" field
        $this->add([
            'type'    => 'select',
            'name'    => 'status',
            'options' => [
                'label'         => 'Status',
                'value_options' => [
                    'booked' => 'Booked',
                    'rented' => 'Rented',
                ],
            ],
        ]);

        // Add "timeStart" field
        $this->add([
            'type' => 'date',
            'name' => 'timeStart',
            'options' => [
                'label' => 'TimeStart',
                'format'=> 'Y-m-d',
            ],
        ]);

        // Add "timeEnd" field
        $this->add([
            'type' => 'date',
            'name' => 'timeEnd',
            'options' => [
                'label' => 'TimeEnd',
                'format'=> 'Y-m-d',
            ],
        ]);

        // Add the submit button
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
            ],
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);


        $inputFilter->add([
            'name' => 'status',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);
    }

}