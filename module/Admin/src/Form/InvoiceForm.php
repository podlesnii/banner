<?php
namespace Admin\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;


/**
 * Class InvoiceForm
 * @package Admin\Form
 */
class InvoiceForm extends Form
{
    /**
     * Constructor.     
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('invoice-form');
     
        // Set POST method for this form
        $this->setAttribute('method', 'post');
                
        $this->addElements();
        $this->addInputFilter();          
    }
    
    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() 
    {
        // Add "login" field
        $this->add([            
            'type'  => 'text',
            'name' => 'company',
            'options' => [
                'label' => 'Company',
            ],
        ]);

        // Add "login" field
        $this->add([
            'type'  => 'text',
            'name' => 'adress',
            'options' => [
                'label' => 'Adress',
            ],
        ]);

        // Add "login" field
        $this->add([
            'type'  => 'text',
            'name' => 'idno',
            'options' => [
                'label' => 'Idno',
            ],
        ]);

        // Add "login" field
        $this->add([
            'type'  => 'text',
            'name' => 'bank',
            'options' => [
                'label' => 'Bank',
            ],
        ]);

        // Add "login" field
        $this->add([
            'type'  => 'text',
            'name' => 'iban',
            'options' => [
                'label' => 'Iban',
            ],
        ]);

        // Add "login" field
        $this->add([
            'type'  => 'text',
            'name' => 'account',
            'options' => [
                'label' => 'Account',
            ],
        ]);

        // Add "login" field
        $this->add([
            'type'  => 'text',
            'name' => 'swift',
            'options' => [
                'label' => 'Swift',
            ],
        ]);

        // Add "login" field
        $this->add([
            'type'  => 'text',
            'name' => 'bic',
            'options' => [
                'label' => 'Bic',
            ],
        ]);


        // Add the submit button
        $this->add([
                'type' => 'submit',
                'name' => 'submit',
                'attributes' => [
                        'value' => 'Submit',
                ],
        ]);
    }
    
    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter() 
    {
        // Create main input filter
        $inputFilter = new InputFilter();        
        $this->setInputFilter($inputFilter);
                
        // Add input for "login" field
        $inputFilter->add([
                'name'     => 'company',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],                    
                ],                
            ]);

        // Add input for "login" field
        $inputFilter->add([
                'name'     => 'adress',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
            ]);
        
           // Add input for "login" field
        $inputFilter->add([
                'name'     => 'idno',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
            ]);

           // Add input for "login" field
        $inputFilter->add([
                'name'     => 'bank',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
            ]);

           // Add input for "login" field
        $inputFilter->add([
                'name'     => 'iban',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
            ]);

           // Add input for "login" field
        $inputFilter->add([
                'name'     => 'account',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
            ]);

           // Add input for "login" field
        $inputFilter->add([
                'name'     => 'swift',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
            ]);

           // Add input for "login" field
        $inputFilter->add([
                'name'     => 'bic',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
            ]);

    }
}

