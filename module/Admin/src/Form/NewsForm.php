<?php

namespace Admin\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class NewsForm
 * @package Admin\Form
 */
class NewsForm extends Form
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('news-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {

        // Add "title" field
        $this->add([
            'type'    => 'text',
            'name'    => 'title',
            'options' => [
                'label' => 'Title',
            ],
        ]);

        // Add "article" field
        $this->add([
            'type'    => 'textarea',
            'name'    => 'article',
            'options' => [
                'label' => 'Article',
            ],
        ]);

        // Add "image" field
        $this->add([
            'type'    => 'text',
            'name'    => 'image',
            'options' => [
                'label' => 'Image',
            ],
        ]);

        // Add "active" field
        $this->add([
            'type'    => 'select',
            'name'    => 'active',
            'options' => [
                'label' => 'Visible',
            'value_options'=>[
                '0' => 'No',
                '1' => 'Yes',
            ],
                ],
        ]);

        // Add the submit button
        $this->add([
            'type'       => 'submit',
            'name'       => 'submit',
            'attributes' => [
                'value' => 'Submit',
            ],
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);


        $inputFilter->add([
            'name'     => 'title',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'article',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'active',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
        ]);

    }

}