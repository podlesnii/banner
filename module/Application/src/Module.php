<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\Service\Language;
use Application\Service\TranslationManager;
use Zend\Http\PhpEnvironment\Request;
use Zend\I18n\Translator\Translator;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Session\SessionManager;

/**
 * Class Module
 * @package Application
 */
class Module
{
    const VERSION = '3.0.3-dev';

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * @param MvcEvent $e
     */
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager   = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $eventManager->attach(MvcEvent::EVENT_ROUTE, array($this, 'switchLocale'), 2);
        $eventManager->attach(MvcEvent::EVENT_ROUTE, array($this, 'initLocale'), 1);

        $eventManager->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($event) {
            /** @var MvcEvent $event */
            $controller      = $event->getTarget();
            $controllerName  = get_class($controller);
            $moduleNamespace = substr($controllerName, 0, strpos($controllerName, '\\'));
            $configs         = $event->getApplication()->getServiceManager()->get('config');
            if (isset($configs['view_manager']['module_layouts'][$moduleNamespace])) {
                $controller->layout($configs['view_manager']['module_layouts'][$moduleNamespace]);
            }
        }, 100);

        $application = $e->getApplication();
        $serviceManager = $application->getServiceManager();

        // The following line instantiates the SessionManager and automatically
        // makes the SessionManager the 'default' one.
        $serviceManager->get(SessionManager::class);
    }

    /**
     * Initialize locale
     * @param MvcEvent $e
     */
    public function switchLocale(MvcEvent $e)
    {
        $serviceManager = $e->getApplication()->getServiceManager();
        /** @var Request $request */
        $request = $e->getRequest();
        $locale = $request->getQuery('locale');
        if (!is_null($locale)) {
            /** @var Language $languageService */
            $languageService = $serviceManager->get(Language::class);
            if ($languageService->localeExists($locale)) {
                $languageService->setCurrentLocale($locale);
            }
        }
    }

    /**
     * Initialize locale
     * @param MvcEvent $e
     */
    public function initLocale(MvcEvent $e)
    {
        $serviceManager = $e->getApplication()->getServiceManager();
        $translator = $serviceManager->get('MvcTranslator');
        /** @var Language $languageService */
        $languageService = $serviceManager->get(Language::class);

        $translator->getEventManager()->attach(Translator::EVENT_MISSING_TRANSLATION, function($event) use ($serviceManager) {
            /** @var TranslationManager $translationManager */
            $translationManager = $serviceManager->get(TranslationManager::class);
            $translationManager->missingTranslation($event->getParams());
        });

        $translator->setLocale($languageService->getCurrentLocale());
    }
}
