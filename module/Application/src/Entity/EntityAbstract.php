<?php
/**
 * User: zfmaster
 * Date: 1/3/18
 * Time: 9:35 PM
 */

namespace Application\Entity;


/**
 * Class EntityAbstract
 * @package Application\Entity
 */
class EntityAbstract
{

    /**
     * Make array from object
     * @return array
     */
    public function getArrayCopy()
    {
        $data = get_object_vars($this);
        return $data;
    }

    /**
     * Make object from array
     * @param $array
     */
    public function fromArray($array)
    {
        foreach ($array as $property=>$value) {
            if (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }
    }

}