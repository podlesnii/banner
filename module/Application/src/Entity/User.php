<?php

namespace Application\Entity;

use Acl\Entity\Role;
use Application\Entity\EntityAbstract;
use Doctrine\Common\Proxy\Proxy;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends EntityAbstract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="index_name", type="string", length=255, nullable=true)
     */
    protected $indexName;

    /**
     * @var \Application\Entity\Invoice
     * @ORM\OneToOne(targetEntity="\Application\Entity\Invoice")
     * @ORM\JoinColumn(name="owner_account", referencedColumnName="id")
     */
    protected $account;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=64, nullable=false)
     */
    protected $login;

    /**
     * @ORM\Column(name="email", type="string", length=64, nullable=false)
     */
    protected $email;

    /**
     * @var \Acl\Entity\Role
     *
     * @ORM\ManyToOne(targetEntity="\Acl\Entity\Role", inversedBy="role")
     * @ORM\JoinColumn(name="role", referencedColumnName="id")
     */
    protected $role;

    /**
     * @ORM\Column(name="password", type="string", length=64, nullable=false)
     */
    protected $password;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=false)
     */
    protected $icon;

    /**
     * @ORM\Column(name="language", type="string", length=32, nullable=true)
     */
    protected $language;

    /**
     * @ORM\Column(name="active", type="boolean", length=32, nullable=false)
     */
    protected $active;

    /**
     * @var Banner[]
     *
     * @ORM\OneToMany(targetEntity="\Application\Entity\Banner", mappedBy="banners")
     * @ORM\JoinColumn(name="id", referencedColumnName="userId")
     */
    protected $banners;


    /**
     * @return int
     */
    public function getId ()
    {
        return $this->id;
    }


    /**
     * @param $id
     */
    public function setId ( $id )
    {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function getName ()
    {
        return $this->name;
    }


    /**
     * @param $name
     */
    public function setName ( $name )
    {
        $this->name = $name;
    }





    /**
     * @return mixed
     */
    public function getEmail ()
    {
        return $this->email;
    }


    /**
     * @param $email
     */
    public function setEmail ( $email )
    {
        $this->email = $email;
    }


    /**
     * @return \Acl\Entity\Role
     */
    public function getRole ()
    {
        return $this->role;
    }


    /**
     * @param $role
     */
    public function setRole ( $role )
    {
        $this->role = $role;
    }


    /**
     * Add a role to the user.
     *
     * @param Role|Proxy $role
     * @return $this
     */
    public function addRole($role)
    {
        $this->role = $role;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getPassword ()
    {
        return $this->password;
    }


    /**
     * @param $password
     */
    public function setPassword ( $password )
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getLanguage ()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage ($language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return Invoice
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Invoice $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getIndexName()
    {
        return $this->indexName;
    }

    /**
     * @param string $indexName
     */
    public function setIndexName($indexName)
    {
        $this->indexName = $indexName;
    }


}