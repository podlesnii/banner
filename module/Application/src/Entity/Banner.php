<?php

namespace Application\Entity;

use Doctrine\Common\Proxy\Proxy;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="banners")
 */
class Banner extends EntityAbstract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(name="bannerNumber", type="string", length=128, nullable=false)
     */
    protected $bannerNumber;

    /**
     * @var \Application\Entity\User
     * @ORM\ManyToOne(targetEntity="\Application\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $userId;

    /**
     * @ORM\Column(name="description", type="string", length=2560, nullable=true)
     */
    protected $description;

    /**
     * @var \Application\Entity\City
     * @ORM\ManyToOne(targetEntity="\Application\Entity\City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    protected $city;

    /**
     * @ORM\Column(name="area", type="string", length=128, nullable=false)
     */
    protected $area;

    /**
     * @ORM\Column(name="adress", type="string", length=256, nullable=false)
     */
    protected $adress;

    /**
     * @var \Application\Entity\BannerCategory
     * @ORM\ManyToOne(targetEntity="\Application\Entity\BannerCategory")
     * @ORM\JoinColumn(name="category", referencedColumnName="id")
     */
    protected $category;

    /**
     * @var Image[]
     *
     * @ORM\OneToMany(targetEntity="\Application\Entity\Image", mappedBy="banner")
     */
    protected $images;

    /**
     * @var \Application\Entity\BannerStatus
     * @ORM\OneToMany(targetEntity="\Application\Entity\BannerStatus", mappedBy="banner")
     */
    protected $bannerStatuses;

    /**
     * @var \Application\Entity\BannerRatings
     * @ORM\OneToMany(targetEntity="\Application\Entity\BannerRatings", mappedBy="banner")
     */
    protected $bannerRatings;

    /**
     * @ORM\Column (name="latitude", type="string", length=16)
     */
    protected $latitude;

    /**
     * @ORM\Column (name="longitude", type="string", length=16)
     */
    protected $longitude;

    /**
     * @ORM\Column (name="price", type="integer", nullable=true)
     */
    protected $price;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return BannerCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * Add a category to the banner.
     *
     * @param BannerCategory|Proxy $category
     * @return $this
     */
    public function addCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Add a category to the banner.
     *
     * @param BannerCategory|Proxy $images
     * @return $this
     */
    public function addImage($images)
    {
        $this->images = $images;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Add a city to the banner.
     *
     * @param City|Proxy $city
     * @return $this
     */
    public function addCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param $area
     */
    public function setArea($area)
    {
        $this->area = $area;
    }

    /**
     * @return mixed
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * @param $adress
     */
    public function setAdress($adress)
    {
        $this->adress = $adress;
    }

    /**
     * @return mixed
     */
    public function getBannerStatuses()
    {
        return $this->bannerStatuses;
    }

    /**
     * @param BannerStatus[] $bannerStatuses
     *
     * @return Banner
     */
    public function setBannerStatuses($bannerStatuses)
    {
        $this->bannerStatuses = $bannerStatuses;

        return $this;
    }

    /**
     *
     * @param BannerStatus bannerStatus
     *
     * @return $this
     */
    public function addBannerStatuses($bannerStatus)
    {
        $this->bannerStatuses[] = $bannerStatus;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBannerRatings()
    {
        return $this->bannerRatings;
    }

    /**
     * @param BannerRatings[] $bannerRatings
     *
     * @return Banner
     */
    public function setBannerRatings($bannerRatings)
    {
        $this->bannerStatuses = $bannerRatings;

        return $this;
    }

    /**
     *
     * @param BannerRatings bannerRatings
     *
     * @return $this
     */
    public function addBannerRatings($bannerRatings)
    {
        $this->bannerRatings[] = $bannerRatings;

        return $this;
    }

    /**
     * @return User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param User $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Add a userId to the banner.
     *
     * @param User|Proxy $userId
     * @return $this
     */
    public function addUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getBannerNumber()
    {
        return $this->bannerNumber;
    }

    /**
     * @param mixed $bannerNumber
     */
    public function setBannerNumber($bannerNumber)
    {
        $this->bannerNumber = $bannerNumber;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

}

