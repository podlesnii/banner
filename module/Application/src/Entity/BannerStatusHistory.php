<?php

namespace Application\Entity;

use Application\Entity\EntityAbstract;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Proxy\Proxy;


/**
 * @ORM\Entity
 * @ORM\Table(name="banners_status_history")
 */
class BannerStatusHistory extends EntityAbstract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(name="status", type="string", length=256, nullable=false)
     */
    protected $status;

    /**
     * @ORM\Column(name="timeChanged", type="datetime", length=256, nullable=false)
     */
    protected $timeChanged;

    /**
     * @ORM\Column(name="timeStart", type="datetime", length=256, nullable=false)
     */
    protected $timeStart;

    /**
     * @ORM\Column(name="timeEnd", type="datetime", length=256, nullable=false)
     */
    protected $timeEnd;

    /**
     * @var \Application\Entity\Banner
     * @ORM\ManyToOne(targetEntity="\Application\Entity\Banner")
     * @ORM\JoinColumn(name="banner_id", referencedColumnName="id")
     */
    protected $banner;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getTimeChanged()
    {
        return $this->timeChanged;
    }

    /**
     * @param mixed $timeChanged
     */
    public function setTimeChanged($timeChanged)
    {
        $this->timeChanged = $timeChanged;
    }

    /**
     * @return mixed
     */
    public function getTimeStart()
    {
        return $this->timeStart;
    }

    /**
     * @param mixed $timeStart
     */
    public function setTimeStart($timeStart)
    {
        $this->timeStart = $timeStart;
    }

    /**
     * @return mixed
     */
    public function getTimeEnd()
    {
        return $this->timeEnd;
    }

    /**
     * @param mixed $timeEnd
     */
    public function setTimeEnd($timeEnd)
    {
        $this->timeEnd = $timeEnd;
    }

    /**
     * @return Banner
     */
    public function getBanner()
    {
        return $this->banner;
    }

    /**
     * @param Banner $banner
     */
    public function setBanner($banner)
    {
        $this->banner = $banner;
    }

    /**
     * Add a banner to the bannerStatus.
     *
     * @param Banner|Proxy $banner
     * @return $this
     */
    public function addBanner($banner)
    {
        $this->banner = $banner;
        return $this;
    }


}

