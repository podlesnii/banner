<?php

namespace Application\Entity;

use Application\Entity\EntityAbstract;
use Doctrine\Common\Proxy\Proxy;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="image")
 */
class Image extends EntityAbstract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(name="path", type="string", length=256, nullable=false)
     */
    protected $path;

    /**
     * @ORM\Column(name="title", type="string", length=256, nullable=true)
     */
    protected $title;

    /**
     * @var \Application\Entity\Banner
     * @ORM\ManyToOne(targetEntity="\Application\Entity\Banner")
     * @ORM\JoinColumn(name="banner_id", referencedColumnName="id")
     */
    protected $banner;


    /**
     * @return int
     */
    public function getId ()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPath ()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath ($path)
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getTitle ()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle ($title)
    {
        $this->title = $title;
    }

    /**
     * @return Banner
     */
    public function getBanner ()
    {
        return $this->banner;
    }

    /**
     * @param Banner $banner
     */
    public function setBanner ($banner)
    {
        $this->banner = $banner;
    }

    /**
     * Add a $banner to the image.
     *
     * @param Banner|Proxy $banner
     * @return $this
     */
    public function addBanner($banner)
    {
        $this->banner = $banner;
        return $this;
    }


}

