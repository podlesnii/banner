<?php

namespace Application\Entity;

use Application\Entity\EntityAbstract;
use Doctrine\Common\Proxy\Proxy;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="invoice")
 */
class Invoice extends EntityAbstract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \Application\Entity\User
     * @ORM\OneToOne(targetEntity="\Application\Entity\User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    protected $owner;

    /**
     * @ORM\Column(name="company", type="string", length=256, nullable=false)
     */
    protected $company;

    /**
     * @ORM\Column(name="adress", type="string", length=256, nullable=false)
     */
    protected $adress;

    /**
     * @ORM\Column(name="idno", type="string", length=256, nullable=false)
     */
    protected $idno;

    /**
     * @ORM\Column(name="bank", type="string", length=256, nullable=false)
     */
    protected $bank;

    /**
     * @ORM\Column(name="iban", type="string", length=256, nullable=false)
     */
    protected $iban;

    /**
     * @ORM\Column(name="account", type="string", length=256, nullable=false)
     */
    protected $account;

    /**
     * @ORM\Column(name="swift", type="string", length=256, nullable=false)
     */
    protected $swift;

    /**
     * @ORM\Column(name="bic", type="string", length=256, nullable=false)
     */
    protected $bic;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * Add a role to the user.
     *
     * @param User|Proxy $owner
     * @return $this
     */
    public function addOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * @param mixed $adress
     */
    public function setAdress($adress)
    {
        $this->adress = $adress;
    }

    /**
     * @return mixed
     */
    public function getIdno()
    {
        return $this->idno;
    }

    /**
     * @param mixed $idno
     */
    public function setIdno($idno)
    {
        $this->idno = $idno;
    }

    /**
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @param mixed $bank
     */
    public function setBank($bank)
    {
        $this->bank = $bank;
    }

    /**
     * @return mixed
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * @param mixed $iban
     */
    public function setIban($iban)
    {
        $this->iban = $iban;
    }

    /**
     * @return mixed
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param mixed $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return mixed
     */
    public function getSwift()
    {
        return $this->swift;
    }

    /**
     * @param mixed $swift
     */
    public function setSwift($swift)
    {
        $this->swift = $swift;
    }

    /**
     * @return mixed
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * @param mixed $bic
     */
    public function setBic($bic)
    {
        $this->bic = $bic;
    }

}

