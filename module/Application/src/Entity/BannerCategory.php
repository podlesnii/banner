<?php
namespace Application\Entity;

use Application\Entity\EntityAbstract;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="banner_categories")
 */
class BannerCategory extends EntityAbstract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="indexname", type="string", length=64, nullable=false)
     */
    protected $indexname;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=2560, nullable=false)
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=256)
     */
    protected $image;

    /**
     * @var Banner[]
     *
     * @ORM\OneToMany(targetEntity="\Application\Entity\Banner", mappedBy="banners")
     * @ORM\JoinColumn(name="id", referencedColumnName="category")
     */
    protected $banners;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @param $id
     */
    public function setId( $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getIndexname()
    {
        return $this->indexname;
    }

    /**
     * @param string $indexname
     */
    public function setIndexname(string $indexname)
    {
        $this->indexname = $indexname;
    }


    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $description
     */
    public function setDescription( $description)
    {
        $this->description = $description;
    }


    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }


    /**
     * @param $image
     */
    public function setImage( $image)
    {
        $this->image = $image;
    }

    /**
     * @return Banner[]
     */
    public function getBanner()
    {
        return $this->banners;
    }

    /**
     * @param Banner[] $banners
     */
    public function setBanner(array $banners)
    {
        $this->banners = $banners;
    }

}

