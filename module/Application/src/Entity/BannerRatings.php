<?php

namespace Application\Entity;

use Application\Entity\EntityAbstract;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Proxy\Proxy;


/**
 * @ORM\Entity
 * @ORM\Table(name="banner_ratings")
 */
class BannerRatings extends EntityAbstract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(name="rating", type="integer", nullable=false)
     */
    protected $rating;

    /**
     * @ORM\Column(name="create_date", type="datetime", length=256, nullable=true)
     */
    protected $createDate;

    /**
     * @var \Application\Entity\Banner
     * @ORM\ManyToOne(targetEntity="\Application\Entity\Banner")
     * @ORM\JoinColumn(name="banner_id", referencedColumnName="id")
     */
    protected $banner;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return Banner
     */
    public function getBanner()
    {
        return $this->banner;
    }

    /**
     * @param Banner $banner
     */
    public function setBanner($banner)
    {
        $this->banner = $banner;
    }

    /**
     * Add a banner to the bannerStatus.
     *
     * @param Banner|Proxy $banner
     * @return $this
     */
    public function addBanner($banner)
    {
        $this->banner = $banner;
        return $this;
    }

}

