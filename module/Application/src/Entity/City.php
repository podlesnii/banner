<?php

namespace Application\Entity;

use Application\Entity\EntityAbstract;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="city")
 */
class City extends EntityAbstract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="indexname_city", type="string", length=255, nullable=false)
     */
    protected $indexnameCity;

    /**
     * @var string
     * @ORM\OneToMany(targetEntity="\Application\Entity\Banner", mappedBy="banner")
     * @ORM\Column(name="name_city", type="string", length=255, nullable=false)
     */
    protected $nameCity;

    /**
     * @var string
     *
     * @ORM\Column(name="indexname_region", type="string", length=255, nullable=false)
     */
    protected $indexnameRegion;

    /**
     * @var string
     *
     * @ORM\Column(name="name_region", type="string", length=255, nullable=false)
     */
    protected $nameRegion;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getIndexnameCity()
    {
        return $this->indexnameCity;
    }

    /**
     * @param string $indexnameCity
     */
    public function setIndexnameCity($indexnameCity)
    {
        $this->indexnameCity = $indexnameCity;
    }

    /**
     * @return string
     */
    public function getNameCity()
    {
        return $this->nameCity;
    }

    /**
     * @param string $nameCity
     */
    public function setNameCity($nameCity)
    {
        $this->nameCity = $nameCity;
    }

    /**
     * @return string
     */
    public function getIndexnameRegion()
    {
        return $this->indexnameRegion;
    }

    /**
     * @param string $indexnameRegion
     */
    public function setIndexnameRegion($indexnameRegion)
    {
        $this->indexnameRegion = $indexnameRegion;
    }

    /**
     * @return string
     */
    public function getNameRegion()
    {
        return $this->nameRegion;
    }

    /**
     * @param string $nameRegion
     */
    public function setNameRegion($nameRegion)
    {
        $this->nameRegion = $nameRegion;
    }


}