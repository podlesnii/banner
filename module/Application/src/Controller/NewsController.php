<?php


namespace Application\Controller;

use Application\Model\NewsDomain;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;



/**
 * Class NewsController
 * @package Application\Controller
 */
class NewsController extends AbstractActionController
{

    /** @var NewsDomain $newsDomain */
    protected $newsDomain;


    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        $filter = null;
        $newsDQL = $this->newsDomain->getNews();
        $news = $newsDQL->execute();


        return new ViewModel([
            'news' => $news,
        ]);
    }

    /**
     * @return ViewModel
     */
    public function articleAction()
    {
        $id = $this->params()->fromQuery('id');

        $newsDQL = $this->newsDomain->getNews(['id' => $id]);
        $news = $newsDQL->execute();

        //Redirect to news page if article was deleted
        if($news == null){
            $this->redirect()->toRoute('news', ['action' => 'index']);
        }

        return new ViewModel([
            'news' => $news,
        ]);

    }



    /**
     * @param NewsDomain $newsDomain
     */
    public function setNewsDomain(NewsDomain $newsDomain)
    {
        $this->newsDomain = $newsDomain;
    }


}