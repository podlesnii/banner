<?php


namespace Application\Controller;

use Application\Service\Auth;
use Application\Service\Notifications;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Model\BannerDomain;
use Zend\Authentication\Result;

/**
 * Class DescriptionController
 * @package Application\Controller
 */
class DescriptionController extends AbstractActionController
{

    /** @var BannerDomain */
    protected $bannerDomain;

    /** @var Auth */
    protected $auth;

    /** @var Notifications */
    protected $notificationsService;


    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        $banners = $this->bannerDomain->getBanners(['id' => $this->params()->fromQuery('id')]);

        $banner = $banners->execute();
        $banner = $banner['0'];
        return new ViewModel([
            'banner' => $banner,
        ]);

    }


    /**
     * @return ViewModel
     * @throws \Exception
     */
    public function modalLoginAction()
    {
        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {

            $data = $this->params()->fromPost();

            // Perform login attempt.
            $result = $this->auth->login($data['login'], $data['password']);

            // Check result.
            if ($result->getCode() == Result::SUCCESS) {
                // Get redirect URL.
                $this->redirect()->toRoute('order', ['controller' => 'order',
                    'action' => 'ordered'], ['query' => ['id' => $data['id']]]);
            } else {
                $parameters = ['type' => 'warning', 'message' => 'Login or E-mail error!'];
                $this->notificationsService->addNotificationMessage($parameters);

                $banners = $this->bannerDomain->getBanners(['id' => $data['id']]);

                $banner = $banners->execute();
                $banner = $banner['0'];
                return new ViewModel([
                    'banner' => $banner,
                ]);
            }
        }
        return new ViewModel();
    }


    /**
     * @param BannerDomain $bannerDomain
     */
    public function setBannerDomain(BannerDomain $bannerDomain)
    {
        $this->bannerDomain = $bannerDomain;
    }

    /**
     * @param Auth $auth
     */
    public function setAuth($auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param Notifications $notificationsService
     */
    public function setNotificationsService($notificationsService)
    {
        $this->notificationsService = $notificationsService;
    }


}
