<?php

namespace Application\Controller;

use Application\Form\ForgotPasswordForm;
use Application\Form\LoginForm;
use Application\Form\RegisterAgencyForm;
use Application\Form\RegisterCustomerForm;
use Application\Form\RegisterOwnerForm;
use Application\Form\ResetPasswordForm;
use Application\Model\UserDomain;
use Application\Service\Auth;
use Application\Service\Notifications;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Authentication\Result;
use Zend\View\Model\ViewModel;
use Zend\Uri\Uri;
use ZendService\ReCaptcha\ReCaptcha;

/**
 * Class UsersController
 *
 * @package Application\Controller
 */
class AuthController extends AbstractActionController
{

    /** @var Auth */
    protected $auth;

    /** @var UserDomain */
    protected $userDomain;

    /** @var Auth $email */
    protected $email;

    /** @var Auth $token */
    protected $token;

    /** @var [] */
    protected $config;

    /** @var Notifications */
    protected $notificationsService;

    /**
     * AuthController constructor.
     *
     * @param Auth $auth
     * @param UserDomain $userDomain
     */
    public function __construct(Auth $auth, UserDomain $userDomain, $config)
    {
        $this->auth = $auth;
        $this->userDomain = $userDomain;
        $this->config = $config;
    }


    /**
     * @return ViewModel
     */
    public function registerCustomerAction()
    {
        // Google Captcha
        $pubKey = $this->config['googleCaptchaConfig']['keys']['publicKey'];
        $privKey = $this->config['googleCaptchaConfig']['keys']['privateKey'];
        $reCaptcha = new ReCaptcha($pubKey, $privKey);
        $form = new RegisterCustomerForm();
        $role = 'Customer';

        return new ViewModel([
            'form' => $form,
            'reCaptcha' => $reCaptcha,
            'role' => $role
        ]);

    }

    /**
     * @return ViewModel
     */
    public function registerOwnerAction()
    {
        // Google Captcha
        $pubKey = $this->config['googleCaptchaConfig']['keys']['publicKey'];
        $privKey = $this->config['googleCaptchaConfig']['keys']['privateKey'];
        $reCaptcha = new ReCaptcha($pubKey, $privKey);
        $form = new RegisterOwnerForm();
        $role = 'Owner';

        return new ViewModel([
            'form' => $form,
            'reCaptcha' => $reCaptcha,
            'role' => $role
        ]);

    }

    /**
     * @return ViewModel
     */
    public function registerAgencyAction()
    {

        // Google Captcha
        $pubKey = $this->config['googleCaptchaConfig']['keys']['publicKey'];
        $privKey = $this->config['googleCaptchaConfig']['keys']['privateKey'];
        $reCaptcha = new ReCaptcha($pubKey, $privKey);
        $role = 'Agency';

        $form = new RegisterAgencyForm();

        return new ViewModel([
            'form' => $form,
            'reCaptcha' => $reCaptcha,
            'role' => $role
        ]);

    }


    /**
     * @return ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function registerAction()
    {
        // Google Captcha
        $pubKey = $this->config['googleCaptchaConfig']['keys']['publicKey'];
        $privKey = $this->config['googleCaptchaConfig']['keys']['privateKey'];
        $reCaptcha = new ReCaptcha($pubKey, $privKey);

        //If user is authenticated -> redirect to home page.
        if (!is_null($this->auth->getCurrentUser())) {
            $this->redirect()->toRoute('home', ['action' => 'index']);
        }

        if ($this->getRequest()->isPost()) {

            $data = $this->params()->fromPost(); // Fill in the form with POST data

            if ($data['role'] == 'Customer') {
                $form = new RegisterCustomerForm();
                $redirectPage = "register-customer";
                $role = 'Customer';
            }
            if ($data['role'] == 'Agency') {
                $form = new RegisterAgencyForm();
                $redirectPage = "register-agency";
                $role = 'Agency';
            }
            if ($data['role'] == 'Owner') {
                $form = new RegisterOwnerForm();
                $redirectPage = "register-owner";
                $role = 'Owner';
            }

            if ($data['g-recaptcha-response'] !== "") {

                $form->setData($data);

                if ($form->isValid()) {  // Validate form
                    $data = $form->getData(); // Get filtered and validated data

                    //Check if the e-mail already exist
                    $isExistEmail = $this->userDomain->findOneBy(['email' => $data['email']]);
                    if (isset($isExistEmail)) {
                        $parameters = ['type' => 'warning', 'message' => 'E-mail error!'];
                        $this->notificationsService->addNotificationMessage($parameters);
                        $isEmailError = "User with that email address already exist. Please indicate another one.";

                        return new ViewModel(
                            [
                                'reCaptcha' => $reCaptcha,
                                'form' => $form,
                                'isEmailError' => $isEmailError,
                                'role' => $role
                            ]
                        );
                    }

                    //Check if the login already exist
                    $isExistLogin = $this->userDomain->findOneBy(['login' => $data['login']]);
                    if (isset($isExistLogin)) {
                        $parameters = ['type' => 'warning', 'message' => 'Login error!'];
                        $this->notificationsService->addNotificationMessage($parameters);
                        $isExistLogin = "User with that login already exist. Please indicate another one.";
                        return new ViewModel(
                            [
                                'reCaptcha' => $reCaptcha,
                                'form' => $form,
                                'isExistLogin' => $isExistLogin,
                                'role' => $role
                            ]
                        );
                    }
                    $this->auth->addUser($data);
                    $this->redirect()->toRoute('register/success', ['action' => 'success']);
                }

            } else {
                $isRecaptchaError = 'Captcha Error!';
                $parameters = ['type' => 'warning', 'message' => 'Captcha Error!'];
                $this->notificationsService->addNotificationMessage($parameters);

                return new ViewModel(
                    [
                        'form' => $form,
                        'reCaptcha' => $reCaptcha,
                        'isRecaptchaError' => $isRecaptchaError,
                        'role' => $role

                    ]
                );
            }
        }
        $role = 'Customer';

        $this->layout()->setVariable('bodyClass', 'login-page');
        $this->layout()->setVariable('wrapperClass', 'page-header');
        $this->layout()->setVariable('filterColor', 'orange');
        return new ViewModel(
            [
                'form' => $form,
                'reCaptcha' => $reCaptcha,
                'role' => $role
            ]
        );
    }


    /**
     * @return ViewModel
     * @throws \Exception
     */
    public function loginAction()
    {

        // Google ReCaptcha
        $pubKey = $this->config['googleCaptchaConfig']['keys']['publicKey'];
        $privKey = $this->config['googleCaptchaConfig']['keys']['privateKey'];
        $reCaptcha = new ReCaptcha($pubKey, $privKey);

        // Retrieve the redirect URL (if passed). We will redirect the user to this
        // URL after successfull login.
        $redirectUrl = (string)$this->params()->fromQuery(
            'redirectUrl', 'home'
        );
        if (strlen($redirectUrl) > 2048) {
            throw new \Exception("Too long redirectUrl argument passed");
        }

        //If user is authenticated yet -> redirect to home page.
        if (!is_null($this->auth->getCurrentUser())) {
            $this->redirect()->toRoute('home', ['action' => 'index']);
        }

        // Create login form
        $form = new LoginForm();
        $form->get('redirect_url')->setValue($redirectUrl);


        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {

            $data = $this->params()->fromPost();
            $form->setData($data);

            if ($data['g-recaptcha-response'] !== "") {

                // Validate form
                if ($form->isValid()) {

                    // Get filtered and validated data
                    $data = $form->getData();

                    // Perform login attempt.
                    $result = $this->auth->login($data['login'], $data['password']);

                    // Check result.
                    if ($result->getCode() == Result::SUCCESS) {

                        // Get redirect URL.
                        $this->redirect()->toRoute('home', ['action' => 'index']);
                        $parameters = ['type' => 'success', 'message' => 'Your have been successfully logged in!'];
                        $this->notificationsService->addNotificationMessage($parameters);
                        if (!empty($redirectUrl)) {
                            // The below check is to prevent possible redirect attack
                            // (if someone tries to redirect user to another domain).
                            $uri = new Uri($redirectUrl);
                            if (!$uri->isValid() || $uri->getHost() != null) {
                                throw new \Exception('Incorrect redirect URL: ' . $redirectUrl);
                            }
                        }

                        // If redirect URL is provided, redirect the user to that URL;
                        // otherwise redirect to Home page.
                        if (empty($redirectUrl)) {
                            return $this->redirect()->toRoute('home');

                        } else {
                            $this->redirect()->toRoute('login', ['action' => 'login']);
                        }

                    }

                    if ($result->getCode() == Result::FAILURE_IDENTITY_NOT_FOUND or
                        $result->getCode() == Result::FAILURE_CREDENTIAL_INVALID
                    ) {
                        $isLoginError = true;
                        $parameters = ['type' => 'warning', 'message' => 'Incorrect Login or Password!'];
                        $this->notificationsService->addNotificationMessage($parameters);
                    }

                    if ($result->getCode() == Result::FAILURE_UNCATEGORIZED) {
                        $isLoginErrorActive = true;
                        $parameters = ['type' => 'success', 'message' == 'Please, confirm your e-mail!'];
                        $this->notificationsService->addNotificationMessage($parameters);
                    }
                } else {
                    $isLoginError = true;
                }
            } else {
                $isRecaptchaError = true;
                $parameters = ['type' => 'warning', 'message' => 'Captcha Error!'];
                $this->notificationsService->addNotificationMessage($parameters);
            }

        }

        $this->layout()->setVariable('bodyClass', 'login-page');
        $this->layout()->setVariable('wrapperClass', 'page-header');
        $this->layout()->setVariable('filterColor', 'orange');

        return new ViewModel(
            [
                'form' => $form,
                'redirectUrl' => $redirectUrl,
                'isLoginError' => $isLoginError,
                'isLoginErrorActive' => $isLoginErrorActive,
                'reCaptcha' => $reCaptcha,
                'isRecaptchaError' => $isRecaptchaError,
            ]
        );
    }


    /**
     * @return ViewModel
     */
    public function successAction()
    {

        $parameters = ['type' => 'success', 'message' => 'Successfully!'];
        $this->notificationsService->addNotificationMessage($parameters);

        return new ViewModel();
    }


    /**
     * @return \Zend\Http\Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function confirmationAction()
    {
        $userEmail = $this->params()->fromQuery('email');
        $user = $this->userDomain->findOneBy(['email' => $userEmail]);

        $userToken = $this->auth->createUserToken($user);


        $tokenFromGet = $this->params()->fromQuery('token');


        if (isset($tokenFromGet)) {
            if ($tokenFromGet == $userToken) {

                $this->auth->emailConfirmed($user);


                // If redirect URL is provided, redirect the user to that URL;
                // otherwise redirect to Home page.
                if (empty($redirectUrl)) {
                    return $this->redirect()->toRoute('home');
                } else {
                    return $this->redirect()->toRoute(
                        'login', ['action' => 'login']
                    );
                }
            }
        } else {
            die ("Token doesn't match");
        }
        return $this->redirect()->toRoute('login', ['action' => 'login']);
    }


    /**
     * @return ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function forgotPasswordAction()
    {
        //If user is authenticated -> redirect to home page.
        if (!is_null($this->auth->getCurrentUser())) {
            $this->redirect()->toRoute('home', ['action' => 'index']);
        }

        //TO DO: place for notification


        // Create reset password form
        $form = new ForgotPasswordForm();

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {

            $data = $this->params()->fromPost();
            $form->setData($data);

            // Validate form
            if ($form->isValid()) {

                // Get filtered and validated data
                $data = $form->getData();

                if (!is_null($user = $this->userDomain->findOneBy(['email' => $data['login_or_email']])) or
                    !is_null($user = $this->userDomain->findOneBy(['login' => $data['login_or_email']]))
                ) {
                    $this->auth->resetPassword($user);

                    $parameters = ['type' => 'success', 'message' => 'Your password has been reset successfully!'];
                    $this->notificationsService->addNotificationMessage($parameters);

                    $this->redirect()->toRoute('login', ['action' => 'login']);

                } else {
                    $parameters = ['type' => 'warning', 'message' => 'Wrong data!'];
                    $this->notificationsService->addNotificationMessage($parameters);
                }
            }
        }

        $this->layout()->setVariable('bodyClass', 'login-page');
        $this->layout()->setVariable('wrapperClass', 'page-header');
        $this->layout()->setVariable('filterColor', 'orange');

        return new ViewModel(
            [
                'form' => $form,
            ]
        );
    }


    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function resetPasswordAction()
    {
        //If user is authenticated -> redirect to home page.
        if (!is_null($this->auth->getCurrentUser())) {
            $this->redirect()->toRoute('home', ['action' => 'index']);
        }

        $form = new ResetPasswordForm();

        $userEmail = $this->params()->fromQuery('email');
        $user = $this->userDomain->findOneBy(['email' => $userEmail]);

        $userToken = $this->auth->createResetToken($user);
        $tokenFromGet = $this->params()->fromQuery('token');


        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            if (isset($tokenFromGet)) {
                if ($tokenFromGet == $userToken) {

                    $data = $this->params()->fromPost();

                    if ($data['new_password'] == $data['confirm_new_password']) {
                        if (!is_null($data)) {
                            $this->userDomain->updateUserPassword($user, $data);
                            $this->auth->confirmResetPassword($user);
                            $this->redirect()->toRoute('profile', ['action' => 'index']);
                            //TO DO: place for notification
                        }
                        return $this->redirect()->toRoute('profile', ['action' => 'index']);

                    }
                    $parameters = ['type' => 'warning', 'message' => 'Passwords does not match!'];
                    $this->notificationsService->addNotificationMessage($parameters);
                }
            }
        }

        $this->layout()->setVariable('bodyClass', 'login-page');
        $this->layout()->setVariable('wrapperClass', 'page-header');
        $this->layout()->setVariable('filterColor', 'orange');

        return new ViewModel(['form' => $form,]);
    }


    /**
     * @return \Zend\Http\Response
     * @throws \Exception
     */
    public function logoutAction()
    {
        $this->auth->logout();

        $parameters = ['type' => 'info', 'message' => 'Your have been successfully logged out!'];
        $this->notificationsService->addNotificationMessage($parameters);

        return $this->redirect()->toRoute('home', ['action' => 'index']);

    }

    /**
     * @param Notifications $notificationsService
     */
    public function setNotificationsService($notificationsService)
    {
        $this->notificationsService = $notificationsService;
    }


}
