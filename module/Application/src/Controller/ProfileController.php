<?php


namespace Application\Controller;

use Application\Form\EmailChangeForm;
use Application\Form\NameChangeForm;
use Application\Form\PasswordChangeForm;
use Application\Model\UserDomain;
use Application\Service\Auth;
use Zend\Crypt\Password\Bcrypt;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Application\Service\Notifications;


/**
 * Class ProfileController
 *
 * @package Application\Controller
 */
class ProfileController extends AbstractActionController
{

    /** @var UserDomain $userDomain */
    protected $userDomain;

    /** @var Auth */
    protected $auth;

    /** @var Notifications */
    protected $notificationsService;


    /**
     * AuthController constructor.
     *
     * @param Auth $auth
     * @param UserDomain $userDomain
     */
    public function __construct(Auth $auth, UserDomain $userDomain)
    {
        $this->auth = $auth;
        $this->userDomain = $userDomain;
    }


    /**
     * @return ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function profileAction()
    {
        //If user is authenticated -> redirect to home page.
        if (is_null($this->auth->getCurrentUser())) {
            $this->redirect()->toRoute('login', ['action' => 'login']);
        }
        return new ViewModel();
    }

    /**
     * @return ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function emailAction()
    {
        //If user is authenticated -> redirect to home page.
        if (is_null($this->auth->getCurrentUser())) {
            $this->redirect()->toRoute('login', ['action' => 'login']);
        }
        $form = new EmailChangeForm();
        $isPasswordError = null;

        if ($this->getRequest()->isPost()) {

            $data = $this->params()->fromPost(); // Fill in the form with POST data
            $form->setData($data);

            if ($form->isValid()) {  // Validate form
                $data = $form->getData(); // Get filtered and validated data
                $currentUser = $this->auth->getCurrentUser(); //Get current User
                $user = $this->userDomain->findOneBy(['id' => $currentUser->getId()]); //find all data about that user

                $bcrypt = new Bcrypt();
                if ($bcrypt->verify($data['password'], $currentUser->getPassword())) {
                    $this->userDomain->updateUserEmail($user, $data);
                    $parameters = ['type' => 'success', 'message' => 'E-mail was updated!'];
                    $this->notificationsService->addNotificationMessage($parameters);
                } else {
                    $parameters = ['type' => 'warning', 'message' => 'Password incorrect!'];
                    $this->notificationsService->addNotificationMessage($parameters);
                }
            }

        }
        return new ViewModel(
            [
                'form' => $form,
            ]
        );
    }

    /**
     * @return ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function nameAction()
    {
        $form = new NameChangeForm();
        $isPasswordError = null;

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost(); // Fill in the form with POST data
            $form->setData($data);

            if ($form->isValid()) {  // Validate form
                $data = $form->getData(); // Get filtered and validated data
                $currentUser = $this->auth->getCurrentUser(); //Get current User
                $user = $this->userDomain->findOneBy(['id' => $currentUser->getId()]); //find all data about that user

                $bcrypt = new Bcrypt();
                if ($bcrypt->verify($data['password'], $currentUser->getPassword())) {
                    $this->userDomain->updateUserName($user, $data);
                    $parameters = ['type' => 'success', 'message' => 'Your Name was updated!'];
                    $this->notificationsService->addNotificationMessage($parameters);
                } else {
                    $parameters = ['type' => 'warning', 'message' => 'Password incorrect'];
                    $this->notificationsService->addNotificationMessage($parameters);
                }
            }
        }

        return new ViewModel(
            [
                'form' => $form,
            ]
        );
    }

    /**
     * @return ViewModel
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function passwordAction()
    {

        $form = new PasswordChangeForm();

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost(); // Fill in the form with POST data
            $form->setData($data);

            if ($form->isValid()) {  // Validate form
                $data = $form->getData(); // Get filtered and validated data
                $currentUser = $this->auth->getCurrentUser(); //Get current User
                $user = $this->userDomain->findOneBy(
                    ['id' => $currentUser->getId()]
                ); //find all data about that user


                $bcrypt = new Bcrypt();
                if ($bcrypt->verify($data['password'], $user->getPassword())) {
                    $this->userDomain->updateUserPassword($user, $data);
                    $parameters = ['type' => 'success', 'message' => 'Password changed!'];
                    $this->notificationsService->addNotificationMessage($parameters);
                } else {
                    $parameters = ['type' => 'warning', 'message' => 'Password incorrect!'];
                    $this->notificationsService->addNotificationMessage($parameters);
                }
            } elseif($form->isValid() == false) {
                $parameters = ['type' => 'warning', 'message' => 'New password does not match!'];
                $this->notificationsService->addNotificationMessage($parameters);
            }
        }
        return new ViewModel(
            [
                'form' => $form,
            ]
        );
    }

    /**
     * @param Notifications $notificationsService
     */
    public function setNotificationsService($notificationsService)
    {
        $this->notificationsService = $notificationsService;
    }

}
