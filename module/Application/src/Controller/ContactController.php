<?php


namespace Application\Controller;

use Mailing\Service\Mailing;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use ZendService\ReCaptcha\ReCaptcha;



/**
 * Class ContactController
 * @package Application\Controller
 */
class ContactController extends AbstractActionController
{

    /** @var Mailing $mailing */
    protected $mailing;

    /** @var [] */
    protected $config;

    /**
     * @return JsonModel|ViewModel
     */
    public function indexAction()
    {

        // Google ReCaptcha
        $pubKey = $this->config['googleCaptchaConfig']['keys']['publicKey'];
        $privKey = $this->config['googleCaptchaConfig']['keys']['privateKey'];
        $reCaptcha = new ReCaptcha($pubKey, $privKey);

        if ($this->getRequest()->isPost()) {


            $data = $this->params()->fromPost();

            if ($data['g-recaptcha-response'] !== "") {
                $email = $data['email'];
                $this->mailing->sendContactMessage($email, [
                    'name' => $data['name'],
                    'phone' => $data['phone'],
                    'email' => $data['email'],
                    'message' => $data['message'],
                ]);
                $notyParameters = ['type' => 'success', 'message' => 'Your message was sent successfully!'];
            }
            else{
                $notyParameters = ['type' => 'warning', 'message' => 'Google ReCaptcha Error!'];
            }
            /**
             * In case if form is valid we do not need to return form again. We return JSON with 'callbacks' method,
             * callbacks are executed in noty-notifications.js.
             */
            return new JsonModel([

                        'notifications' => [[
                            'type'=> $notyParameters['type'],
                            'message' => $notyParameters['message'],
                        ]],
            ]);
        }

        $viewModel = new ViewModel([
            'reCaptcha' => $reCaptcha,
        ]);

        return $viewModel;

    }


    /**
     * @param Mailing $mailing
     */
    public function setMailingService(Mailing $mailing)
    {
        $this->mailing = $mailing;
    }

    /**
     * @param mixed $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }


}