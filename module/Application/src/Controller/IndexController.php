<?php


namespace Application\Controller;

use Application\Model\BannerCategoryDomain;
use Application\Model\BannerDomain;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use ZendService\ReCaptcha\ReCaptcha;



/**
 * Class IndexController
 * @package Application\Controller
 */
class IndexController extends AbstractActionController
{

    /** @var [] */
    protected $config;

    /** @var BannerDomain */
    protected $bannerDomain;

    /** @var BannerCategoryDomain */
    protected $bannerCategoryDomain;

    /**
     * @return ViewModel
     * @throws \Exception
     */
    public function indexAction()
    {
        // Google Captcha
        $pubKey = $this->config['googleCaptchaConfig']['keys']['publicKey'];
        $privKey = $this->config['googleCaptchaConfig']['keys']['privateKey'];
        $reCaptcha = new ReCaptcha($pubKey, $privKey);
        $limit = 10;

        $dateTime = new \DateTime('now');
        $bannersQuery = $this->bannerDomain->getBanners([
            'city' => $this->params()->fromQuery('city'),
            'category' => $this->params()->fromQuery('category'),
            'timeStart' => $dateTime,
            'timeEnd' => $dateTime,
        ])->setMaxResults($limit);

        $banners = $bannersQuery->execute();

        $categories = $this->bannerCategoryDomain->getBannerCategories();

        return new ViewModel([
            'reCaptcha' => $reCaptcha,
            'banners' => $banners,
            'categories' => $categories,
        ]);
    }

    /**
     * @param mixed $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @param BannerDomain $bannerDomain
     */
    public function setBannerDomain(BannerDomain $bannerDomain)
    {
        $this->bannerDomain = $bannerDomain;
    }

    /**
     * @param BannerCategoryDomain $bannerCategoryDomain
     */
    public function setBannerCategoryDomain(BannerCategoryDomain $bannerCategoryDomain)
    {
        $this->bannerCategoryDomain = $bannerCategoryDomain;
    }

}
