<?php
namespace Application\Controller\Factory;

use Application\Controller\IndexController;
use Application\Model\BannerCategoryDomain;
use Application\Model\BannerDomain;
use Application\Service\DomainModelManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class IndexControllerFactory
 * @package Application\Controller\Factory
 */
class IndexControllerFactory implements FactoryInterface
{


    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return IndexController|object
     */
    public function __invoke (ContainerInterface $container, $requestedName, array $options = null )
    {
        $config = $container->get('Config');

        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        $bannerDomain = $domainManager->get(BannerDomain::class);
        $bannerCategoryDomain = $domainManager->get(BannerCategoryDomain::class);

        $indexController = new IndexController();
        $indexController->setConfig($config);
        $indexController->setBannerDomain($bannerDomain);
        $indexController->setBannerCategoryDomain($bannerCategoryDomain);

        // Instantiate the controller and inject dependencies
        return $indexController;

    }
}

