<?php

namespace Application\Controller\Factory;

use Application\Controller\ContactController;
use Interop\Container\ContainerInterface;
use Mailing\Service\Mailing;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ContactControllerFactory
 * @package Application\Controller\Factory
 */
class ContactControllerFactory implements FactoryInterface
{


    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ContactController|object
     */
    public function __invoke (ContainerInterface $container, $requestedName, array $options = null )
    {
        $mailing = $container->get(Mailing::class);

        $config = $container->get('Config');


        $contactController = new ContactController();
        $contactController->setMailingService($mailing);
        $contactController->setConfig($config);

        // Instantiate the controller and inject dependencies
        return $contactController;
    }
}