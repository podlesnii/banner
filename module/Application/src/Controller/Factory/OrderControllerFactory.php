<?php

namespace Application\Controller\Factory;

use Application\Controller\OrderController;
use Application\Model\BannerDomain;
use Application\Model\BannerStatusHistoryDomain;
use Application\Model\InvoiceDomain;
use Application\Model\UserDomain;
use Application\Service\Auth;
use Application\Service\DomainModelManager;
use Application\Service\Notifications;
use Application\Service\Order;
use Interop\Container\ContainerInterface;
use Mailing\Service\Mailing;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class OrderControllerFactory
 * @package Application\Controller\Factory
 */
class OrderControllerFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return OrderController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke (ContainerInterface $container, $requestedName, array $options = null )
    {
        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        $bannerDomain = $domainManager->get(BannerDomain::class);
        $bannerStatusHistoryDomain = $domainManager->get(BannerStatusHistoryDomain::class);
        $userDomain = $domainManager->get(UserDomain::class);
        $invoiceDomain = $domainManager->get(InvoiceDomain::class);
        $orderService = $container->get(Order::class);

        $mailing = $container->get(Mailing::class);

        $auth = $container->get(Auth::class);

        $notificationsService = $container->get(Notifications::class);

        $orderController = new OrderController();
        $orderController->setBannerDomain($bannerDomain);
        $orderController->setBannerStatusHistoryDomain($bannerStatusHistoryDomain);
        $orderController->setUserDomain($userDomain);
        $orderController->setOrderService($orderService);
        $orderController->setInvoiceDomain($invoiceDomain);
        $orderController->setMailingService($mailing);
        $orderController->setAuthService($auth);
        $orderController->setNotificationsService($notificationsService);


        // Instantiate the controller and inject dependencies
        return $orderController;
    }
}