<?php

namespace Application\Controller\Factory;

use Application\Controller\ProfileController;
use Application\Model\UserDomain;
use Application\Service\Auth;
use Application\Service\DomainModelManager;
use Application\Service\Notifications;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class ProfileControllerFactory
 * @package Application\Controller\Factory
 */
class ProfileControllerFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ProfileController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {

        /** @var Auth $auth */
        $auth = $container->get(Auth::class);

        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        $userDomain = $domainManager->get(UserDomain::class);

        $notificationsService = $container->get(Notifications::class);

        $profileController = new ProfileController($auth, $userDomain);
        $profileController->setNotificationsService($notificationsService);

        return $profileController;

    }
}