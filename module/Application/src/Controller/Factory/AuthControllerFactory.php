<?php
namespace Application\Controller\Factory;

use Application\Controller\AuthController;
use Application\Model\UserDomain;
use Application\Service\Auth;
use Application\Service\DomainModelManager;
use Application\Service\Notifications;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class AuthControllerFactory
 * @package Application\Controller\Factory
 */
class AuthControllerFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return AuthController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke (ContainerInterface $container, $requestedName, array $options = null )
    {

        /** @var Auth $auth */
        $auth = $container->get(Auth::class);

        $notificationsService = $container->get(Notifications::class);

        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        $userDomain = $domainManager->get(UserDomain::class);

        $config = $container->get('Config');

        $notificationsService = $container->get(Notifications::class);

        // Instantiate the controller and inject dependencies
        $authController = new AuthController($auth, $userDomain, $config);
        $authController->setNotificationsService($notificationsService);

        return $authController;


    }
}

