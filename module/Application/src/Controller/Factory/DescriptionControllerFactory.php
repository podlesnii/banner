<?php

namespace Application\Controller\Factory;

use Application\Controller\DescriptionController;
use Application\Model\BannerDomain;
use Application\Service\Auth;
use Application\Service\DomainModelManager;
use Application\Service\Notifications;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Controller\SearchController;


/**
 * Class DescriptionControllerFactory
 * @package Application\Controller\Factory
 */
class DescriptionControllerFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return SearchController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke (ContainerInterface $container, $requestedName, array $options = null )
    {
        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        $bannerDomain = $domainManager->get(BannerDomain::class);

        /** @var Auth $auth */
        $auth = $container->get(Auth::class);

        $notificationsService = $container->get(Notifications::class);

        $descriptionController = new DescriptionController();
        $descriptionController->setBannerDomain($bannerDomain);
        $descriptionController->setAuth($auth);
        $descriptionController->setNotificationsService($notificationsService);


        // Instantiate the controller and inject dependencies
        return $descriptionController;
    }
}