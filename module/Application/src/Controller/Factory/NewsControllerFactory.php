<?php

namespace Application\Controller\Factory;

use Admin\Controller\NewsAdminController;
use Application\Controller\NewsController;
use Application\Model\NewsDomain;
use Application\Service\DomainModelManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class NewsControllerFactory
 * @package Admin\Controller\Factory
 */
class NewsControllerFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return NewsAdminController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        $newsDomain = $domainManager->get(NewsDomain::class);

        $newsController = new NewsController();
        $newsController->setNewsDomain($newsDomain);

        // Instantiate the controller and inject dependencies
        return $newsController;
    }
}




