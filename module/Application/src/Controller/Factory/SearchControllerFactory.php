<?php

namespace Application\Controller\Factory;

use Application\Form\SearchForm;
use Application\Model\BannerDomain;
use Application\Service\DomainModelManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Controller\SearchController;


/**
 * Class SearchControllerFactory
 * @package Application\Controller\Factory
 */
class SearchControllerFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return SearchController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke (ContainerInterface $container, $requestedName, array $options = null )
    {
        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        $bannerDomain = $domainManager->get(BannerDomain::class);

        $searchForm = $container->get(SearchForm::class);

        $searchController = new SearchController();
        $searchController->setBannerDomain($bannerDomain);
        $searchController->setSearchForm($searchForm);

        // Instantiate the controller and inject dependencies
        return $searchController;
    }
}