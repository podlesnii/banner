<?php


namespace Application\Controller;

use Application\Entity\Banner;
use Application\Model\BannerStatusHistoryDomain;
use Application\Model\InvoiceDomain;
use Application\Model\UserDomain;
use Application\Service\Auth;
use Application\Service\Notifications;
use Application\Service\Order;
use DateTime;
use Doctrine\ORM\Query;
use Mailing\Service\Mailing;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Model\BannerDomain;

/**
 * Class OrderController
 *
 * @package Application\Controller
 */
class OrderController extends AbstractActionController
{

    /** @var BannerDomain */
    protected $bannerDomain;

    /** @var BannerStatusHistoryDomain */
    protected $bannerStatusHistoryDomain;

    /** @var Order $order */
    protected $order;

    /** @var Auth $auth */
    protected $auth;

    /** @var UserDomain $userDomain */
    protected $userDomain;

    /** @var InvoiceDomain $invoiceDomain */
    protected $invoiceDomain;

    /** @var Mailing $mailing */
    protected $mailing;

    /** @var Notifications */
    protected $notificationsService;

    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        $banners = $this->bannerDomain->getBanners([
            'id' => $this->params()->fromQuery('id'),
        ]);

        $banner = $banners->execute();

        return new ViewModel([
            'banner' => $banner,
        ]);
    }

    /**
     * @return ViewModel
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Spipu\Html2Pdf\Exception\Html2PdfException
     */
    public function orderedAction()
    {
        $bannerId = $this->params()->fromQuery('id');

        if (!is_null($bannerId)) {
            $getBanner = $this->bannerDomain->getBanners(['id' => $bannerId], Query::HYDRATE_OBJECT);
            $banners = $getBanner->execute();
            /** @var Banner $banner */
            $banner = $banners['0'];

            // Change banner status to "BOOKED"
            $status = "booked";

            // data for banner status history
            $timeStart = new DateTime('now');
            $timeEnd = new DateTime('now + 10 day');
            $bannerStatus = ['status' => $status, 'timeStart' => $timeStart->format('Y-m-d'),
                'timeEnd' => $timeEnd->format('Y-m-d')];

            $this->order->bookingBanner($banner, $status);

            //Add banner status to history
            $this->bannerStatusHistoryDomain->addBannerStatusHistory($bannerId, $bannerStatus);

            $currentUserId = $this->auth->getCurrentUser()->getId();
            $getCurrentUser = $this->userDomain->getUsers(['id' => $currentUserId]);
            $currentUser = $getCurrentUser->execute();

            //get Owner account data
            $ownerAccount = $this->invoiceDomain->getInvoice(['owner_id' => $currentUserId]);

            //generate PDF
            $fileName = $this->order->toPdf(['banner' => $banner, 'currentUser' => $currentUser, 'ownerAccount' => $ownerAccount]);

            //Send Confirmation Mail to user e-mail
            $email = $currentUser['0']['email'];
            $this->mailing->sendBookingConfirmation($email,
                ['banner' => $banner, 'currentUser' => $currentUser,
                    'ownerAccount' => $ownerAccount, 'fileName' => $fileName]);

            $parameters = ['type' => 'success', 'message' => 'Banner was successfully booked'];
            $parameters2 = ['type' => 'success', 'message' => 'Please, check your e-mail'];
            $this->notificationsService->addNotificationMessage($parameters);
            $this->notificationsService->addNotificationMessage($parameters2);

        }

        return new ViewModel([
            'fileName' => $fileName,
        ]);
    }


    /**
     * @return \Zend\Http\Response\Stream
     */
    public function downloadAction()
    {

        if (!is_null($_GET)) {

            $fileName = $this->params()->fromQuery('invoice');

            $response = new \Zend\Http\Response\Stream();
            $response->setStream(fopen('data/invoices/' . $fileName, 'r'));
            $response->setStatusCode(200);

            $headers = new \Zend\Http\Headers();
            $headers->addHeaderLine('Content-Type', 'whatever your content type is')
                ->addHeaderLine('Content-Disposition', 'attachment; filename="' . 'invoice' . substr($fileName, '0', '14') . '"')
                ->addHeaderLine('Content-Length', filesize('data/invoices/' . $fileName));

            $response->setHeaders($headers);

            return $response;

        }
        return null;
    }


    /**
     * @param BannerDomain $bannerDomain
     */
    public function setBannerDomain(BannerDomain $bannerDomain)
    {
        $this->bannerDomain = $bannerDomain;
    }

    /**
     * @param BannerStatusHistoryDomain $bannerStatusHistoryDomain
     */
    public function setBannerStatusHistoryDomain(BannerStatusHistoryDomain $bannerStatusHistoryDomain)
    {
        $this->bannerStatusHistoryDomain = $bannerStatusHistoryDomain;
    }

    /**
     * @param Order $order
     */
    public function setOrderService(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param Auth $auth
     */
    public function setAuthService(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param UserDomain $userDomain
     */
    public function setUserDomain(UserDomain $userDomain)
    {
        $this->userDomain = $userDomain;
    }

    /**
     * @param InvoiceDomain $invoiceDomain
     */
    public function setInvoiceDomain(InvoiceDomain $invoiceDomain)
    {
        $this->invoiceDomain = $invoiceDomain;
    }

    /**
     * @param Mailing $mailing
     */
    public function setMailingService(Mailing $mailing)
    {
        $this->mailing = $mailing;
    }

    /**
     * @param Notifications $notificationsService
     */
    public function setNotificationsService($notificationsService)
    {
        $this->notificationsService = $notificationsService;
    }


}
