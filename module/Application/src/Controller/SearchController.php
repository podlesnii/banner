<?php


namespace Application\Controller;

use Application\Form\SearchForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Model\BannerDomain;


/**
 * Class SearchController
 * @package Application\Controller
 */
class SearchController extends AbstractActionController
{

    /** @var BannerDomain */
    protected $bannerDomain;

    /** @var SearchForm */
    protected $searchForm;


    /**
     * @return ViewModel
     */
    public function indexAction()
    {

        $dateTime = new \DateTime('now');


        $query = $this->bannerDomain->getBanners([
            'city' => $this->params()->fromQuery('city'),
            'category' => $this->params()->fromQuery('category'),
            'timeStart' => $dateTime,
            'timeEnd' => $dateTime,
        ]);


        $banners = $query->execute();
        $bannersJson = json_encode($banners);

        $viewModel = new ViewModel([
            'form' => $this->searchForm,
            'bannersJson' => $bannersJson,
            'banners' => $banners,
        ]);

        return $viewModel;
    }


    /**
     * @param BannerDomain $bannerDomain
     */
    public function setBannerDomain(BannerDomain $bannerDomain)
    {
        $this->bannerDomain = $bannerDomain;
    }

    /**
     * @param SearchForm $searchForm
     */
    public function setSearchForm(SearchForm $searchForm)
    {
        $this->searchForm = $searchForm;
    }


}
