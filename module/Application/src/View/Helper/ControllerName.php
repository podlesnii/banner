<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Class ControllerName
 * @package Application\View\Helper
 */
class ControllerName extends AbstractHelper
{

    protected $routeMatch;

    /**
     * ControllerName constructor.
     *
     * @param $routeMatch
     */
    public function __construct($routeMatch)
    {
        $this->routeMatch = $routeMatch;
    }

    /**
     * @return mixedz
     */
    public function __invoke()
    {
        if ($this->routeMatch) {
            $controller = $this->routeMatch->getParam('controller', 'index');

            return $controller;
        }

        return null;
    }
}