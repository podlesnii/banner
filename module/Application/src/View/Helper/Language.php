<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Class Language
 * @package Application\View\Helper
 */
class Language extends AbstractHelper
{

    /** @var \Application\Service\Language */
    protected $languageService;

    /**
     * Language constructor.
     *
     * @param $languageService
     */
    public function __construct($languageService)
    {
        $this->languageService = $languageService;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrentLocale()
    {
        return $this->languageService->getCurrentLocale();
    }

    /**
     * @return string
     */
    public function getCurrentLanguageShortcut()
    {
        return $this->languageService->getCurrentLanguageShortcut();
    }
}