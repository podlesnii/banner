<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Class Notifications
 * @package Application\View\Helper
 */
class Notifications extends AbstractHelper
{

    /** @var $notificationsService Notifications */
    public $notificationsService;

    /**
     * Notifications constructor.
     *
     * @param $notificationsService \Application\Service\Notifications
     */
    public function __construct($notificationsService)
    {
        $this->notificationsService = $notificationsService;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {

        $message = $this->notificationsService->getMessages();

        return $message;

    }



}