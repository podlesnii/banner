<?php

namespace Application\View\Helper;

use Application\Service\Auth;
use Zend\View\Helper\AbstractHelper;

/**
 * Class User
 * @package Application\View\Helper
 */
class User extends AbstractHelper
{

    /** @var Auth */
    protected $authService;

    /**
     * User constructor.
     *
     * @param $authService
     */
    public function __construct($authService)
    {
        $this->authService = $authService;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * @return bool
     */
    public function isAuthenticated()
    {
        $user = $this->authService->getCurrentUser();
        return (!is_null($user));
    }

    /**
     * @return null|string
     */
    public function getCurrentUserName()
    {
        if (!$this->isAuthenticated()) {
            return null;
        }
        return $this->authService->getCurrentUser()->getName();
    }

    /**
     * @return null|string
     */
    public function getCurrentUserIcon()
    {
        if (!$this->isAuthenticated()) {
            return null;
        }
        return $this->authService->getCurrentUser()->getIcon();
    }

    /**
     * @return null|string
     */
    public function getCurrentUserEmail()
    {
        if (!$this->isAuthenticated()) {
            return null;
        }
        return $this->authService->getCurrentUser()->getEmail();
    }

    /**
     * @return null|string
     */
    public function getCurrentUserRole()
    {
        if (!$this->isAuthenticated()) {
            return null;
        }
        return $this->authService->getCurrentUser()->getRole()->getName();
    }



}