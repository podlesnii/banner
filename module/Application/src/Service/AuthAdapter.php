<?php

    namespace Application\Service;

    use Application\Model\UserDomain;
    use Zend\Authentication\Adapter\AdapterInterface;
    use Zend\Authentication\Result;
    use Zend\Crypt\Password\Bcrypt;


    /**
     * Class AuthAdapter
     * @package Application\Service
     */
    class AuthAdapter implements AdapterInterface
    {
        /**
         * User login.
         * @var string
         */
        private $login;

        /**
         * Password
         * @var string
         */
        private $password;

        /** @var UserDomain */
        protected $userDomain;

        /**
         * hashPassword
         * @var string
         */
        private $hashPassword;

        /**
         * Constructor.
         *
         * @param $userDomain
         */
        public function __construct(userDomain $userDomain)
        {
            $this->userDomain = $userDomain;
        }

        /**
         * @param $login
         */
        public function setlogin($login)
        {
            $this->login = $login;

        }

        /**
         * @param $password
         */
        public function setPassword($password)
        {
            $this->password = (string)$password;
        }


        /**
         * @return Result
         * @throws \Doctrine\ORM\NonUniqueResultException
         */
        public function authenticate()
        {

            $user = $this->userDomain->findOneBy([
                'login' => $this->login,
            ]);

            if ($user == null) {
                return new Result(
                    Result::FAILURE_CREDENTIAL_INVALID,
                    $user,
                    ['Incorrect login and/or password.']);
            }
            $hashPassword = $user->getPassword();
            if (!is_null($this->password) && is_null($this->hashPassword)) {
                // Now we need to calculate hash based on user-entered password and compare
                // it with the password hash stored in database.
                $bcrypt = new Bcrypt();
                $passwordHash = $user->getPassword();

                if ($bcrypt->verify($this->password, $passwordHash) and $user->getActive() == true) {
                    // Great! The password hash matches. Return user identity (email) to be
                    // saved in session for later use.
                    return new Result(
                        Result::SUCCESS,
                        $user,
                        ['Authenticated successfully.']);
                }
                if ($bcrypt->verify($this->password, $passwordHash) and $user->getActive() == false) {
                    return new Result(
                        Result::FAILURE_UNCATEGORIZED,
                        $user,
                        ['E-mail is not confirmed']);
                }
            }

            // If password check didn't pass return 'Invalid Credential' failure status.
            return new Result(
                Result::FAILURE_CREDENTIAL_INVALID,
                null,
                ['Invalid credentials.']);

        }
    }

