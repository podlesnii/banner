<?php

namespace Application\Service;


use Application\Model\BannerDomain;
use Mailing\Service\Mailing;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Html2Pdf;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Model\ViewModel;



/**
 * Class Order
 *
 * @package Application\Service
 */
class Order
{

    /** @var BannerDomain $bannerDomain */
    protected $bannerDomain;

    /** @var Mailing $mailing */
    protected $mailing;

    /** @var Html2Pdf $html2pdf */
    protected $html2pdf;

    /** @var PhpRenderer */
    protected $viewRenderer;

    protected $variables;


    /**
     * @param $variables
     * @return string
     * @throws Html2PdfException
     */
    public function toPdf($variables) {

        $content = $this->renderTemplate('pdf/invoice.phtml', $variables);

        $fileName = $variables['banner']->getBannerStatuses()->last()->getTimeStart()->format('Ymdhms')
            . sha1($variables['currentUser']['0']['id']. $variables['banner']->getId());
        $fileDir = '/../data/invoices/'. $fileName;

        $html2pdf = new Html2Pdf('P', 'A4', 'en', 'true', 'UTF-8', '10');
        $html2pdf->setDefaultFont('Arial');
        $html2pdf->writeHTML($content);
        $html2pdf->output($_SERVER['DOCUMENT_ROOT'].$fileDir.'.pdf', 'F');

        return $fileName;
    }

    /**
     * @param $banner
     * @param $status
     *
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function bookingBanner($banner, $status)
    {

       $booked = $this->bannerDomain->addBannerStatus($banner, $status);

        return $booked;
    }


    /**
     * @param $templateName
     * @param $variables
     *
     * @return string
     */
    public function renderTemplate($templateName, $variables) {

        $viewModel = new ViewModel();
        $viewModel->setTemplate($templateName)
                  ->setVariables($variables);

        $content = $this->viewRenderer->render($viewModel);

        return $content;
    }



    /**
     * @param BannerDomain $bannerDomain
     */
    public function setBannerDomain(BannerDomain $bannerDomain)
    {
        $this->bannerDomain = $bannerDomain;
    }

    /**
     * @param Mailing $mailing
     */
    public function setMailingService(Mailing $mailing)
    {
        $this->mailing = $mailing;
    }

    /**
     * @param Html2Pdf $html2pdf
     */
    public function setHtml2Pdf(Html2Pdf $html2pdf)
    {
        $this->html2pdf = $html2pdf;
    }

     /**
     * @param PhpRenderer $viewRenderer
     */
    public function setViewRenderer(PhpRenderer $viewRenderer)
    {
        $this->viewRenderer = $viewRenderer;
    }


}