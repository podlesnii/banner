<?php

namespace Application\Service;

use Acl\Model\RoleDomain;
use Application\Entity\User;
use Application\Model\UserDomain;
use Mailing\Service\Mailing;
use Zend\Authentication\AuthenticationService;
use Zend\Session\SessionManager;
use Zend\Authentication\Result;

/**
 * Class Auth
 */
class Auth
{
    /**
     * Authentication service.
     *
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authenticationService;

    /**
     * Session manager.
     *
     * @var SessionManager
     */
    private $sessionManager;

    /** @var Mailing */
    protected $mailingService;

    /** @var UserDomain */
    protected $userDomain;

    /** @var RoleDomain $roleDomain */
    protected $roleDomain;

    /** @var User */
    protected $currentUserEntity;

    /**
     * @param UserDomain $userDomain
     */
    public function setUserDomain(UserDomain $userDomain)
    {
        $this->userDomain = $userDomain;
    }

    /**
     * @param RoleDomain $roleDomain
     */
    public function setRoleDomain(RoleDomain $roleDomain)
    {
        $this->roleDomain = $roleDomain;
    }

    /**
     * Auth constructor.
     *
     * @param AuthenticationService $authenticationService
     * @param SessionManager        $sessionManager
     * @param Mailing               $mailingService
     */
    public function __construct(AuthenticationService $authenticationService,
        SessionManager $sessionManager, Mailing $mailingService)
    {
        $this->authenticationService = $authenticationService;
        $this->sessionManager = $sessionManager;
        $this->mailingService = $mailingService;
    }


    /**
     * @param $data
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addUser($data)
    {
        $giveRole = $this->roleDomain->findOneBy(['name' => $data['role']]);
        $data['role'] = $giveRole->getId();

        $user = $this->userDomain->addUser($data);

        $token = $this->createUserToken($user);
        $email = $user->getEmail();


        /** @var Auth $token
         * @var Auth $email
         */
        $this->mailingService->setRegistrationConfirmation(
            $email, ['token' => $token, 'email' => $email]);
    }


    /**
     * Create user token
     *
     * @param User $user
     *
     * @return string
     */
    public function createUserToken($user)
    {
        $userToken = sha1(
            $user->getEmail() . $user->getLogin() . $user->getPassword()
            . $user->getId()
        );
        return $userToken;
    }

    /**
     * @param User $user
     *
     * @return Result
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function emailConfirmed($user)
    {
        $this->userDomain->activateUser($user);

        $login = $user->getLogin();
        $hashPassword = $user->getPassword();

        // Authenticate with login/password.
        /** @var AuthAdapter $authAdapter */
        $authAdapter = $this->authenticationService->getAdapter();
        $authAdapter->setLogin($login);
        $authAdapter->setPassword($hashPassword);
        $result = $this->authenticationService->authenticate();

        return $result;
    }

    /**
     * @param User $user
     *
     */
    public function resetPassword($user){

        $email = $user->getEmail();
        $token = $this->createResetToken($user);

        $this->mailingService->sendResetPassword(
            $email, ['token' => $token, 'email' => $email]
        );

    }

    /**
     * @param User $user
     *
     * @return string
     */
    public function createResetToken($user)
    {
        $login = $user->getLogin();
        $userId = $user->getId();

        $token = sha1($login.$userId);

        return $token;
    }

    /**
     * @param User $user
     *
     * @return Result
     */
    public function confirmResetPassword($user)
    {
        $login = $user->getLogin();
        $password = $user->getPassword();

        // Authenticate with login/password.
        /** @var AuthAdapter $authAdapter */
        $authAdapter = $this->authenticationService->getAdapter();
        $authAdapter->setLogin($login);
        $authAdapter->setPassword($password);
        $result = $this->authenticationService->authenticate();

        return $result;

    }

    /**
     * @param string $login
     * @param string $password
     *
     * @return \Zend\Authentication\Result
     * @throws \Exception
     */
    public function login($login, $password)
    {
        // Authenticate with login/password.
        /** @var AuthAdapter $authAdapter */
        $authAdapter = $this->authenticationService->getAdapter();
        $authAdapter->setLogin($login);
        $authAdapter->setPassword($password);
        $result = $this->authenticationService->authenticate();

        return $result;
    }

    /**
     * @throws \Exception
     */
    public function logout()
    {
        // Clear user_data from session.
        $this->authenticationService->clearIdentity();

    }


    /**
     * @return User|mixed|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCurrentUser()
    {
        if ($this->authenticationService->hasIdentity()) {
            if (is_null($this->currentUserEntity)) {
                $id = $this->authenticationService->getIdentity();
                $this->currentUserEntity = $this->userDomain->findUserWithRole($id);
            }
            return $this->currentUserEntity;
        }
        return null;
    }


    /**
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCurrentRole()
    {
        $currentUser = $this->getCurrentUser();
        if (!is_null($currentUser)) {
            return $currentUser->getRole()->getName();
        }
        return 'Guest';
    }

}