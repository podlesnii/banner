<?php
namespace Application\Service\Doctrine;

use Application\Service\Doctrine\Configuration\Configuration;
use Application\Service\DomainModelManager;
use Doctrine\DBAL\Migrations\OutputWriter;
use Interop\Container\ContainerInterface;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Class MigrationsConfigurationFactory
 * @package Application\Service\Doctrine
 */
class MigrationsConfigurationFactory extends \DoctrineORMModule\Service\MigrationsConfigurationFactory
{

    /**
     * {@inheritDoc}
     *
     * @return \Doctrine\DBAL\Migrations\Configuration\Configuration
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $name             = $this->getName();
        /* @var $connection \Doctrine\DBAL\Connection */
        $connection       = $container->get('doctrine.connection.' . $name);
        $appConfig        = $container->get('config');
        $migrationsConfig = $appConfig['doctrine']['migrations_configuration'][$name];

        $output = new ConsoleOutput();
        $writer = new OutputWriter(function ($message) use ($output) {
            return $output->writeln($message);
        });

        $configuration = new Configuration($connection, $writer);

        $configuration->setName($migrationsConfig['name']);
        $configuration->setMigrationsDirectory($migrationsConfig['directory']);
        $configuration->setMigrationsNamespace($migrationsConfig['namespace']);
        $configuration->setMigrationsTableName($migrationsConfig['table']);
        $configuration->registerMigrationsFromDirectory($migrationsConfig['directory']);
        $configuration->setCustomTemplate($appConfig['doctrine']['custom_template']);


        $domainModelManager = $container->get(DomainModelManager::class);
        $configuration->setDomainModelManager($domainModelManager);

        if (method_exists($configuration, 'setMigrationsColumnName')) {
            $configuration->setMigrationsColumnName($migrationsConfig['column']);
        }

        return $configuration;
    }

}