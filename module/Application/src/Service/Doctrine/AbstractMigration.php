<?php

namespace Application\Service\Doctrine;

use Application\Service\Doctrine\Configuration\Configuration;
use Doctrine\DBAL\Schema\Schema;
use Application\Service\DomainModelManager;


/**
 * Class AbstractMigration
 * @package Application\Service\Doctrine
 */
abstract class AbstractMigration extends \Doctrine\DBAL\Migrations\AbstractMigration
{

    /**
     * @param Schema $schema
     */
    public function postUp( Schema $schema)
    {
        /** @var Configuration $configuration */
        $configuration = $this->version->getConfiguration();
        $domainModelManager =  $configuration->getDomainModelManager();

        $this->seed($domainModelManager);
    }

    /**
     * @param Schema $schema
     */
    public function up( Schema $schema)
    {

    }

    /**
     * @param Schema $schema
     */
    public function down( Schema $schema)
    {

    }

    /**
     * @param DomainModelManager $domainManager
     * @return mixed
     */
    abstract function seed( DomainModelManager $domainManager);
}
