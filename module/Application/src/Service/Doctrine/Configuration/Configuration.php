<?php
namespace Application\Service\Doctrine\Configuration;

use Application\Service\DomainModelManager;

/**
 * Class Configuration
 * @package Application\Service\Doctrine\Configuration
 */
class Configuration extends \Doctrine\DBAL\Migrations\Configuration\Configuration
{

    /** @var DomainModelManager */
    protected $domainModelManager;


    /**
     * @param DomainModelManager $domainModelManager
     */
    public function setDomainModelManager($domainModelManager)
    {
        $this->domainModelManager = $domainModelManager;
    }

    /**
     * @return DomainModelManager
     */
    public function getDomainModelManager()
    {
        return $this->domainModelManager;
    }

}