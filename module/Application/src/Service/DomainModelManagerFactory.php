<?php

namespace Application\Service;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class DomainModelManagerFactory
 * @package Application\Service
 */
class DomainModelManagerFactory implements FactoryInterface
{
    /**
     *
     * @param  ContainerInterface $container
     * @param $name
     * @param  null|array $options
     *
     * @return DomainModelManager
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $name, array $options = null)
    {
        if (is_null($options)) {
            $options = $container->get('Config');
        }
        return new DomainModelManager($container, $options['domain_model_manager']);
    }
}
