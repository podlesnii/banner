<?php

namespace Application\Service;
use Application\Model\UserDomain;
use Zend\Session\Container;
use Zend\Session\SessionManager;


/**
 * Class Language
 * @package Application\Service
 */
class Language
{

    /** @var SessionManager */
    protected $sessionManager;

    /** @var Container */
    protected $languageSessionContainer;

    protected $defaultLocale;

    protected $existingLocales;

    /** @var Auth $auth*/
    protected $auth;

    /** @var UserDomain $userDomain*/
    protected $userDomain;

    /**
     * Language constructor.
     *
     * @param $sessionManager
     * @param $config
     */
    public function __construct($sessionManager, $config)
    {
        $this->sessionManager = $sessionManager;
        $this->defaultLocale = $config['defaultLocale'];
        $this->existingLocales = $config['existingLocales'];
        $this->languageSessionContainer = new Container('Language', $sessionManager);

    }

    /**
     * @param $locale
     *
     * @return bool
     */
    public function localeExists($locale)
    {
        if (in_array($locale, $this->existingLocales)) {
            return true;
        }
        return false;
    }


    /**
     * @return mixed
     * @throws \Exception
     */
    public function getCurrentLocale ()
    {
        $currentUser = $this->auth->getCurrentUser(); //Get current User
        if (!is_null($currentUser)) {
            $user = $this->userDomain->findOneBy(['id' => $currentUser->getId()]); //find all data about that user
            $currentLocale = $user->getLanguage();
            return $currentLocale;
        }

        $currentLocale = $this->languageSessionContainer->locale;
        if (is_null($currentLocale)) {
            $currentLocale = $this->defaultLocale;
            $this->setCurrentLocale($currentLocale);
        }
        return $currentLocale;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getCurrentLanguageShortcut()
    {

        $currentLocale = $this->getCurrentLocale();
        $localeParts = explode('_', $currentLocale);

        return strtolower($localeParts[1]);
    }


    /**
     * @param $locale
     * @return $this
     * @throws \Exception
     */
    public function setCurrentLocale($locale)
    {
        $this->languageSessionContainer->locale = $locale;

        $currentUser = $this->auth->getCurrentUser(); //Get current User
        if (!is_null($currentUser))
        {
            $user = $this->userDomain->findOneBy(['id' => $currentUser->getId()]); //find all data about that user
            $this->userDomain->updateUserLanguage($user, $locale);
        }

        return $this;
    }

    /**
     * @param $auth Auth
     */
    public function setAuth($auth)
    {
        $this->auth = $auth;

    }

    /**
     * @param UserDomain $userDomain
     */
    public function setUserDomain($userDomain)
    {
        $this->userDomain = $userDomain;

    }

}