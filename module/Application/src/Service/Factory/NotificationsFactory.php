<?php
namespace Application\Service\Factory;


use Application\Service\Notifications;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Session\SessionManager;


/**
 * Class NotificationsFactory
 * @package Application\Service\Factory
 */
class NotificationsFactory implements FactoryInterface
{


    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return Notifications|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var SessionManager $sessionManager */
        $sessionManager = $container->get(SessionManager::class);

        $notificationsService = new Notifications($sessionManager);


        return $notificationsService;
    }
}

