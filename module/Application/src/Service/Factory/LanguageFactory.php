<?php
namespace Application\Service\Factory;

use Application\Model\UserDomain;
use Application\Service\Auth;
use Application\Service\DomainModelManager;
use Application\Service\Language;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Session\SessionManager;


/**
 * Class LanguageFactory
 * @package Application\Service\Factory
 */
class LanguageFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return Language|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
        /** @var SessionManager $sessionManager */
        $sessionManager = $container->get(SessionManager::class);

        $auth = $container->get(Auth::class);

        $domainManager = $container->get(DomainModelManager::class);
        $userDomain = $domainManager->get(UserDomain::class);

        $languageService = new Language($sessionManager, $config['translator']);
        $languageService->setAuth($auth);
        $languageService->setUserDomain($userDomain);

        return $languageService;
    }
}

