<?php
namespace Application\Service\Factory;

use Application\Model\BannerDomain;
use Application\Service\DomainModelManager;
use Application\Service\Order;
use Interop\Container\ContainerInterface;
use Mailing\Service\Mailing;
use Spipu\Html2Pdf\Html2Pdf;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\View\Renderer\PhpRenderer;


/**
 * Class OrderFactory
 * @package Application\Service\Factory
 */
class OrderFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return Order|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var DomainModelManager $domainManager */
        $domainManager = $container->get(DomainModelManager::class);
        $bannerDomain = $domainManager->get(BannerDomain::class);


        $mailing = $container->get(Mailing::class);

        $html2pdf = new Html2Pdf();

        /** @var PhpRenderer $viewRenderer */
        $viewRenderer = $container->get('Zend\View\Renderer\RendererInterface');

        $orderService = new Order();
        $orderService->setBannerDomain($bannerDomain);
        $orderService->setMailingService($mailing);
        $orderService->setHtml2pdf($html2pdf);
        $orderService->setViewRenderer($viewRenderer);

        $orderService->setViewRenderer($viewRenderer);

        // Instantiate the controller and inject dependencies
        return $orderService;
    }
}

