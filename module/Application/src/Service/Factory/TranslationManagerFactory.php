<?php
namespace Application\Service\Factory;

use Application\Service\TranslationManager;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class TranslationManagerFactory
 * @package Application\Service\Factory
 */
class TranslationManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|AuthenticationService
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');

        $translationManager = new TranslationManager($config['translation_manager']);
        return $translationManager;
    }
}

