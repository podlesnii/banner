<?php

namespace Application\Service\Factory;

use Acl\Model\RoleDomain;
use Application\Model\UserDomain;
use Interop\Container\ContainerInterface;
use Application\Service\Auth;
use Application\Service\DomainModelManager;
use Mailing\Service\Mailing;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Session\SessionManager;



/**
 * Class AuthServiceFactory
 * @package Application\Service\Factory
 */
class AuthServiceFactory implements FactoryInterface
{


    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return Auth|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $authenticationService = $container->get(AuthenticationService::class);

        $sessionManager = $container->get(SessionManager::class);

        $domainManager = $container->get(DomainModelManager::class);
        $userDomain = $domainManager->get(UserDomain::class);
        $roleDomain = $domainManager->get(RoleDomain::class);

        $mailingService = $container->get(Mailing::class);


        /** @var Auth $authService
         * @param $userDomain
         * @param $roleDomain
         */
        $authService = new Auth($authenticationService, $sessionManager, $mailingService);
        $authService->setUserDomain($userDomain);
        $authService->setRoleDomain($roleDomain);



        return $authService;
    }
}