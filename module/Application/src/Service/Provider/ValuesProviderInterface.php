<?php
/**
 * Values provider interface
 */

namespace Application\Service\Provider;

/**
 * Interface ValuesProviderInterface
 */
interface ValuesProviderInterface
{
    /**
     * Get form values
     * @return mixed
     */
    public function getFormValues();

    /**
     * Get default values
     * @return mixed
     */
    public function getDefaultValue();

    /**
     * Get value by key
     *
     * @param $key
     *
     * @return string
     */
    public function getValue($key);
}