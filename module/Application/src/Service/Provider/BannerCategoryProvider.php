<?php

namespace Application\Service\Provider;

use Application\Entity\BannerCategory;
use Application\Model\BannerCategoryDomain;

/**
 * Class BannerCategoryProvider
 * @package Application\Service\Provider
 */
class BannerCategoryProvider implements ValuesProviderInterface
{
    /**
     * @var BannerCategoryDomain
     */
    protected $domain;

    /**
     * Local variable, form values
     * @var array|null
     */
    protected $formValues = null;

    /**
     * @param BannerCategoryDomain $domain
     */
    public function __construct(BannerCategoryDomain $domain)
    {
        $this->domain = $domain;

        $formValues = array();
        $categories = $this->domain->findAll();

        /** @var BannerCategory $category */
        foreach ($categories as $category) {
            $formValues[$category->getId()] = $category->getName();
        }
        $this->formValues = $formValues;
    }

    /**
     * Get form values
     * @return array|null
     */
    public function getFormValues()
    {
        return $this->formValues;
    }

    /**
     * Get value
     *
     * @param $key
     *
     * @return string
     */
    public function getValue($key)
    {
        return $this->formValues[$key];
    }

    /**
     * Get default values
     * @return mixed
     */
    public function getDefaultValue()
    {
        return null;
    }
}