<?php

namespace Application\Service\Provider;

use Application\Entity\City;
use Application\Model\CityDomain;

/**
 * Class CityProvider
 * @package Application\Service\Provider
 */
class CityProvider implements ValuesProviderInterface
{
    /**
     * @var CityDomain
     */
    protected $domain;

    /**
     * Local variable, form values
     * @var array|null
     */
    protected $formValues = null;

    /**
     * @param CityDomain $domain
     */
    public function __construct(CityDomain $domain)
    {
        $this->domain = $domain;

        $formValues = array();
        $cities = $this->domain->findAll();

        /** @var City $city */
        foreach ($cities as $city) {
            $formValues[$city->getId()] = $city->getNameCity();
        }
        $this->formValues = $formValues;
    }

    /**
     * Get form values
     * @return array|null
     */
    public function getFormValues()
    {
        return $this->formValues;
    }

    /**
     * Get value
     *
     * @param $key
     *
     * @return string
     */
    public function getValue($key)
    {
        return $this->formValues[$key];
    }

    /**
     * Get default values
     * @return mixed
     */
    public function getDefaultValue()
    {
        return null;
    }
}