<?php

    namespace Application\Service\Provider\Factory;

    use Acl\Model\RoleDomain;
    use Application\Service\DomainModelManager;
    use Application\Service\Provider\RoleProvider;
    use Interop\Container\ContainerInterface;
    use Application\Service\AuthAdapter;
    use Zend\ServiceManager\Factory\FactoryInterface;


    /**
     * Class RoleProviderFactory
     * @package Application\Service\Provider\Factory
     */
    class RoleProviderFactory implements FactoryInterface
    {

        /**
         * @param ContainerInterface $container
         * @param string $requestedName
         * @param array|null $options
         *
         * @return AuthAdapter|object
         * @throws \Psr\Container\ContainerExceptionInterface
         * @throws \Psr\Container\NotFoundExceptionInterface
         */
        public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
        {
            $domainManager = $container->get(DomainModelManager::class);
            $roleDomain = $domainManager->get(RoleDomain::class);

            return new RoleProvider($roleDomain);
        }
    }
