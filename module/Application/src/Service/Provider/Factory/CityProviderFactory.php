<?php

namespace Application\Service\Provider\Factory;

use Application\Model\CityDomain;
use Application\Service\DomainModelManager;
use Application\Service\Provider\CityProvider;
use Interop\Container\ContainerInterface;
use Application\Service\AuthAdapter;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class CityProviderFactory
 * @package Application\Service\Provider\Factory
 */
class CityProviderFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     *
     * @return AuthAdapter|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $domainManager = $container->get(DomainModelManager::class);
        $categoryDomain = $domainManager->get(CityDomain::class);

        return new CityProvider($categoryDomain);
    }
}
