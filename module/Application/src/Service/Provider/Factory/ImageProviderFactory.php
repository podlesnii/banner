<?php

namespace Application\Service\Provider\Factory;

use Application\Model\ImageDomain;
use Application\Service\DomainModelManager;
use Application\Service\Provider\ImageProvider;
use Interop\Container\ContainerInterface;
use Application\Service\AuthAdapter;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class ImageProviderFactory
 * @package Application\Service\Provider\Factory
 */
class ImageProviderFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     *
     * @return AuthAdapter|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $domainManager = $container->get(DomainModelManager::class);
        $imageDomain = $domainManager->get(ImageDomain::class);

        return new ImageProvider($imageDomain);
    }
}
