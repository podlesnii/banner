<?php

namespace Application\Service\Provider\Factory;

use Application\Model\BannerCategoryDomain;
use Application\Service\DomainModelManager;
use Application\Service\Provider\BannerCategoryProvider;
use Interop\Container\ContainerInterface;
use Application\Service\AuthAdapter;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class BannerCategoryProviderFactory
 * @package Application\Service\Provider\Factory
 */
class BannerCategoryProviderFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     *
     * @return AuthAdapter|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $domainManager = $container->get(DomainModelManager::class);
        $categoryDomain = $domainManager->get(BannerCategoryDomain::class);

        return new BannerCategoryProvider($categoryDomain);
    }
}
