<?php

namespace Application\Service\Provider;

use Application\Entity\Image;
use Application\Model\ImageDomain;

/**
 * Class ImageProvider
 * @package Application\Service\Provider
 */
class ImageProvider implements ValuesProviderInterface
{
    /**
     * @var ImageDomain
     */
    protected $domain;

    /**
     * Local variable, form values
     * @var array|null
     */
    protected $formValues = null;

    /**
     * @param ImageDomain $domain
     */
    public function __construct(ImageDomain $domain)
    {
        $this->domain = $domain;

        $formValues = array();
        $images = $this->domain->findAll();

        /** @var Image $image */
        foreach ($images as $image) {
            $formValues[$image->getId()] = $image->getPath();
        }
        $this->formValues = $formValues;
    }

    /**
     * Get form values
     * @return array|null
     */
    public function getFormValues()
    {
        return $this->formValues;
    }

    /**
     * Get value
     *
     * @param $key
     *
     * @return string
     */
    public function getValue($key)
    {
        return $this->formValues[$key];
    }

    /**
     * Get default values
     * @return mixed
     */
    public function getDefaultValue()
    {
        return null;
    }
}