<?php

    namespace Application\Service\Provider;

    use Acl\Entity\Role;
    use Acl\Model\RoleDomain;

    /**
     * Class RoleProvider
     * @package Application\Service\Provider
     */
    class RoleProvider implements ValuesProviderInterface
    {
        /**
         * @var RoleDomain
         */
        protected $domain;

        /**
         * Local variable, form values
         * @var array|null
         */
        protected $formValues = null;

        /**
         * @param RoleDomain $domain
         */
        public function __construct(RoleDomain $domain)
        {
            $this->domain = $domain;

            $formValues = array();
            $roles = $this->domain->findAll();

            /** @var Role $role */
            foreach ($roles as $role) {
                $formValues[$role->getId()] = $role->getName();
            }
            $this->formValues = $formValues;
        }

        /**
         * Get form values
         * @return array|null
         */
        public function getFormValues()
        {
            return $this->formValues;
        }

        /**
         * Get value
         *
         * @param $key
         *
         * @return string
         */
        public function getValue($key)
        {
            return $this->formValues[$key];
        }

        /**
         * Get default values
         * @return mixed
         */
        public function getDefaultValue()
        {
            return null;
        }
    }