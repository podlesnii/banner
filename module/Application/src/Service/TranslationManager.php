<?php

namespace Application\Service;

use Zend\Config\Writer\PhpArray;


/**
 * Class TranslationManager
 * @package Application\Service
 */
class TranslationManager
{
    /**
     * Missing translations variable
     * @var array $missingTranslations
     */
    protected $missingTranslations;

    /**
     * Config variable
     * @var TranslationManager $config
     */
    protected $config;

    /**
     * TranslationManager constructor.
     *
     * @param $config
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * Standart php magic method
     */
    public function __destruct()
    {
        $configWriter = new PhpArray();

        foreach ($this->missingTranslations ?: array() as $locale => $messages) {
            $localesArray = [];
            foreach ($messages as $message) {
                $localesArray[$message] = '';
            }
            $configWriter->toFile($this->getFilePath($locale), $localesArray);
        }

    }


    /**
     * Collect missing translations
     *
     * @param $params
     */
    public function missingTranslation($params)
    {
        $message = trim($params['message']);
        $missingTranslations = $this->getMissingTranslations($params['locale']);
        if (!in_array($message, $missingTranslations)) {
            $this->missingTranslations[$params['locale']][] = $message;
        }
    }

    /**
     * Get file path
     *
     * @param $locale
     *
     * @return string
     */
    public function getFilePath($locale)
    {
        return $this->config['base_dir'] . DIRECTORY_SEPARATOR . sprintf($this->config['pattern'], $locale);
    }

    /**
     * Get missing translations
     *
     * @param $locale
     *
     * @return mixed
     */
    protected function getMissingTranslations($locale)
    {
        if (is_null($this->missingTranslations[$locale])) {
            $filePath = $this->getFilePath($locale);
            if (file_exists($filePath)) {
                $missingTranslations = require $filePath;
                foreach ($missingTranslations ?: [] as $key => $value) {
                    $this->missingTranslations[$locale][] = $key;
                }
            } else {
                $this->missingTranslations[$locale] = array();
                if (!file_exists($this->config['base_dir'])) {
                    mkdir($this->config['base_dir'], '0777', true);
                }
            }
        }

        return $this->missingTranslations[$locale];
    }
}