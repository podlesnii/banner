<?php

namespace Application\Service;

use Zend\Session\Container;
use Zend\Session\SessionManager;


/**
 * Class Notifications
 *
 * @package Application\Service
 */
class Notifications
{

    /** @var SessionManager */
    protected $sessionManager;

    /** @var Container */
    protected $notificationsSessionContainer;


    /**
     * Notifications constructor.
     *
     * @param $sessionManager
     */
    public function __construct($sessionManager)
    {
        $this->sessionManager = $sessionManager;

    }


    /**
     * @param $parameters
     */
    public function addNotificationMessage($parameters)
    {
        $this->notificationsSessionContainer = new Container('Notifications', $this->sessionManager);
        $message = [
                'type'        => $parameters['type'],
                'message'     => $parameters['message'],
        ];
        $notifications = $this->notificationsSessionContainer->notifications;
        $alreadyExist = false;

        if ($notifications != null){
        foreach ($notifications as $notification) {
            if ($notification['type'] === $message['type'] && $notification['message'] === $message['message']) {
                $alreadyExist = true;
            }
        }
        }
        if(!$alreadyExist) {
            $notifications[] = $message;
            $this->notificationsSessionContainer->notifications = $notifications;
        }
    }


    /**
     * @return array
     */
    public function getMessages()
    {

        $notifications = $_SESSION['Notifications']['notifications'];

        $showNowMessages = [];
        foreach ($notifications?:[] as $key => $message) {
                $showNowMessages[] = $message;
        }
        if ($notifications != null) {
            $this->notificationsSessionContainer->notifications = $notifications;
        }

        return $showNowMessages;
    }




}