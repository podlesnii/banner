<?php

namespace Application\Form;

use Application\Service\Provider\BannerCategoryProvider;
use Application\Service\Provider\CityProvider;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;


/**
 * Class SearchForm
 * @package Application\Form
 */
class SearchForm extends Form
{

    /** @var BannerCategoryProvider */
    protected $categoryProvider;

    /** @var CityProvider */
    protected $cityProvider;

    /**
     * Constructor
     *
     * @param BannerCategoryProvider $categoryProvider
     * @param CityProvider $cityProvider
     */
    public function __construct(BannerCategoryProvider $categoryProvider, CityProvider $cityProvider)
    {
        $this->categoryProvider = $categoryProvider;
        $this->cityProvider = $cityProvider;

        // Define form name
        parent::__construct('search-form');

        // Set POST method for this form
        $this->setAttribute('method', 'get');

        $this->addElements();
        $this->addInputFilter();
    }


    /**
     *
     */
    protected function addElements()
    {

        // Add "category" field
        $categoryField = [
            'type' => 'select',
            'name' => 'category',
            'options' => [
                'label' => 'Category',
            ],
        ];
        $categoryField['options']['value_options'] = $this->categoryProvider->getFormValues();
        $categoryField['attributes']['value'] = $this->categoryProvider->getDefaultValue();

        $this->add($categoryField);

        // Add "city" field
        $cityField = [
            'type' => 'select',
            'name' => 'city',
            'options' => [
                'label' => 'City',
            ],
        ];
        $cityField['options']['value_options'] = $this->cityProvider->getFormValues();
        $cityField['attributes']['value'] = $this->cityProvider->getDefaultValue();
        $this->add($cityField);


        // Add the submit button
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
            ],
        ]);

    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);


        $inputFilter->add([
            'name' => 'search',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);

        $inputFilter->add([
            'name' => 'select',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);

    }

}