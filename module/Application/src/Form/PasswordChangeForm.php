<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;


/**
 * Class PasswordChangeForm
 * @package Application\Form
 */
class PasswordChangeForm extends Form
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('passwordChange-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {

          // Add "email" field
        $this->add([
            'type' => 'password',
            'name' => 'new_password',
            'options' => [
                'label' => 'New password',
            ],
        ]);

        // Add "confirm_new_password" field
        $this->add([
                       'type'  => 'password',
                       'name' => 'confirm_new_password',
                       'attributes' => [
                           'id' => 'confirm_new_password'
                       ],
                       'options' => [
                           'label' => 'Confirm new password',
                       ],
                   ]);

        // Add "current_password" field
        $this->add([
            'type'  => 'password',
            'name' => 'password',
            'attributes' => [
                'id' => 'password'
            ],
            'options' => [
                'label' => 'Password',
            ],
        ]);


        // Add the submit button
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
            ],
        ]);
    }
    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        // Add input for "new_password" field
        $inputFilter->add([
                              'name'     => 'new_password',
                              'required' => true,
                              'filters'  => [
                              ],
                              'validators' => [
                                  [
                                      'name'    => 'StringLength',
                                      'options' => [
                                          'min' => 1,
                                          'max' => 64
                                      ],
                                  ],
                              ],
                          ]);

        // Add input for "confirm_new_password" field
        $inputFilter->add([
                              'name'     => 'confirm_new_password',
                              'required' => true,
                              'filters'  => [
                              ],
                              'validators' => [
                                  [
                                      'name'    => 'Identical',
                                      'options' => [
                                          'token' => 'new_password',
                                      ],
                                  ],
                              ],
                          ]);

        // Add input for "password" field
        $inputFilter->add([
                'name'     => 'password',
                'required' => true,
                'filters'  => [
                ],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 6,
                            'max' => 64
                        ],
                    ],
                ],
            ]);

    }
}

