<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;


/**
 * Class ForgotPasswordForm
 * @package Application\Form
 */
class ForgotPasswordForm extends Form
{
    /**
     * Constructor.     
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('forgot-password-form');
     
        // Set POST method for this form
        $this->setAttribute('method', 'post');
                
        $this->addElements();
        $this->addInputFilter();          
    }
    
    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() 
    {
        // Add "login" field
        $this->add([            
            'type'  => 'text',
            'name' => 'login_or_email',
            'options' => [
                'label' => 'Your Login or Email',
            ],
        ]);


        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [                
                'value' => 'next',
                'id' => 'submit',
            ],
        ]);
    }
    
    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter() 
    {
        // Create main input filter
        $inputFilter = new InputFilter();        
        $this->setInputFilter($inputFilter);
                
        // Add input for "login" field
        $inputFilter->add([
                'name'     => 'login_or_email',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],                    
                ],                
            ]);
       }
}

