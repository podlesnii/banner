<?php

    namespace Application\Form\Factory;

    use Application\Form\SearchForm;
    use Application\Service\Provider\BannerCategoryProvider;
    use Application\Service\Provider\CityProvider;
    use Interop\Container\ContainerInterface;
    use Zend\ServiceManager\Factory\FactoryInterface;


    /**
     * Class SearchFormFactory
     * @package Application\Form\Factory
     */
    class SearchFormFactory implements FactoryInterface
    {


        /**
         * @param ContainerInterface $container
         * @param $requestedName
         * @param array|null $options
         * @return SearchForm
         * @throws \Psr\Container\ContainerExceptionInterface
         * @throws \Psr\Container\NotFoundExceptionInterface
         */
        public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
        {
            $bannerCategoryProvider = $container->get(BannerCategoryProvider::class);
            $cityProvider = $container->get(CityProvider::class);

            $form = new SearchForm($bannerCategoryProvider, $cityProvider);

            return $form;
        }
    }