<?php

namespace Application\Model;

use Application\Entity\BannerCategory;
use Doctrine\ORM\Query;

/**
 * Class BannerCategoryDomain
 * @package Application\Model
 */
class BannerCategoryDomain extends DomainModelAbstract
{
    protected $entityName = BannerCategory::class;

    /**
     * @param null $filter
     * @return array
     */
    public function getBannerCategories($filter = NULL)
    {
        $query = $this->getBannerCategory($filter);
        return $query->execute();
    }

    /**
     * @param null $filter
     * @return Query
     */
    public function getBannerCategory ($filter = NULL)
    {

        $qb = $this->getRepository()->createQueryBuilder('bannerCategories');


        if (count($filter)) {
            foreach ($filter as $name => $value) {
                if ($name == 'category') {
                    if (is_array($value)) {
                        $qb->andWhere('bannerCategories.name IN :name')
                            ->setParameter('name', $value);
                    }
                    else {
                        $qb->andWhere('bannerCategories.name = :name')
                            ->setParameter('name', $value);

                    }
                }
            }

        }



        $query = $qb->getQuery();
        $query->setHydrationMode(Query::HYDRATE_ARRAY);

        return $query;
    }


    /**
     * @param $data
     * @return BannerCategory
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addBannerCategory($data)
    {
        $bannerCategory = new BannerCategory();

        $bannerCategory->fromArray($data);

        $this->persist($bannerCategory);
        $this->flush();

        return $bannerCategory;
    }


    /**
     * @param BannerCategory $bannerCategory
     * @param $data
     *
     * @return BannerCategory
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateBannerCategory($bannerCategory, $data)
    {
        $bannerCategory->fromArray($data);

        $this->persist($bannerCategory);
        $this->flush();

        return $bannerCategory;
    }


    /**
     * @param $bannerCategory
     *
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteBannerCategory($bannerCategory)
    {
        $this->entityManager->remove($bannerCategory);
        $this->flush();

        return true;
    }


}