<?php

namespace Application\Model;

use Application\Entity\City;
use Doctrine\ORM\Query;


/**
 * Class CityDomain
 * @package Application\Model
 */
class CityDomain extends DomainModelAbstract
{

    protected $entityName = City::class;


    /**
     * @param null $filter
     * @return array
     */
    public function getCities($filter = NULL)
    {
        $query = $this->getCityQuery($filter);
        return $query->execute();
    }

    /**
     * @param null $filter
     * @return Query
     */
    public function getCityQuery($filter = NULL)
    {
        $qb = $this->getRepository()->createQueryBuilder('city');

        if (count($filter)) {
            foreach ($filter as $name => $value) {
                if ($name == 'nameCity') {
                    if (is_array($value)) {
                        $qb->andWhere('city.nameCity IN :nameCity')
                           ->setParameter('nameCity', $value);
                    }
                    else {
                        $qb->andWhere('city.nameCity = :nameCity')
                           ->setParameter('nameCity', $value);

                    }
                }
            }

        }

        $query = $qb->getQuery();
        $query->setHydrationMode(Query::HYDRATE_ARRAY);

        return $query;
    }


    /**
     * @param $data
     * @return City
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addCity($data)
    {
        $city = new City();

        $data['indexnameCity']=str_replace(' ', '_',$data['indexnameCity']);
        $data['indexnameCity']=str_replace('.', '_',$data['indexnameCity']);
        $data['indexnameRegion']=str_replace('.', '_',$data['indexnameRegion']);
        $data['indexnameRegion']=str_replace(' ', '_',$data['indexnameRegion']);

        $city->setNameCity($data['nameCity']);
        $city->setNameRegion($data['nameRegion']);
        $city->setIndexnameCity(strtolower($data['indexnameCity']));
        $city->setIndexnameRegion(strtolower($data['indexnameRegion']));

        $this->persist($city);
        $this->flush();

        return $city;
    }

    /**
     * @param City $city
     * @param $data
     * @return City
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateCity($city, $data)
    {
        $data['indexnameCity']=str_replace(' ', '_',$data['indexnameCity']);
        $data['indexnameCity']=str_replace('.', '_',$data['indexnameCity']);
        $data['indexnameRegion']=str_replace('.', '_',$data['indexnameRegion']);
        $data['indexnameRegion']=str_replace(' ', '_',$data['indexnameRegion']);

        $city->setNameCity($data['nameCity']);
        $city->setNameRegion($data['nameRegion']);
        $city->setIndexnameCity(strtolower($data['indexnameCity']));
        $city->setIndexnameRegion(strtolower($data['indexnameRegion']));

        $this->persist($city);
        $this->flush();

        return $city;
    }

    /**
     * @param $city
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteCity($city)
    {
        $this->entityManager->remove($city);
        $this->flush();

        return true;
    }


}