<?php

namespace Application\Model;

use Application\Entity\News;
use Doctrine\ORM\Query;


/**
 * Class NewsDomain
 * @package Application\Model
 */
class NewsDomain extends DomainModelAbstract
{

    protected $entityName = News::class;

    /**
     * @param $filter
     * @return \Doctrine\ORM\Query
     */
    public function getNews($filter = null)
    {
        $qb = $this->getRepository()->createQueryBuilder('news');

        if (!empty($filter['id'])) {
            $qb->andwhere('news.id = :id')
                ->setParameter('id', $filter['id']);
        }


        $query = $qb->getQuery();
        $query->setHydrationMode(Query::HYDRATE_ARRAY);


        return $query;
    }

    /**
     * @param $data
     * @return News
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addNews($data)
    {
        $news = new News();

        $news->setTitle($data['title']);
        $news->setArticle($data['article']);
        $news->setImage($data['image']);
        $news->setActive($data['active']);

        $this->persist($news);
        $this->flush();

        return $news;
    }

    /**
     * @param News $news
     * @param $data
     * @return News
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateNews($news, $data)
    {

        $news->setTitle($data['title']);
        $news->setArticle($data['article']);
        $news->setImage($data['image']);
        $news->setActive($data['active']);

        $this->persist($news);
        $this->flush();

        return $news;
    }

    /**
     * @param $news
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteNews($news)
    {
        $this->entityManager->remove($news);
        $this->flush();

        return true;
    }


}
