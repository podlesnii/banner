<?php

namespace Application\Model;

use Application\Entity\Invoice;
use Doctrine\ORM\Query;


/**
 * Class InvoiceDomain
 * @package Application\Model
 */
class InvoiceDomain extends DomainModelAbstract
{

    protected $entityName = Invoice::class;

    /**
     * @return Query
     */
    public function getInvoice($filter)
    {

        $qb = $this->getRepository()->createQueryBuilder('invoice');
        $qb->addSelect('invoice', 'owner')
            //->setParameter(['owner']['id'], [1])
           ->leftJoin('invoice.owner', 'owner');

        if ($filter['owner_id'] != null) {
            $qb->andwhere('invoice.owner = :owner_id')
                ->setParameter('owner_id', $filter['owner_id']);
        }

        $query = $qb->getQuery();
        $query->setHydrationMode(Query::HYDRATE_ARRAY);

        $invoiceData = $query->execute();

        return $invoiceData;
    }

    /**
     * @param $data
     * @param $userId
     *
     * @return Invoice
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addInvoice($data, $userId)
    {
        $invoice = new Invoice();

        $invoice->setAccount($data['account']);
        $invoice->setAdress($data['adress']);
        $invoice->setBank($data['bank']);
        $invoice->setBic($data['bic']);
        $invoice->setCompany($data['company']);
        $invoice->setIban($data['iban']);
        $invoice->setSwift($data['swift']);
        $invoice->setIdno($data['idno']);
        $invoice->addOwner($this->entityManager->getReference(Invoice::class, $userId));


        $this->persist($invoice);
        $this->flush();

        return $invoice;
    }


    /**
     * @param Invoice $invoice
     * @param $data
     *
     * @return Invoice
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateInvoice($invoice, $data)
    {
        $invoice->setAccount($data['account']);
        $invoice->setAdress($data['adress']);
        $invoice->setBank($data['bank']);
        $invoice->setBic($data['bic']);
        $invoice->setCompany($data['company']);
        $invoice->setIban($data['iban']);
        $invoice->setSwift($data['swift']);
        $invoice->setIdno($data['idno']);

        $this->persist($invoice);
        $this->flush();

        return $invoice;
    }


}