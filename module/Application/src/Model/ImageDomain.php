<?php

namespace Application\Model;

use Application\Entity\Image;
use Doctrine\ORM\Query;


/**
 * Class ImageDomain
 * @package Application\Model
 */
class ImageDomain extends DomainModelAbstract
{

    protected $entityName = Image::class;


    /**
     * @param null $filter
     * @return Query
     */
    public function getImage($filter = null)
    {
        $qb = $this->getRepository()->createQueryBuilder('image');
        $qb->addSelect('image');

        if ($filter['id'] != null) {
            $qb->andwhere('image.id = :id')
                ->setParameter('id', $filter['id']);
        }

        if ($filter['path'] != null) {
            $qb->andwhere('image.path = :path')
                ->setParameter('path', $filter['path']);
        }

        return $qb->getQuery();

    }

    /**
     * @param $banner
     * @param $path
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addImage($banner, $path)
    {
        $image = new Image();
        $image->setPath($path);
        $image->setBanner($banner);

        $this->persist($image);
        $this->flush();

        return $image;
    }


    /**
     * @param Image $image
     * @param $data
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateImage($image, $data)
    {
        $image->setPath('/img/banner-photo/' . $data);

        $this->persist($image);
        $this->flush();

        return $image;
    }


    /**
     * @param $banner
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteImages($banner)
    {
        $images = $banner;
        foreach ($images as $image) {
            $this->entityManager->remove($image);
            $this->flush();
        }
        return true;

    }


}