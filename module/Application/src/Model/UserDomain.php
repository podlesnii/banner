<?php

namespace Application\Model;

use Acl\Entity\Role;
use Acl\Model\RoleDomain;
use Application\Entity\User;
use Application\Service\Auth;
use Doctrine\ORM\Query;
use Zend\Crypt\Password\Bcrypt;



/**
 * Class UserDomain
 * @package Application\Model
 */
class UserDomain extends DomainModelAbstract
{

    protected $entityName = User::class;

    /**
     * @param $filter
     *
     * @return Query
     */
    public function getUsers($filter)
    {

        $qb = $this->getRepository()->createQueryBuilder('user');
        $qb->addSelect('user', 'role')
            ->leftJoin('user.role', 'role');

        if ($filter['id'] != null) {
            $qb->andwhere('user.id = :id')
                ->setParameter('id', $filter['id']);
        }
        $qb->groupBy('user.id');

        $query = $qb->getQuery();
        $query->setHydrationMode(Query::HYDRATE_ARRAY);

        return $query;
    }

    /**
     * @param $data
     *
     * @return User
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addUser($data)
    {
        $user = new User();

        $user->setIcon('/img/user_default_icon.png');
        $user->setEmail($data['email']);
        $user->setName($data['name']);
        $user->setLogin($data['login']);
        $user->setActive('0');
        $user->setLanguage('ro_RO');



        $bcrypt = new Bcrypt();
        $data['password'] = $bcrypt->create($data['password']);
        $user->setPassword($data['password']);

        $role = $data['role'];
        $user->addRole($this->entityManager->getReference(Role::class, $role));


        $this->persist($user);
        $this->flush();

        return $user;
    }

    /**
     * @param User $user
     * @param $data
     *
     * @return User
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateUser($user, $data)
    {
        $user->setEmail($data['email']);
        $user->setName($data['name']);
        $user->setLogin($data['login']);

        $bcrypt = new Bcrypt();
        $data['password'] = $bcrypt->create($data['password']);
        $user->setPassword($data['password']);

        if (!is_null($data['role']) && $data['role'] != '') {
            $role = $data['role'];
            $user->addRole($this->entityManager->getReference(Role::class, $role));
        } else {
            $role = $data['role'];
            $user->addRole($this->entityManager->getReference(Role::class, $role));
        }

        if (!is_null($data['locale'])){
            $user->setLanguage($data['locale']);
             }

        $this->persist($user);
        $this->flush();

        return $user;
    }


    /**
     * @param User $user
     * @param $data
     *
     * @return User
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateUserEmail($user, $data)
    {
        $user->setEmail($data['email']);

        $this->persist($user);
        $this->flush();

        return $user;
    }

    /**
     * @param User $user
     * @param $locale
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateUserLanguage($user, $locale)
    {
        $user->setLanguage($locale);

        $this->persist($user);
        $this->flush();

        return $user;
    }


    /**
     * @param User $user
     * @param $data
     *
     * @return User
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateUserName($user, $data)
    {
        $user->setName($data['name']);

        $this->persist($user);
        $this->flush();

        return $user;
    }

    /**
     * @param User $user
     * @param $data
     *
     * @return User
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateUserPassword($user, $data)
    {

        $bcrypt = new Bcrypt();
        $data['new_password'] = $bcrypt->create($data['new_password']);
        $user->setPassword($data['new_password']);

        $this->persist($user);
        $this->flush();

        return $user;
    }


    /**
     * @param User $user
     *
     * @return User
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function activateUser($user)
    {
        $user->setActive(true);

        $this->persist($user);
        $this->flush();

        return $user;
    }


    /**
     * @param $user
     *
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteUser($user)
    {
        $this->entityManager->remove($user);
        $this->flush();

        return true;
    }

    /**
     * @param null $user
     * @param null $token
     *
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function resetPassword($user = null, $token = null)
    {
        if ($user==null) {
            $user = $this->authManager->getIdentity();
        }
        elseif (is_string($user)) {
            $user = $this->findOneBy(array(
                'email' => $user
            ));
            if (!$user) {
                return false;
            }
        }


        if (!is_null($token)) {
            $userToken = $this->credentialsService->calculateUserToken($user);
            if ($userToken != $token) {
                return false;
            }
        }
        $newCredentials = $this->credentialsService->generateCredentials();
        $this->emailService->sendResetPasswordEmail($user, $newCredentials['password']);
        $user->setPassword($this->credentialsService->encryptPassword($newCredentials['password']));
        $this->flush();
        return true;
    }


    /**
     * @param $id Auth
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findUserWithRole($id)
    {

        $qb = $this->getRepository()->createQueryBuilder('user');
        $qb->where('user.id = :id')
                ->setParameter('id', $id);
        $qb->addSelect('user', 'role')
                ->leftJoin('user.role', 'role');


        $query = $qb->getQuery()->getOneOrNullResult();

        return $query;
    }


}