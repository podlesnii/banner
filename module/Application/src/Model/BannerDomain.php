<?php

namespace Application\Model;


use Application\Entity\Banner;
use Application\Entity\BannerCategory;
use Application\Entity\Image;
use Application\Entity\User;
use Application\Entity\BannerStatus;
use Application\Entity\City;
use Doctrine\ORM\Query;
use Zend\Form\Element\DateTime;


/**
 * Class BannerDomain
 * @package Application\Model
 */
class BannerDomain extends DomainModelAbstract
{

    protected $entityName = Banner::class;

    protected $image = Image::class;

    protected $imageDomain = ImageDomain::class;

    /**
     * @param $filter
     *
     * @param null|string $hydrationMode
     *
     * @return \Doctrine\ORM\Query
     */
    public function getBanners($filter, $hydrationMode = NULL)
    {

        $qb = $this->getRepository()->createQueryBuilder('banners');
        $dateTime = new \DateTime('now');

        $qb->addSelect('banners', 'category', 'city', 'images', 'bannerStatuses', 'bannerRatings')
            ->leftJoin('banners.category', 'category')
            ->leftJoin('banners.city', 'city')
            ->leftJoin('banners.images', 'images')
            ->leftJoin('banners.bannerRatings', 'bannerRatings')
            ->leftJoin('banners.bannerStatuses', 'bannerStatuses', Query\Expr\Join::WITH,
                'bannerStatuses.timeStart < :timeStart AND
                            bannerStatuses.timeEnd > :timeEnd')
            ->addOrderBy('banners.city')
            ->addOrderBy('banners.area');
        $qb->setParameter('timeStart', $dateTime);
        $qb->setParameter('timeEnd', $dateTime);


        if ($filter['city'] != null) {
            $qb->where('banners.city = :city')
                ->setParameter('city', $filter['city']);
        }
        if ($filter['category'] != null) {
            $qb->andwhere('banners.category = :category')
                ->setParameter('category', $filter['category']);
        }
        if ($filter['id'] != null) {
            $qb->andwhere('banners.id = :id')
                ->setParameter('id', $filter['id']);
        }
        if ($filter['currentUserRole'] !== NULL
            AND $filter['currentUserRole'] !== 'Administrator') {
            $qb->andwhere('banners.userId = :userId')
                ->setParameter('userId', $filter['currentUserId']);
        }

        $query = $qb->getQuery();

        if (is_null($hydrationMode)) {
            $hydrationMode = Query::HYDRATE_ARRAY;
        }
        $query->setHydrationMode($hydrationMode);

        return $query;
    }


    /**
     * @param $data
     * @param $currentUserId
     *
     * @return Banner
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addBanner($data, $currentUserId)
    {
        $banners = new Banner();

        $banners->setArea($data['area']);
        $banners->setDescription($data['description']);
        $banners->setBannerNumber($data['bannerNumber']);

        if (!is_null($currentUserId) && $currentUserId != '') {
            $banners->addUserId($this->entityManager->getReference(User::class, $currentUserId));
        }
        if (!is_null($data['city']) && $data['city'] != '') {
            $city = $data['city'];
            $banners->addCity($this->entityManager->getReference(City::class, $city));
        }

        if (!is_null($data['category']) && $data['category'] != '') {
            $category = $data['category'];
            $banners->addCategory($this->entityManager->getReference(BannerCategory::class, $category));
        }

        if (!is_null($data['adress'] && $data['adress'] != '')) {
            $banners->setAdress($data['adress']);
        }

        if (!is_null($data['latitude'] && $data['latitude'] != '')) {
            $banners->setLatitude($data['latitude']);
        }

        if (!is_null($data['longitude'] && $data['longitude'] != '')) {
            $banners->setLongitude($data['longitude']);
        }


        $this->persist($banners);
        $this->flush();

        return $banners;
    }

    /**
     * @param $banners
     * @param $currentUserId
     * @return Banner
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addManyBanners($banners, $currentUserId)
    {
        if ($banners != null) {
            foreach ($banners as $banner) {

                $newBanner = new Banner();
                $newBanner->setBannerNumber($banner['banner_number']);
                $newBanner->setDescription($banner['description']);
                $newBanner->setArea($banner['area']);
                $newBanner->setAdress($banner['adress']);
                $newBanner->setPrice($banner['price']);

                if (!is_null($banner['longitude']) && $banner['longitude'] != '') {
                    $newBanner->setLongitude($banner['longitude']);
                }

                if (!is_null($banner['latitude']) && $banner['latitude'] != '') {
                    $newBanner->setLatitude($banner['latitude']);
                }

                if (!is_null($currentUserId) && $currentUserId != '') {
                    $newBanner->addUserId($this->entityManager->getReference(User::class, $currentUserId));
                }

                if (!is_null($banner['city_id']) && $banner['city_id'] != '') {
                    $newBanner->addCity($this->entityManager->getReference(City::class, $banner['city_id']));
                }

                if (!is_null($banner['category']) && $banner['category_id'] != '') {
                    $newBanner->addCategory($this->entityManager->getReference(BannerCategory::class, $banner['category_id']));
                }

                $this->persist($newBanner);
                $this->flush();

            }
        }

        return $banners;
    }


    /**
     * @param Banner $banner
     * @param $data
     *
     * @return Banner
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateBanner($banner, $data)
    {
        $banner->setArea($data['area']);
        $banner->setAdress($data['adress']);
        $banner->setDescription($data['description']);

        if (!is_null($data['city']) && $data['city'] != '') {
            $city = $data['city'];
            $banner->addCity($this->entityManager->getReference(City::class, $city));
        }

        if (!is_null($data['category']) && $data['category'] != '') {
            $category = $data['category'];
            $banner->addCategory($this->entityManager->getReference(BannerCategory::class, $category));
        }

//        if ($image1) {
//            $banner->addImage($this->entityManager->getReference(Image::class, $image1));
//        }

        $this->persist($banner);
        $this->flush();

        return $banner;
    }

    /**
     * @param $banner
     * @param $image
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addBannerImage($banner, $image)
    {
        $imageEntity = new Image();
        $imageEntity->setPath($image['path']);
        $imageEntity->setTitle($image['title']);
        $imageEntity->setBanner($banner);

        $this->persist($imageEntity);
        $this->flush();
    }

    /**
     * @param Banner $banner
     * @param $status
     *
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addBannerStatus($banner, $status)
    {
        $timeStart = new \DateTime();

        $oldStatus = $this->getCurrentStatus($banner, $timeStart);
        if ($oldStatus != NULL) {
            $oldStatus->setTimeEnd($timeStart);
        }

        $bannerStatus = new BannerStatus();
        $bannerStatus->setStatus($status);
        $bannerStatus->setBanner($banner);
        $bannerStatus->setTimeEnd(new \DateTime('now +10 day'));
        $bannerStatus->setTimeStart($timeStart);

        $this->persist($bannerStatus);

        $banner->addBannerStatuses($bannerStatus);

        $this->persist($banner);
        $this->flush();

        return $bannerStatus;
    }


    /**
     * @param Banner banner
     *
     * @return BannerStatus
     */
    protected function getCurrentStatus($banner, $timeStart)
    {
        return null;
    }


    /**
     * @param $banner
     *
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteBanner($banner)
    {
        $this->entityManager->remove($banner);
        $this->flush();

        return true;
    }


}