<?php

namespace Application\Model;

use Application\Entity\BannerStatus;
use DateTime;
use Doctrine\ORM\Query;


/**
 * Class BannerStatusDomain
 * @package Application\Model
 */
class BannerStatusDomain extends DomainModelAbstract
{

    protected $entityName = BannerStatus::class;


    /**
     * @param $currentUser
     * @return \Doctrine\ORM\Query
     */
    public function getBannerStatuses($currentUser)
    {
        $userId = $currentUser->getId();

        $qb = $this->getRepository()->createQueryBuilder('bannersStatus');
        $qb->addSelect('bannersStatus', 'banner', 'city');
        $qb->leftJoin('bannersStatus.banner', 'banner');
        $qb->leftJoin('banner.city', 'city');
        $qb->addOrderBy('bannersStatus.banner');
        $qb->addOrderBy('bannersStatus.timeEnd');

        if ($currentUser->getRole()->getName() !== 'Administrator') {
            $qb->andwhere('banner.userId = :userId')
                ->setParameter('userId', $userId);
        }

        $query = $qb->getQuery();
        $query->setHydrationMode(Query::HYDRATE_ARRAY);

        return $query;
    }


    /**
     * @param BannerStatus $bannerStatus
     * @param $data
     *
     * @return BannerStatus
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateBannerStatus($bannerStatus, $data)
    {
        $bannerStatus->setStatus($data['status']);
        $bannerStatus->setTimeStart(new \DateTime($data['timeStart']));
        $bannerStatus->setTimeEnd(new \DateTime($data['timeEnd']));

        $this->persist($bannerStatus);
        $this->flush();

        return $bannerStatus;
    }



    /**
     * @param $bannerStatus
     *
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteBannerStatus($bannerStatus)
    {
        $this->entityManager->remove($bannerStatus);
        $this->flush();

        return true;
    }

}