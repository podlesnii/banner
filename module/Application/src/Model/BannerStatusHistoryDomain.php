<?php

namespace Application\Model;

use Application\Entity\Banner;
use Application\Entity\BannerStatusHistory;
use DateTime;
use Doctrine\ORM\Query;


/**
 * Class BannerStatusHistoryDomain
 * @package Application\Model
 */
class BannerStatusHistoryDomain extends DomainModelAbstract
{

    protected $entityName = BannerStatusHistory::class;


    /**
     * @param $currentUser
     * @return \Doctrine\ORM\Query
     */
    public function getBannerStatusesHistory($currentUser)
    {
        $userId = $currentUser->getId();

        $qb = $this->getRepository()->createQueryBuilder('bannersStatusHistory');
        $qb->addSelect('bannersStatusHistory', 'banner', 'city');
        $qb->leftJoin('bannersStatusHistory.banner', 'banner');
        $qb->leftJoin('banner.city', 'city');
        $qb->addOrderBy('bannersStatusHistory.banner');
        $qb->addOrderBy('bannersStatusHistory.timeEnd');

        if ($currentUser->getRole()->getName() !== 'Administrator') {
            $qb->andwhere('banner.userId = :userId')
                ->setParameter('userId', $userId);
        }

        $query = $qb->getQuery();
        $query->setHydrationMode(Query::HYDRATE_ARRAY);

        return $query;
    }


    /**
     * @param $bannerId
     * @param $status
     * @return BannerStatusHistory
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addBannerStatusHistory($bannerId, $status)
    {
        $currentTime = new \DateTime('now');
        $bannerStatusHistory= new BannerStatusHistory();

        $bannerStatusHistory->setStatus($status['status']);
        $bannerStatusHistory->setTimeStart(new \DateTime($status['timeStart']));
        $bannerStatusHistory->setTimeEnd(new \DateTime($status['timeEnd']));
        $bannerStatusHistory->setTimeChanged($currentTime);
        $bannerStatusHistory->addBanner($this->entityManager->getReference(Banner::class, $bannerId));

        $this->persist($bannerStatusHistory);
        $this->flush();

        return $bannerStatusHistory;
    }


    /**
     * @param $bannerStatus
     *
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteBannerStatusHistory($bannerStatus)
    {
        $this->entityManager->remove($bannerStatus);
        $this->flush();

        return true;
    }

}