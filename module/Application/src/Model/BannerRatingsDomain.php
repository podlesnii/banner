<?php

namespace Application\Model;



use Application\Entity\BannerRatings;

/**
 * Class BannerRatingsDomain
 * @package Application\Model
 */
class BannerRatingsDomain extends DomainModelAbstract
{

    protected $entityName = BannerRatings::class;

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getBannerRatings()
    {

        $query = $this->getRepository()->createQueryBuilder('banner_ratings');
        $query->addSelect('*');

        return $query;
    }




}