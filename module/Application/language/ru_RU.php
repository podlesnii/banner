<?php

return [
        'Billboard.md'                                         => 'Billboard.md',
        'Please sign in'                                       => 'Вход',
        'or be classical'                                      => 'или заполните',
        'CREATE ACCOUNT'                                       => 'Регистрация',
        'Log In / Registration'                                => 'Вход / Регистрация',
        'Login'                                                => 'Логин',
        'Login2'                                               => 'Вход',
        'Login_adminlte'                                       => 'Логин',
        'Registration'                                         => 'Регистрация',
        'Mobile App'                                           => 'Приложение',
        'Banner'                                               => 'Баннер',
        'Banners'                                              => 'Баннеры',
        'Search'                                               => 'Поиск',
        'News'                                                 => 'Новости',
        'Contact'                                              => 'Контакты',
        'City'                                                 => 'Город',
        'Chisinau'                                             => 'Кишинев',
        'Balti'                                                => 'Бельцы',
        'Orhei'                                                => 'Оргеев',
        'Cricova'                                              => 'Криково',
        'Submit'                                               => 'Подтвердить',
        'Your Login'                                           => 'Ваш логин',
        'Password'                                             => 'Пароль',
        'login'                                                => 'логин',
        'Name'                                                 => 'Имя',
        'E-mail'                                               => 'Е-мэйл',
        'Repeat password'                                      => 'Повторите пароль',
        'Tiraspol'                                             => 'Тирасполь',
        'Tighina'                                              => 'Тигина',
        'Ribnita'                                              => 'Рыбница',
        'Ungheni'                                              => 'Унгены',
        'Cahul'                                                => 'Кагул',
        'Soroca'                                               => 'Сороки',
        'Comrat'                                               => 'Комрат',
        'Dubasari'                                             => 'Дубоссары',
        'Straseni'                                             => 'Страшены',
        'Durlesti'                                             => 'Дурлешты',
        'Ceadar-Lunga'                                         => 'Чадыр-Лунга',
        'Causeni'                                              => 'Каушаны',
        'Codru'                                                => 'Кодры',
        'Edinet'                                               => 'Единцы',
        'Drochia'                                              => 'Дрокия',
        'Slobozia'                                             => 'Слободзея',
        'Ialoveni'                                             => 'Яловены',
        'Hancesti'                                             => 'Хынчешты',
        'Sangerei'                                             => 'Сынжерей',
        'Taraclia'                                             => 'Тараклия',
        'Vulcanesti'                                           => 'Вулканешты',
        'Falesti'                                              => 'Фалешты',
        'Floresti'                                             => 'Флорешты',
        'Cimislia'                                             => 'Чимишлия',
        'Rezina'                                               => 'Резина',
        'Calarasi'                                             => 'Калараш',
        'Dnestrovsc'                                           => 'Днестровск',
        'Nisporeni'                                            => 'Ниспорены',
        'Grigoriopol'                                          => 'Григориополь',
        'Rascani'                                              => 'Рышкань',
        'Glodeni'                                              => 'Глодяны',
        'Camenca'                                              => 'Каменка',
        'Basarabeasca'                                         => 'Бессарабка',
        'Leova'                                                => 'Леово',
        'Ocnita'                                               => 'Окница',
        'Donduseni'                                            => 'Дондюшаны',
        'Briceni'                                              => 'Бричаны',
        'Otaci'                                                => 'Атаки',
        'Anenii Noi'                                           => 'Новые Анены',
        'Stefan Voda'                                          => 'Штефан Водэ',
        'Cupcini'                                              => 'Купчинь',
        'Sangera'                                              => 'Сынжера',
        'Criuleni'                                             => 'Криуляны',
        'Telenesti'                                            => 'Теленешты',
        'Soldanesti'                                           => 'Шолданешты',
        'Tvardita'                                             => 'Твардица',
        'Lipcani'                                              => 'Липканы',
        'Vadul lui Voda'                                       => 'Ваду-луй-Водэ',
        'Iargara'                                              => 'Яргара',
        'Cainari'                                              => 'Кэинары',
        'Cantemir'                                             => 'Кантемир',
        'Vatra'                                                => 'Ватра',
        'Biruinta'                                             => 'Бируинца',
        'Crasnoe'                                              => 'Красное',
        'Cornesti'                                             => 'Корнешты',
        'Costesti'                                             => 'Костешты',
        'Marculesti'                                           => 'Маркулешты',
        'Ghindesti'                                            => 'Гындешты',
        'Tiraspolul Nou'                                       => 'Новый Тирасполь',
        'Frunza'                                               => 'Фрунзе',
        'Bucovat'                                              => 'Буковець',
        'Maiac'                                                => 'Маяки',
        'Category'                                             => 'Категория',
        'Area'                                                 => 'Район',
        'Adress'                                               => 'Адрес',
        'Description'                                          => 'Описание',
        'Role'                                                 => 'Роль',
        'Image'                                                => 'Фотография',
        'Creative Tim'                                         => 'Творческая команда',
        'License'                                              => 'Лицензия',
        'Designed by'                                          => 'Разработано',
        'Cost Effective Service'                               => 'Эффективное обслуживание',
        'Buy with Confidence'                                  => 'Пользуйтесь с уверенностью',
        'Book a billboard'                                     => 'Заказать баннер',
        'ACTIVE_BILLBOARDS'                                    => 'Доступные баннеры',
        'See all'                                              => 'Смотреть все',
        'Get in Touch'                                         => 'Свяжитесь с нами',
        'Find us at the office'                                => 'Мы находимся по адрессу',
        'Our address'                                          => 'Наш адресс',
        'more_information'                                     => 'дополнительная информация',
        'Easy registration'                                    => 'Простая регистрация',
        'Boards search'                                        => 'Поиск баннеров',
        'Start search'                                         => 'Поиск',
        'Owner'                                                => 'Владелец',
        'Owners'                                               => 'Владельцы',
        'Agencies'                                             => 'Агенства',
        'Agency'                                               => 'Агентство',
        'Administrator'                                        => 'Администратор',
        'Guest'                                                => 'Гость',
        'Companies'                                            => 'Компании',
        'Easy board add'                                       => 'Легко добавить баннер',
        'Transparent prices'                                   => 'Прозрачная ценовая политика',
        'Boards management'                                    => 'Управление баннерами',
        'Price configuration'                                  => 'Менеджмент цен',
        'Statistics'                                           => 'Статистика',
        'Create Account'                                       => 'Регистрация',
        'Incorrect login and/or password.'                     => 'Неправильный логин и/или пароль',
        'Forgot password?'                                     => 'Забыли пароль?',
        'Custom solutions'                                     => 'Индивидуальный подход',
        'CRM Impord and Export'                                => 'CRM импорт/экспорт',
        'Full support'                                         => 'Полная поддержка',
        'Extended statics'                                     => 'Расширенная статистика',
        'Register'                                             => 'Регистрация',
        'Contact us'                                           => 'Свяжитесь с нами',
        'Available'                                            => 'Доступно',
        'Banner categories'                                    => 'Категории баннеров',
        'Big banners'                                          => 'Большие баннеры',
        'Standard banners'                                     => 'Стандартные баннеры',
        'Digital banners'                                      => 'Цифровые баннеры',
        'Firewall'                                             => 'Брендмауэр',
        'Other'                                                => 'Другие',
        'Bracing'                                              => 'Перетяжка',
        'Cityboards'                                           => 'Ситиборды',
        'Frequently Asked Questions'                           => 'Часто задаваемые вопросы',
        'Contact Us'                                           => 'Свяжитесь с нами',
        'Email address'                                        => 'Ваш электронный адрес',
        'Your message'                                         => 'Ваше сообщение',
        'I am not a robot'                                     => 'Я не робот',
        'Send Message'                                         => 'Отправить письмо',
        'Region'                                               => 'Регион',
        'About Us'                                             => 'О нас',
        'Email Here...'                                        => 'Ваш е-мэйл',
        'First name'                                           => 'Имя',
        'Last name'                                            => 'Фамилия',
        'First Name'                                           => 'Имя',
        'Last Name'                                            => 'Фамилия',
        'First Name...'                                        => 'Имя',
        'Last Name...'                                         => 'Фамилия',
        'The biggest base oo billborads in Moldova.'           => 'Самая большая база Баннеров Молдовы',
        'HOW_ADD'                                              => 'Как добавить новый рекламный щит в систему?',
        'SHOULD_ADD'                                           => 'Зарегистрируйтесь и воспользуйтесь панелью управления баннерами (доступно и с мобильного приложения)',
        'HOW_RENT'                                             => 'Как арендовать рекламный щит',
        'SHOULD_RENT'                                          => 'Воспользуйтесь поиском для поиска баннера, который лучше всего соответствует вашим потребностям, укажите желаемую дату и подтвердите аренду',
        'CONNECT_BILLBOARD'                                    => 'Можно ли подключить систему рекламных щитов к нашему CRM?',
        'CONNECT_FLEXIBLE'                                     => 'Мы гибки и открыты для любых предложений. Если вам нужно индивидуальное решение - просто свяжитесь с нами',
        'ANY_QUESTIONS'                                        => 'Появились вопросы, на которые мы можем ответить?',
        'WE_ARE_HAPPY'                                         => 'Мы рады Вам помочь. Свяжитесь с нами.',
        'YOU_CAN'                                              => 'Вы можете легко арендовать, забронировать или управлять рекламным щитом в любой точке Молдовы с помощью нашей мощной системы',
        'OUR_MISSION'                                          => 'Мы стремимся к тому, чтобы создать самую большую базу рекламных щитов в Молдове. Наша миссия состоит в том, чтобы сделать аренду рекламного щита простым и доступным, как 1-2-3',
        'USE_OUR_EXPERIENCE'                                   => 'Воспользуйтесь нашим опытом по доставке баннеров по наилучшей цене. У нас очень давние отношения со многими компаниями, и их знание в сочетании с нашими собственными силами создает для Вас качественные предоставление услуг.',
        'BUY_WITH_CONFIDENCE'                                  => 'Покупайте с уверенностью с нашего безопасного онлайн-сайта. Также Вы можете позвонить нам или отправьте нам свой номер телефона в нашей функции «возврат вызова» на домашней странице, и мы будем рады обсудить ваши требования и принять заказы и обсудить детали по телефону.',
        'Extended statistics'                                  => 'Расширенная статистика',
        'Add Banner'                                           => 'Добавить баннер',
        'Add Banner from file'                                 => 'Добавить баннеры из файла',
        'Update'                                               => 'Обновить',
        'Delete Banner'                                        => 'Удалить баннер',
        'All Users'                                            => 'Все пользователи',
        'Add User'                                             => 'Добавить пользователя',
        'Delete User'                                          => 'Удалить пользователя',
        'Roles'                                                => 'Роли',
        'Privilege description'                                => 'Описание привилегий',
        'Update description'                                   => 'Обновить описание',
        'Cities'                                               => 'Города',
        'Name city'                                            => 'Название города',
        'Name region'                                          => 'Название региона',
        'city'                                                 => 'город',
        'region'                                               => 'регион',
        'Add City'                                             => 'Добавить Город',
        'Delete City'                                          => 'Удалить Город',
        'Update Role'                                          => 'Обновить роль',
        'Close'                                                => 'Закрыть',
        'Update Banner'                                        => 'Обновить баннер',
        'Are you sure to delete this Banner?'                  => 'Вы уверены, что хотите удалить выбранный баннер ???',
        'No'                                                   => 'Нет',
        'Yes'                                                  => 'Да',
        'Banner Categories'                                    => 'Категории Баннеров',
        'Add Banner Category'                                  => 'Добавить категорию Баннеров',
        'Add Category'                                         => 'Добавить категорию',
        'Update Banners Category'                              => 'Обновить категорию Баннеров',
        'Delete Category'                                      => 'Удалить категорию',
        'Are you sure to delete this Category?'                => 'Вы уверены, что хотите удалить выбранную категорию?',
        'Value is required and can\'t be empty'                => 'Необходимо заполнить поле',
        'Name_city'                                            => 'Название города',
        'Name_region'                                          => 'Название региона',
        'Indexname_city'                                       => 'Indexname города',
        'Indexname_region'                                     => 'Indexname региона',
        'Are you sure to delete this City?'                    => 'Вы уверены, что хотите удалить выбранный город ??',
        'Update City'                                          => 'Обновить Город',
        'Update User'                                          => 'Обновить пользователя',
        'Are you sure to delete this User?'                    => 'Вы уверены, что хотите удалить выбранного Пользователя ??',
        'Profile'                                              => 'Профиль',
        'Logout'                                               => 'Выйти',
        'REG_SUCCESS'                                          => 'Вы успешно зарегистрировались. Пожалуйста, проверьте Ваш e-mail адрес.',
        'THANK_YOU'                                            => 'Благодарим за регистрацию!',
        'E-mail is not confirmed'                              => 'Вы не подтвердили свой E-mail',
        'Confirm now'                                          => 'Подтверждение',
        'Change Email'                                         => 'Изменить e-mail',
        'Change Password'                                      => 'Изменить пароль',
        'Change Your Image'                                    => 'Изменить аватар',
        'Incorrect password.'                                  => 'Неправильный пароль',
        'Control Panel'                                        => 'Панель управления',
        'Change Your Name'                                     => 'Изменить Имя',
        'Manage Your profile'                                  => 'Управление профилем',
        'The two given tokens do not match'                    => 'Указанные пароли не совпадают',
        'The input is less than 4 characters long'             => 'Пожалуйста, введите не менее 4 символов',
        'The input is less than 6 characters long'             => 'Пожалуйста, введите не менее 6 символов',
        'Please confirm your email to complete registration.'  => 'Пожалуйста, подтвердите свой email для завершения регистрации.',
        'You have successfully booked a banner!'               => 'Вы успешно забронировали баннер!
                                                              Вам был отправлен счет на оплату на вашу почту.
                                                              Просим Вас оплатить его в течении 24 часов. В противном случае мы будем вынуждены снять бронь и сделать его доступным нашим клиентам.',
        'Also, you can view the account in your account or by clicking on the link:'                                => 'Также, Вы можете просмотреть счет в личном кабинете или перейдя по ссылке:',
        'Our News'                                             => 'Наши новости',
        'Send us a message'                                    => 'Напишите нам письмо',
        'Your Name'                                            => 'Ваше Имя',
        'Phone'                                                => 'Телефон',
        'Give us a ring'                                       => 'Контакты',
        'All News'                                             => 'Все новости',
        'Title'                                                => 'Заголовок',
        'Article'                                              => 'Статья',
        'Image path'                                           => 'Путь к фотографии',
        'Visible'                                              => 'Видим',
        'Add News'                                             => 'Добавить новость',
        'Update News'                                          => 'Обновить новость',
        'Delete News'                                          => 'Удалить новость',
        'Are you sure to delete this News?'                    => 'Вы уверены, что хотите удалить выбранную новость ??',
        'Active'                                               => 'Активна',
        'You can contact us with anything related to our Products. 
        We\'ll get in touch with you as soon as possible.'     => 'По всем интересующим Вас вопросам, Вы можете связаться с нами.',
        'For agency'                                           => 'Для агенств',
        'For owners'                                           => 'Для владельцев',
        'For customers'                                        => 'Для компаний',
        'Advantages of working with us'                        => 'Преимущества работы с нами',
        'Blog'                                                 => 'Блог',
        'Working hours'                                        => 'Часы работы',
        'Mon. - Fr.'                                           => 'Пн. - Пт.',
        'Sat.'                                                 => 'Сб.',
        'Sun. - Day off'                                       => 'Вс. - Выходной',
        'Coming soon'                                          => 'Скоро освободятся',
        'Busy'                                                 => 'Затяны',
        'Free'                                                 => 'Свободны',
        'Booked'                                               => 'Забронированны',
        'New flatness'                                         => 'Новые полскости',
        'Popular flatness'                                     => 'Популярные плоскости',
        'Hot flatness'                                         => 'Купить',



];
