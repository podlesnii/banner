<?php

return [
        'OUR_MISSION'                            => 'We are here to connect banner owners and billboard renters. Our mission is to make billboard rent easy as 1-2-3!',
        'YOU_CAN'                                => 'You can rent, lease or manage a billboard easilly in any place of Moldova with our powerfull',
        'more_information'                       => 'You need more information? Contact us and we will help you.',
        'HOW_ADD'                                => 'How can I add new billboard to system?',
        'SHOULD_ADD'                             => 'You should register and use our billboard management panel or mobile app',
        'HOW_RENT'                               => 'How can I rent billboard',
        'SHOULD_RENT'                            => 'You should use search and found the billboard best fits your needs, pick up desired dates,
                            and confrim rent',
        'CONNECT_BILLBOARD'                      => 'Is it possible to connect billboard system to our CRM?',
        'CONNECT_FLEXIBLE'                       => 'We are flexible and open for any suggestions. If you need custom solution - please just
                            contact us',
        'ANY_QUESTIONS'                          => 'Any other questions we can answer?',
        'WE_ARE_HAPPY'                           => 'We are happy to help you. Contact us.',
        'ACTIVE_BILLBOARDS'                      => ' Active billboards in our system. You can show your comertials on them right now!',
        'USE_OUR_EXPERIENCE'                     => ' Use our experience to deliver the displays that you need at the best possible price. We have great long standing relationships with many of our material manufacturers, and, their knowledge combined with our own, makes for a formidable team at your disposal.',
        'BUY_WITH_CONFIDENCE'                    => ' Buy with confidence from our secure online site. If you prefer, please telephone us or send us your phone number in our \'return call feature\' on the home page, and we will be delighted to discuss your requirements and take orders and payment details on the telephone.',
        'Login_adminlte'                         => 'Login',
        'Login2'                                 => 'Login',
        'Repeat password'                        => 'Repeat password',
        'Update User'                            => 'Update User',
        'Are you sure to delete this User?'      => 'Are you sure to delete this User?',
        'No'                                     => 'No',
        'Yes'                                    => 'Yes',
        'Update Banner'                          => 'Update Banner',
        'Are you sure to delete this Banner?'    => 'Are you sure to delete this Banner?',
        'Banner Categories'                      => 'Banner Categories',
        'Add Banner Category'                    => 'Add Banner Category',
        'Add Category'                           => 'Add Category',
        'Update Banners Category'                => 'Update Banners Category',
        'Delete Category'                        => 'Delete Category',
        'Are you sure to delete this Category?'  => 'Are you sure to delete this Category?',
        'Delete'                                 => 'Delete',
        'REG_SUCCESS'                            => 'Registration is performed successfully, please check your email for confirmation',
        'THANK_YOU'                              => 'Thank you!',
        'You have successfully booked a banner!' => 'You have successfully booked a banner!
                                                     The invoice was sent to your mail.
                                                     We ask you to pay for it within 24 hours. Otherwise we will be forced to withdraw the reservation and make it available to our customers.',
        'Also, you can view the account in your 
        account or by clicking on the link:'     => 'Also, you can view the account in your account or by clicking on the link:',

];
