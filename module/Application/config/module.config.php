<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Acl\Model\RoleDomain;
use Application\Controller\Factory\AuthControllerFactory;
use Application\Controller\Factory\ContactControllerFactory;
use Application\Controller\Factory\DescriptionControllerFactory;
use Application\Controller\Factory\IndexControllerFactory;
use Application\Controller\Factory\NewsControllerFactory;
use Application\Controller\Factory\OrderControllerFactory;
use Application\Controller\Factory\ProfileControllerFactory;
use Application\Controller\Factory\SearchControllerFactory;
use Application\Form\Factory\SearchFormFactory;
use Application\Form\SearchForm;
use Application\Model\BannerCategoryDomain;
use Application\Model\BannerDomain;
use Application\Model\BannerRatingsDomain;
use Application\Model\BannerStatusDomain;
use Application\Model\BannerStatusHistoryDomain;
use Application\Model\ImageDomain;
use Application\Model\InvoiceDomain;
use Application\Model\NewsDomain;
use Application\Service\Auth;
use Application\Service\AuthAdapter;
use Application\Service\DomainModelManager;
use Application\Model\DomainModelAbstractFactory;
use Application\Model\UserDomain;
use Application\Model\CityDomain;
use Application\Service\DomainModelManagerFactory;
use Application\Service\Factory\AuthAdapterFactory;
use Application\Service\Factory\AuthServiceFactory;
use Application\Service\Factory\LanguageFactory;
use Application\Service\Factory\NotificationsFactory;
use Application\Service\Factory\OrderFactory;
use Application\Service\Factory\TranslationManagerFactory;
use Application\Service\Language;
use Application\Service\Notifications;
use Application\Service\Order;
use Application\Service\Provider\BannerCategoryProvider;
use Application\Service\Provider\CityProvider;
use Application\Service\Provider\Factory\ImageProviderFactory;
use Application\Service\Provider\ImageProvider;
use Application\Service\Provider\RoleProvider;
use Application\Service\Provider\Factory\BannerCategoryProviderFactory;
use Application\Service\Provider\Factory\CityProviderFactory;
use Application\Service\Provider\Factory\RoleProviderFactory;
use Application\Service\TranslationManager;
use Application\View\Helper\ControllerName;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Zend\Authentication\AuthenticationService;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\ServiceManager;

return [
    'router' => [
        'routes' => [
            'home'            => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'register'        => [
                'type'          => Literal::class,
                'options'       => [
                    'route'    => '/register',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'register',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'success'      => [
                        'type'    => Literal::class,
                        'options' => [
                            'route'    => '/success',
                            'defaults' => [
                                'controller' => Controller\AuthController::class,
                                'action'     => 'success',
                            ],
                        ],
                    ],
                    'confirmation' => [
                        'type'    => Literal::class,
                        'options' => [
                            'route'    => '/confirmation',
                            'defaults' => [
                                'controller' => Controller\AuthController::class,
                                'action'     => 'confirmation',
                            ],
                        ],
                    ],
                ],
            ],
            'register-customer'      => [
                    'type'    => Literal::class,
                    'options' => [
                            'route'    => '/register-customer',
                            'defaults' => [
                                    'controller' => Controller\AuthController::class,
                                    'action'     => 'register-customer',
                            ],
                    ],
            ],
            'register-owner'      => [
                    'type'    => Literal::class,
                    'options' => [
                            'route'    => '/register-owner',
                            'defaults' => [
                                    'controller' => Controller\AuthController::class,
                                    'action'     => 'register-owner',
                            ],
                    ],
            ],
            'register-agency'      => [
                    'type'    => Literal::class,
                    'options' => [
                            'route'    => '/register-agency',
                            'defaults' => [
                                    'controller' => Controller\AuthController::class,
                                    'action'     => 'register-agency',
                            ],
                    ],
            ],
            'profile'         => [
                'type'          => Literal::class,
                'options'       => [
                    'route'    => '/profile',
                    'defaults' => [
                        'controller' => Controller\ProfileController::class,
                        'action'     => 'profile',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'email'    => [
                        'type'    => Literal::class,
                        'options' => [
                            'route'    => '/email',
                            'defaults' => [
                                'controller' => Controller\ProfileController::class,
                                'action'     => 'email',
                            ],
                        ],
                    ],
                    'name'     => [
                        'type'    => Literal::class,
                        'options' => [
                            'route'    => '/name',
                            'defaults' => [
                                'controller' => Controller\ProfileController::class,
                                'action'     => 'name',
                            ],
                        ],
                    ],
                    'password' => [
                        'type'    => Literal::class,
                        'options' => [
                            'route'    => '/password',
                            'defaults' => [
                                'controller' => Controller\ProfileController::class,
                                'action'     => 'password',
                            ],
                        ],
                    ],
                    'icon'     => [
                        'type'    => Literal::class,
                        'options' => [
                            'route'    => '/icon',
                            'defaults' => [
                                'controller' => Controller\ProfileController::class,
                                'action'     => 'icon',
                            ],
                        ],
                    ],
                ],
            ],
            'login'           => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/login',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'login',
                    ],
                ],
            ],
            'logout'          => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/logout',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'logout',
                    ],
                ],
            ],
            'reset_password'  => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/reset-password',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'reset-password',
                    ],
                ],
            ],
            'forgot_password' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/forgot-password',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'forgot-password',
                    ],
                ],
            ],
            'search'          => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/search',
                    'defaults' => [
                        'controller' => Controller\SearchController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'basket'          => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/basket',
                    'defaults' => [
                        'controller' => Controller\BasketController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'description'     => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/description[/:id]',
                    'defaults' => [
                        'controller' => Controller\DescriptionController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'modalLogin'     => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/description/modalLogin',
                    'defaults' => [
                        'controller' => Controller\DescriptionController::class,
                        'action'     => 'modalLogin',
                    ],
                ],
            ],

            'order'           => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/order[/:action]',
                    'defaults' => [
                        'controller' => Controller\OrderController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'application'     => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/application[/:action]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'contact'         => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/contact',
                    'defaults' => [
                        'controller' => Controller\ContactController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'news'            => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/news[/:action]',
                    'defaults' => [
                        'controller' => Controller\NewsController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],

    'controllers'          => [
        'factories' => [
            Controller\IndexController::class       => IndexControllerFactory::class,
            Controller\AuthController::class        => AuthControllerFactory::class,
            Controller\SearchController::class      => SearchControllerFactory::class,
            Controller\ProfileController::class     => ProfileControllerFactory::class,
            Controller\DescriptionController::class => DescriptionControllerFactory::class,
            Controller\OrderController::class       => OrderControllerFactory::class,
            Controller\ContactController::class     => ContactControllerFactory::class,
            Controller\NewsController::class        => NewsControllerFactory::class,
        ],
    ],
    'service_manager'      => [
        'factories' => [
            DomainModelManager::class     => DomainModelManagerFactory::class,
            Auth::class                   => AuthServiceFactory::class,
            AuthAdapter::class            => AuthAdapterFactory::class,
            AuthenticationService::class  => Service\Factory\AuthenticationServiceFactory::class,
            BannerCategoryProvider::class => BannerCategoryProviderFactory::class,
            CityProvider::class           => CityProviderFactory::class,
            ImageProvider::class          => ImageProviderFactory::class,
            RoleProvider::class           => RoleProviderFactory::class,
            TranslationManager::class     => TranslationManagerFactory::class,
            Language::class               => LanguageFactory::class,
            SearchForm::class             => SearchFormFactory::class,
            Order::class                  => OrderFactory::class,
            Notifications::class          => NotificationsFactory::class,
        ],
    ],
    'domain_model_manager' => [
        'factories' => [
            UserDomain::class                 => DomainModelAbstractFactory::class,
            BannerDomain::class               => DomainModelAbstractFactory::class,
            BannerRatingsDomain::class        => DomainModelAbstractFactory::class,
            BannerCategoryDomain::class       => DomainModelAbstractFactory::class,
            ImageDomain::class                => DomainModelAbstractFactory::class,
            CityDomain::class                 => DomainModelAbstractFactory::class,
            BannerStatusDomain::class         => DomainModelAbstractFactory::class,
            BannerStatusHistoryDomain::class  => DomainModelAbstractFactory::class,
            RoleDomain::class                 => DomainModelAbstractFactory::class,
            InvoiceDomain::class              => DomainModelAbstractFactory::class,
            NewsDomain::class                 => DomainModelAbstractFactory::class,
        ],
    ],
    'translator'           => [
        'defaultLocale'             => 'ro_RO',
        'existingLocales'           => [
            'ru_RU', 'ro_RO', 'en_GB',
        ],
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
        'event_manager_enabled'     => true,
    ],
    'translation_manager'  => [
        'base_dir' => __DIR__ . '/../../../data/language',
        'pattern'  => 'missing/%s_missing.php',
    ],
    'doctrine'             => [
        'driver' => [
            'application_entities' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity',
                ],
            ],
            'orm_default'          => [
                'drivers' => [
                    'Application\Entity' => 'application_entities',
                ],
            ],
        ],
    ],
    'view_helpers'         => array(
        'factories' => array(
            'controllerName' => function (ServiceManager $sm) {
                $application = $sm->get('Application');
                $routeMatch = $application->getMvcEvent()->getRouteMatch();

                $helper = new ControllerName($routeMatch);

                return $helper;
            },
            'language'       => function (ServiceManager $sm) {
                $languageService = $sm->get(Language::class);

                $helper = new View\Helper\Language($languageService);

                return $helper;
            },
            'userHelper'     => function (ServiceManager $sm) {
                $authService = $sm->get(Auth::class);

                $helper = new View\Helper\User($authService);

                return $helper;
            },
            'notificationsHelper' => function (ServiceManager $sm) {
                $notificationsService = $sm->get(Notifications::class);

                $helper = new \Application\View\Helper\Notifications($notificationsService);

                return $helper;
            },
        ),
    ),
    'view_manager'         => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map'             => [
            'layout/layout'                      => __DIR__ . '/../view/layout/layout.phtml',
            'application/description/modal-login' => __DIR__ . '/../view/application/description/index.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],

    'googleCaptchaConfig' => [
        'keys' => [
            'publicKey' => '6LeW-WkUAAAAAJWt3u45E7hnmXbHlMdYUnZBxP2N',
            'privateKey' => '6LeW-WkUAAAAADPdKheAl6S8PaNpMaYO_w79aNEI',
        ],
    ],
];
