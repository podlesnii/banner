<?php
namespace Cli\Service;
use Zend\Console\Adapter\AdapterInterface;
use ZF\Console\Route;

/**
 * Class CliAbstract
 * @package Cli\Service
 */
abstract class CliAbstract extends LoggableAbstract
{

    /** @var  Route */
    protected $route;


    /**
     * @param Route $route
     * @param AdapterInterface $console
     */
    public function __invoke(Route $route, AdapterInterface $console)
    {
        $this->route = $route;
        $this->console = $console;

        $this->execute();
    }

    /**
     * @return mixed
     */
    abstract function execute();

}