<?php
namespace Cli\Service\Factory;

use Cli\Service\CliAbstract;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class CliDefaultFactory
 * @package Cli\Service\Factory
 */
class CliDefaultFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     *
     * @return CliAbstract
     * @throws \Exception
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $cliManager = new $requestedName();
        $class = CliAbstract::class;
        if (!$requestedName instanceof $class) {
            throw new \Exception('Inorrect requested name');
        }

        return $cliManager;
    }

}