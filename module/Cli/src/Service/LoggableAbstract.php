<?php

namespace Cli\Service;


use Zend\Console\Adapter\AdapterInterface;
use Zend\Console\ColorInterface;
use Zend\Log\Logger;

/**
 * Class LoggableAbstract
 * @package Application\Service
 */
class LoggableAbstract
{

    protected $defaultColor = ColorInterface::WHITE;

    protected $colorMapper = [
        Logger::WARN   => ColorInterface::YELLOW,
        Logger::ERR    => ColorInterface::RED,
        Logger::INFO   => ColorInterface::WHITE,
        Logger::NOTICE => ColorInterface::BLUE,
        Logger::DEBUG  => ColorInterface::GREEN
    ];

    /** @var  AdapterInterface */
    protected $console;

    /**
     * @param $message
     * @param $priority
     */
    public function log($message, $priority = Logger::INFO)
    {
        $color = isset($this->colorMapper[$priority]) ? $this->colorMapper[$priority] : $this->defaultColor;
        $this->console->writeLine($message, $color);
    }

    /**
     * @return AdapterInterface
     */
    public function getConsole()
    {
        return $this->console;
    }

    /**
     * @param AdapterInterface $console
     */
    public function setConsole($console)
    {
        $this->console = $console;
    }

}