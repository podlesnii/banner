<?php


namespace Cli;

/**
 * Class Module
 * @package Cli
 */
class Module
{
    const VERSION = '3.0.1';

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

}
