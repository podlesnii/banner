/**
 * Trigger loading spinner to container
 *
 * @param container
 * @param active - if true, spinner is displayed, otherwise none
 */
function overlayBox(container, active) {
    var box = $(container).parent('.box'),
        overlayWrapper = $(container).parent('.overlay-wrapper'),
        wrapperContainer = null;
    if (box.length) {
        wrapperContainer = box;
    }
    else {
        if (overlayWrapper.length) {
            wrapperContainer = overlayWrapper;
        }
    }

    if (wrapperContainer != null) {
        var overlay = wrapperContainer.children('.overlay');
        if (!overlay.length) {
            wrapperContainer.append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
            overlay = wrapperContainer.children('.overlay');
        }
        if (active) {
            overlay.show();
        }
        else {
            overlay.hide();
        }
    }
}

/**
 * Makes ajax call to URL and loads returned data into container
 *
 * @param containerName
 * @param url
 */
function loadDataToContainer(containerName, url) {
    var mainWindow = $('#main-window'),
        container = mainWindow.find('#' + containerName);

    overlayBox(container, true);
    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            container.empty();
            container.append(data);
            overlayBox(container, false);
        },
        fail: function (data) {
            console.log('fail', data);
        }
    });
}

//Checking json notifications
function executeCallbacks(data) {
    if (data.callbacks != null) {
        jQuery.each(data.callbacks, function (index, item) {
            if (item.callback != null && item.arguments != null) {
                var fn = window[item.callback];
                if (typeof fn === 'function') {
                    fn.apply(this, item.arguments);
                }
            }
        });
    }
    jQuery.each(data.notifications, function (index, notification){
        type = notification.type;
        message = notification.message;
        notifyNow(type, message);

        console.log(notification.type);
        console.log(notification.message);

    });

}

/**
 * Used to hide modal window
 */
function hideModal() {
    var modalContainer = $('#modal-container');
    modalContainer.modal('hide');
}

function initEditor(modalBody) {
    var editorFields = $(modalBody).find('.wysiwyg-editor');
    if (editorFields.length > 0) {
        jQuery.each(editorFields, function (index, item) {
            CKEDITOR.replace(item, {
                // Define the toolbar: http://docs.ckeditor.com/ckeditor4/docs/#!/guide/dev_toolbar
                // The standard preset from CDN which we used as a base provides more features than we need.
                // Also by default it comes with a 2-line toolbar. Here we put all buttons in a single row.
                toolbar: [
                    { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
                    { name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
                    { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
                    { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
                    { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
                    { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
                    { name: 'colors', items: [ 'TextColor', 'BGColor' ] },

                ]});
        });
    }
}

jQuery(document).ready(function ($) {
    var mainWindow = $('#main-window'),
        modalContainer = $('#modal-container');


    /**
     * Makes ajax call and loads result data to global modal container.
     * Also sets the title for global modal container
     *
     * @param url
     * @param modalTitle
     */
    function loadModalWindow(url, modalTitle) {
        var modalBody = modalContainer.find('.modal-body');
        modalContainer.find('.modal-title').text(modalTitle);
        modalBody.empty();
        modalContainer.modal('show');
        overlayBox(modalBody, true);

        $.ajax({
            type: "GET",
            url: url,
            success: function (data) {
                modalBody.append(data);
                initEditor(modalBody);
                overlayBox(modalBody, false);
            },
            fail: function (data) {
                console.log('fail', data);
            }
        });
    }


    /**
     * Listener which will load any data from link with ajax to specified container.
     * Will not work if 'href' or 'data-result-container-name' attributes are missing.
     *
     * Example of usage: <a href='example.url' data-result-container-name='example-container-id'>Example</a>
     *
     */
    mainWindow.on('click', 'a.btn-load-data', function (e) {
        e.preventDefault();
        var linkItem = $(this),
            href = linkItem.attr('href'),
            resultContainerName = linkItem.attr('data-result-container-name');

        if (href.length < 1 || resultContainerName.length < 1) {
            return false;
        }

        loadDataToContainer(resultContainerName, href);
    });

    /**
     * Listener which will do ajax query by the link and will load result data to modal window
     * Will not work if 'href' or 'data-modal-title' attributes are missing.
     *
     */
    mainWindow.on('click', 'a.btn-modal-action', function (e) {
        e.preventDefault();
        var linkItem = $(this),
            href = linkItem.attr('href'),
            modalTitle = linkItem.attr('data-modal-title');

        if (href.length < 1 || modalTitle.length < 1) {
            return false;
        }

        loadModalWindow(href, modalTitle);
    });
});

jQuery(document).ready(function ($) {
    var body = $('body');

    /**
     * Submit form displayed in modal window using ajax
     */
    body.on('click', '.btn-ajax-form-submit', function (e) {
        e.preventDefault();
        var button = $(this),
            form = button.closest('form');

        $(form).find('.wysiwyg-editor').each(function (index, item) {
            $(item).val(CKEDITOR.instances[$(item).attr('name')].getData());
        });
        var action = form.attr('action'),
            method = form.attr('method'),
            data = form.serialize(),
            containerSelector = form.attr('data-result-container-selector'),
            container = body.find(containerSelector);

        overlayBox(container, true);
        $.ajax({
            type: method,
            url: action,
            data: data,
            success: function (data) {
                container.empty();
                if (typeof data === 'object' && data != null) {
                    executeCallbacks(data);
                }
                else {
                    container.append(data);
                }
                overlayBox(container, false);
            }
        });

    });

});

jQuery(document).ready(function ($) {
    var body = $('body');

    /**
     * Display modal window with Yes and No buttons
     */
    body.on('click', '.btn-ajax-confirmation', function (e) {
        e.preventDefault();
        var linkItem = $(this),
            href = linkItem.attr('href'),
            confirmationText = linkItem.attr('data-confirmation-text'),
            confirmationTitle = linkItem.attr('data-confirmation-title'),
            modalContainer = $('#modal-confirmation-container');

        if (href.length && confirmationText.length && confirmationTitle.length) {
            modalContainer.find('.modal-title').text(confirmationTitle);
            modalContainer.find('.modal-body-text').text(confirmationText);
            modalContainer.attr('data-href', href);
            modalContainer.modal('show');
        }
    });

    body.on('click', '.btn-modal-confirmation-yes', function (e) {
        e.preventDefault();
        var linkItem = $(this),
            modalContainer = linkItem.closest('#modal-confirmation-container'),
            href = modalContainer.attr('data-href');

        if (href.length) {
            overlayBox(modalContainer, true);
            $.ajax({
                type: 'GET',
                url: href,
                success: function (data) {
                    if (typeof data === 'object' && data != null) {
                        executeCallbacks(data);
                    }
                    overlayBox(modalContainer, false);
                    modalContainer.modal('hide');
                }
            });
        }
    });
});

jQuery(document).ready(function ($) {
    'use strict';
    var fileUploadContainer = $('#fileupload');
    if (fileUploadContainer.length > 0) {
        var config = JSON.parse(fileUploadContainer.attr('data-config'));

        // Initialize the jQuery File Upload widget:
        $('#fileupload').fileupload({
            dataType: 'json',
            url: config.url,
            autoUpload: true
        }).bind('fileuploadprocessdone', function (e, data) {
            console.log(e, data);
        });
    }

});