$(document).ready(function () {

    $('#contact_form').click(function (e) {
        e.preventDefault();
        var form = $(this).closest('form');
        var data = form.serialize();
        var name = document.forms["contact-form"]["name"].value;
        var email = $("#email").val();
        var phone = document.forms["contact-form"]["phone"].value;
        var message = document.forms["contact-form"]["message"].value;

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        if (!validateEmail(email)) {
            $('#contact_danger').show("slow");
            $('#contact_danger').hide(5000);
            notifyNow('warning', 'Please enter Email!');
            return false;
        }
        if (name.length == 0) {
            $('#contact_danger').show("slow");
            $('#contact_danger').hide(5000);
            notifyNow('warning', 'Please enter your Name!');
            return false;
        }
        if (phone.length == 0) {
            $('#contact_danger').show("slow");
            $('#contact_danger').hide(5000);
            notifyNow('warning', 'Please enter your Phone!');
            return false;
        }
        if (message.length == 0) {
            $('#contact_danger').show("slow");
            $('#contact_danger').hide(5000);
            notifyNow('warning', 'Please enter your message!');
            return false;
        }

        $.ajax({
        type: 'post',
            url: '/contact',
            data: data,
            success: function (result) {
                result.notifications.forEach(function(value) {
                    notifyNow(value.type, value.message);
                });
                form[0].reset();
            }
        });

    });
});



