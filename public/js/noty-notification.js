    //Checking hidden div from footer for new notification
    var messages = $.find('.noty-message');
    $.each(messages, function (index, message) {
        var type = $(message).data('noty-type'),
            messageContent = $(message).data('noty-message');
        notifyNow(type, messageContent);
    });


    function notifyNow(type, messageContent) {
        new Noty({
            type: type,
            text: messageContent,
            layout: 'topRight',
            theme: 'metroui',
            closeWith: ['click', 'button'],
            timeout: 4000,
            animation: {
                open: 'animated bounceInRight', // Animate.css class names
                close: 'animated bounceOutRight', // Animate.css class names
            },
        }).show();
    }
