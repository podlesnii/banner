$(document).ready(function() 
{
	$(".animate-in-view").each(function() {
		var b = $(this);
		b.data("animation"),
		new Waypoint({
			element: b,
			handler: function(a) {
				b.addClass(b.data("animation") + " animated")
			},
			offset: "90%"
		});
	});

	var swiper = new Swiper('.swiper-container1', {
		initialSlide    :0,
        slidesPerView   : 4,
		speed			: 700,
		loop			: true,
		pagination: {
			el: '.swiper-pagination1',
			clickable   : true
		},
		navigation: {
			nextEl: '.swiper-button-next1',
			prevEl: '.swiper-button-prev1',
		},
	});

	swiper.on('slideChange', function () {
		var ind = swiper.realIndex + 1;
		$('.swiper-slider-index1').html( ind + '/' + $('.swiper-container1').data('sliders-count') );
	});

	var swiper2 = new Swiper('.swiper-container2', {
		initialSlide    : 0,
        slidesPerView   : 4,
		speed			: 700,
		loop			: true,
		pagination: {
			el: '.swiper-pagination2',
			clickable   : true
		},
		navigation: {
			nextEl: '.swiper-button-next2',
			prevEl: '.swiper-button-prev2',
		},
	});

	swiper2.on('slideChange', function () {
		var ind = swiper2.realIndex + 1;
		$('.swiper-slider-index2').html( ind + '/' + $('.swiper-container2').data('sliders-count') );
	});

	var swiper3 = new Swiper('.swiper-container3', {
		initialSlide    : 0,
        slidesPerView   : 4,
		speed			: 700,
		loop			: true,
		pagination: {
			el: '.swiper-pagination3',
			clickable   : true
		},
		navigation: {
			nextEl: '.swiper-button-next3',
			prevEl: '.swiper-button-prev3',
		},
	});

	swiper3.on('slideChange', function () {
		var ind = swiper3.realIndex + 1;
		$('.swiper-slider-index3').html( ind + '/' + $('.swiper-container3').data('sliders-count') );
	});

	var swiper4 = new Swiper('.swiper-container4', {
		initialSlide    : 0,
        slidesPerView   : 4,
		speed			: 700,
		loop			: true,
		pagination: {
			el: '.swiper-pagination4',
			clickable   : true
		},
		navigation: {
			nextEl: '.swiper-button-next4',
			prevEl: '.swiper-button-prev4',
		},
	});

	swiper4.on('slideChange', function () {
		var ind = swiper4.realIndex + 1;
		$('.swiper-slider-index4').html( ind + '/' + $('.swiper-container4').data('sliders-count') );
	});

	var swiper5 = new Swiper('.swiper-container5', {
		initialSlide    : 0,
        slidesPerView   : 2,
		speed			: 700,
		loop			: true,
		pagination: {
			el: '.swiper-pagination5',
			clickable   : true
		},
		navigation: {
			nextEl: '.swiper-button-next5',
			prevEl: '.swiper-button-prev5',
		},
	});

	swiper5.on('slideChange', function () {
		var ind = swiper5.realIndex + 1;
		$('.swiper-slider-index5').html( ind + '/' + $('.swiper-container5').data('sliders-count') );
	});
});
