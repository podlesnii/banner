//Checking banners from json callback

var searchBanners = $('#searchBanners').val();

var banner = JSON.parse(searchBanners);

$(document).ready(function () {

    ymaps.ready(init);

    $('#searchTable').DataTable();

    geoObjects = [];

    function init() {
        var map = new ymaps.Map('YMap', {
            center: [47.005560, 28.857500],
            zoom: 12,
            controls: [
                'zoomControl',
                'typeSelector',
                'fullscreenControl',
            ]
        });

        for (var i = 0; i < banner.length; i++) {
            for (var key in banner[i].images) {
                if (banner[i].images[0]) {
                    image = banner[i].images[0].path;
                }
            }
                status = "Available";
                if (banner[i].bannerStatuses) {
                    for (var key3 in banner[i].bannerStatuses) {
                        status = banner[i].bannerStatuses[key3].status;
                        timeStart = banner[i].bannerStatuses[key3].timeStart.date;
                        timeEnd = banner[i].bannerStatuses[key3].timeEnd.date;
                    }
                }


            geoObjects[i] = new ymaps.Placemark([banner[i].latitude, banner[i].longitude], {
                    hintContent: banner[i].category.name,
                    balloonContent: [
                        '<div class="card-image">',
                        '<img src="', image, '" width="200px" height="120px">',
                        '</div>',
                        '<div class="table-responsive">',
                        '<table class="table table-bordered">',
                        '<tr><th>Category:', '</th>',
                        '<td>', banner[i].category.name, '</td></tr>',
                        '<tr><th>Area:', '</th>',
                        '<td>', banner[i].area, '</td></tr>',
                        '<tr><th>Address:', '</th>',
                        '<td>', banner[i].adress, '</td></tr>',
                        '<tr><th>Status:', '</th>',
                        '<td>', status, '</td></tr>',
                        '<a class="btn btn-primary" href="/description?id=', banner[i].id, '">Details</a>',
                        '</table>',
                        '</div>',
                    ].join('')
                },

// Object Icon, could be different by groups
                /*        {
                            iconLayout: 'default#image',
                            iconImageHref: 'img/banner_icon.png',
                            iconImageSize: [46, 57],
                            iconImageOffset: [-23, -57],
                        }*/
            );

        }

        var clusterer = new ymaps.Clusterer({
// Cluster Icons, be not default
            /*        clusterIcons:[
                        {
                            href: 'img/banner_icon.png',
                            size: [100, 100],
                            offset: [-50 , -50]
                        }
                    ]*/
        });

        map.geoObjects.add(clusterer);
        clusterer.add(geoObjects);

    }
});






